/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
var app = {
		
    _onReady: null,
	    
	
    // Application Constructor
    initialize: function(onReady) {
    	_onReady = onReady,
        this.bindEvents();
    },
    
    // Bind Event Listeners
    //
    // Bind any events that are required on startup. Common events are:
    // 'load', 'deviceready', 'offline', and 'online'.
    bindEvents: function() {
        document.addEventListener('deviceready', this.onDeviceReady, false);
    },
    // deviceready Event Handler
    //
    // The scope of 'this' is the event. In order to call the 'receivedEvent'
    // function, we must explicity call 'app.receivedEvent(...);'
    onDeviceReady: function() {
        $.mobile.transitionFallbacks.slideout = "none";

        app.receivedEvent('deviceready');

    },
    // Update DOM on a Received Event
    receivedEvent: function(id) {
        navigator.splashscreen.hide();
    	if (_onReady)
    		_onReady();
    }
};

function isOnline(onSuccess){
    console.log("isOnline checking");
//    if(( /(ipad|iphone|ipod|android)/i.test(navigator.userAgent) )) {
//        document.addEventListener('deviceready', function(){
//            GumiServices.execAction(function(s){
//                if(s.status == 0){
//                    onSuccess(false);
//                }else{
//                    onSuccess(true);
//                }
//            }, function(){
//                console.log("error");
//            }, "isOnline", {});
//        }, false);
//
//    }else{
//        onSuccess(true);
//    }

    try{
        GumiServices.execAction(function(s){
            if(s.status == 0){
                onSuccess(false);
            }else{
                onSuccess(true);
            }
        }, function(){
            onSuccess(false);
        }, "isOnline", {});
    }catch (e){
        console.log("isOnline"+ e.message);
        onSuccess(true);
    }
}

function isFloat(n) {
    return n === +n && n !== (n|0);
}

function isInteger(n) {
    return n === +n && n === (n|0);
}

function isNotEmptyArray(ar){
    return Object.prototype.toString.call(ar) === "[object Array]" && ar.length > 0;
}
function isEmpty(val){
    return (val === undefined || val == null || val.length <= 0);
}
