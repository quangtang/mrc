/// <reference path="consts.ts" />
/// <reference path="dataStore.ts" />
/// <reference path="dataModel.ts" />
/// <reference path="../angular.d.ts" />
var gumi;
(function (gumi) {
    /**
    * Cung cap co che de truy cap vao application,
    * thong qua:
    * gumi.Application.instance(): lay application instance
    * gumi.Application.instance().dm(): Lay data model instance
    */
    var Application = (function () {
        function Application(toJson, fromJson) {
            this._params = [];
            this._mrID = "";
            this._caseStudyId = "1";
            this._answerGroupList = [];
            this._caseStudyFlowList = [];
            this._isEdity = false;
            this._questionScopes = {};
            this._ds = new gumi.DataStore(gumi.Const.APPCODE, toJson, fromJson);
            this._dm = new gumi.DataModel(this._ds);
        }
        Application.initialize = function (toJson, fromJson) {
            if (!Application._instance)
                Application._instance = new Application(toJson, fromJson);
        };

        Application.instance = function () {
            if (!Application._instance) {
                var toJson = function (obj) {
                    return angular.toJson(obj);
                };
                var fromJson = function (s) {
                    return angular.fromJson(s);
                };
                Application.initialize(toJson, fromJson);
            }
            return Application._instance;
        };

        /**
        * Access the data model instance
        */
        Application.prototype.dm = function () {
            return this._dm;
        };

        /**
        * Get external param
        */
        Application.prototype.getExtParam = function (paramName) {
            return this._params[paramName];
        };

        /**
        * Set external param
        */
        Application.prototype.setExtParam = function (paramName, paramValue) {
            this._params[paramName] = paramValue;
        };

        /**
        * Login
        */
        Application.prototype.login = function (userName, password, onSuccess, onFailed) {
            var user = this._dm.getUserByName(userName);
            if (user && user.password === password) {
                onSuccess();
            } else {
                onFailed();
            }
        };

        /**
        * Check and get last user id
        */
        Application.prototype.checkAndGetLastUser = function () {
            return this._dm.getLoginInfoData();
        };

        /**
        * Save last user id
        */
        Application.prototype.saveLastUser = function (userData) {
            this._dm.setLoginInfoData(userData);
        };

        /**
        * Show hospital list follow mr
        */
        Application.prototype.getHospitalList = function (mrID) {
            return this._dm.getHospitalList(mrID);
        };


        /**
        * Show doctor list follow hospital
        */
        Application.prototype.getDoctorList = function (hospitalId) {
            return this._dm.getDoctorList(hospitalId);
        };


        /**
        * Show pg list
        */
        Application.prototype.showPGlist = function () {
            return this._dm.getPgInfoList();
        };

        /**
        * show data theo case study id
        */
        Application.prototype.showCaseData = function (caseId) {
            return this._dm.getCaseStudy(caseId);
        };

        // MS. Trinh
        /**
        * Push answers to confirm page
        */
        Application.prototype.submitAnswer = function (answerData) {
            for (var i = 0; i < this._answerGroupList.length; i++) {
                var ag = this._answerGroupList[i];
                if (ag.groupId == answerData.groupId) {
                    this._answerGroupList[i] = answerData;
                    return;
                }
            }

            this._answerGroupList.push(answerData);
        };

        /**
        * Remove Answer Group
        */
        Application.prototype.removeAnswerGroup = function (index) {
            var t = _.filter(this._answerGroupList,function(d){
                return d.groupId != index;
            });
            this._answerGroupList = Object.prototype.toString.call(t) === "[object Array]"?t:[t];
        };

        /**
        * Return all submitted answers, will be used for confirm page
        */
        Application.prototype.getAnswerGroupList = function () {
            return this._answerGroupList;
        };

        /**
        * Reset Default Case Study Flows
        */
        Application.prototype.setDefaultCaseStudyFlowList = function () {
            // Add 6 default case study flows
            if (this._caseStudyFlowList.length == 0) {
                for (var i = 1; i <= gumi.MAX_CASE_SRUDY; i++) {
                    var emptyCaseStudyFlow = {
                        id: i,
                        type: gumi.Const.OT_CF,
                        csId: i,
                        hospitalId: this._hospitalId,
                        doctorId: this._doctorId,
                        pgId: this._pgId,
                        createdDate: "",
                        isSynced: false,
                        answers: null,
                        isChecked: false,
                        pgName: "",
                        syncText: gumi.Const.QC_NOT_SYNCED
                    };
                    this._caseStudyFlowList.push(emptyCaseStudyFlow);
                }
            }
        };

        Application.prototype.getDefaultCaseStudyFlowList = function () {
            var ret = [];
            for (var i = 1; i <= gumi.MAX_CASE_SRUDY; i++) {
                var emptyCaseStudyFlow = {
                    id: i,
                    type: gumi.Const.OT_CF,
                    csId: i,
                    hospitalId: this._hospitalId,
                    doctorId: this._doctorId,
                    pgId: this._pgId,
                    createdDate: "",
                    isSynced: false,
                    answers: null,
                    isChecked: false,
                    pgName: "",
                    syncText: gumi.Const.QC_NOT_SYNCED
                };
                ret.push(emptyCaseStudyFlow);
            }
            return ret;
        };

        /**
        * Set CaseStudyFlow for confirm page
        */
        Application.prototype.submitCaseStudyFlow = function (caseStudyFlow) {

            // Add 3 Default Case Study Flows
            this.setDefaultCaseStudyFlowList();
            for (var i = 0; i < this._caseStudyFlowList.length; i++) {
                var caseStudy = this._caseStudyFlowList[i];

                if (caseStudy.csId == caseStudyFlow.csId) {
                    this._caseStudyFlowList[i] = caseStudyFlow;
                    return;
                }
            }
            this._caseStudyFlowList.push(caseStudyFlow);
        };

        /**
        * Get Case Study Flow List
        */
        Application.prototype.getCaseStudyFlowList = function () {

            var caseStudyFlows = [];
            for (var i = 0; i < this._caseStudyFlowList.length; i++) {
                var caseStudyFlow = this._caseStudyFlowList[i];
                if (caseStudyFlow.hospitalId === this._hospitalId && caseStudyFlow.doctorId === this._doctorId) {
                    var pg = this.getPg(caseStudyFlow.pgId);
                    caseStudyFlow.pgName = pg != null ? pg.name : "";
                    caseStudyFlow.syncText = caseStudyFlow.isSynced ? gumi.Const.QC_SYNCED : gumi.Const.QC_NOT_SYNCED;
                    caseStudyFlows.push(caseStudyFlow);
                }
            }

            return caseStudyFlows;
        };

        /**
        * Save the answer list, after confirm
        */
        Application.prototype.saveAnswer = function (appInfo) {
            // insert db
            this.dm().saveAppInfo(appInfo);
        };

        /**
        * Get Hospital
        */
        Application.prototype.getHospital = function (hospitalId) {
            return this._dm.getHospital(hospitalId);
        };

        /**
        * Get Doctor
        */
        Application.prototype.getDoctor = function (doctorId) {
            return this._dm.getDoctor(doctorId);
        };

        /**
        * Get PG Info
        */
        //todo: remove hard code for case 6 '{ type: gumi.Const.OT_PI, id: "0", name: "無治療" }'
        Application.prototype.getPg = function (pgId) {
            return pgId == 0?{ type: gumi.Const.OT_PI, id: "0", name: "無治療" }:this._dm.getPg(pgId);
        };

        /**
        * Get Case Study
        */
        Application.prototype.getCaseStudy = function () {
            return this._dm.getCaseStudy(this._caseStudyId);
        };

        /**
        * Get Question Group List
        */
        Application.prototype.getQuestionGroupList = function () {
            var questionGroupList = this._dm.getQuestionGroupList(this._caseStudyId);

            for (var i = 0; i < questionGroupList.length; i++) {
                var questionGroup = questionGroupList[i];
                var questionMainList = questionGroup.children;
                for (var j = 0; j < questionMainList.length; j++) {
                    var questionMain = questionMainList[j];
                    var questionSubList = questionMain.children;

                    // Set selected answers
                    this.setSelectedAnswers(questionGroup, questionMain, questionSubList);
                }
            }

            return questionGroupList;
        };

        /**
        * Set selected answers
        */
        Application.prototype.setSelectedAnswers = function (questionGroup, questionMain, questionSubList) {
            if(questionSubList.length > 0){
                for (var i = 0; i < questionSubList.length; i++) {
                    var questionSub = questionSubList[i];
                    var flag = this.checkExistedAnswerSub(questionGroup, questionMain, questionSub);
                    if (flag) {
                        questionSub.isChecked = true;
                        questionMain.isChecked = true;
                    }
                }
            }else{
                var flagMainSingle = this.checkExistedAnswerSub(questionGroup, questionMain, questionSubList);
                if (flagMainSingle) {
                    questionMain.isChecked = true;
                }
            }
        };

        /**
        * Check existed answer sub
        */
        Application.prototype.checkExistedAnswerSub = function (questionGroup, questionMain, questionSub) {
            var flag = false;
            for (var i = 0; i < this._answerGroupList.length; i++) {
                var answerGroup = this._answerGroupList[i];
                if (questionGroup.index === answerGroup.groupId) {
                    var answerSubList = answerGroup.answers;
                    for (var j = 0; j < answerSubList.length; j++) {
                        var answerSub = answerSubList[j];
                        if (questionMain.id === answerSub.mainId && questionSub.id === answerSub.subId) {

                            flag = true;
                            if (questionSub.isFreeText) {
                                questionSub.freeText = answerSub.freeText;
                            }
                            break;
                        }
                    }
                }
            }
            return flag;
        };

        /**
        * Get Question Group By Id
        */
        Application.prototype.getQuestionGroup = function (index) {
            var questionGroupList = this.getQuestionGroupList();

            var questionGroup;

            for (var i = 0; i < questionGroupList.length; i++) {
                if (questionGroupList[i].index === index) {
                    questionGroup = questionGroupList[i];
                }
            }

            // Set PG list for main question of question 2
            if (questionGroup != null && questionGroup.isRadio) {
                var mainQuestions = questionGroup.children;
                for (var i = 0; i < mainQuestions.length; i++) {
                    var questionMain = mainQuestions[i];

                    // Set sub question list for main question which has type is Pg
                    if (questionMain.isPg) {
                        questionMain.children = [];

                        // Get Pg Sub Questions
                        var questionSubList = this.getPgSubQuestions(questionMain.id);
                        this.setSelectedAnswers(questionGroup, questionMain, questionSubList);

                        // Set sub question list
                        questionMain.children = questionSubList;
                    }
                }
            }
            return questionGroup;
        };

        /**
        * Get PG list for main question of question 2
        */
        Application.prototype.getPgSubQuestions = function (mainQuestionId) {
            var subQuestions = [];

            // Get all of pg list except selected pgId
            var pgList = this._dm.getPgInfoList();

            for (var j = 0; j < pgList.length; j++) {
                var pg = pgList[j];
                if (pg.id === this._pgId) {
                    continue;
                }

                // Create sub question from pgList
                var subQuestion = {
                    type: gumi.Const.OT_QS,
                    id: mainQuestionId + gumi.Const.QM_DOT + pg.id,
                    description: pg.name,
                    isFreeText: false,
                    isChecked: false,
                    count: "0"
                };
                subQuestions.push(subQuestion);
            }
            return subQuestions;
        };


        /**
        * Get Application Info List
        */
        Application.prototype.getAppInfoList = function () {
            var appInfoList = this._dm.getAppInfoList(this._mrID, this._userID);
            for (var i = 0; i < appInfoList.length; i++) {
                var appInfo = appInfoList[i];
                //var isSynced = true;
                var caseStudyFlows = appInfo.caseStudyFlows;
                for (var j = 0; j < caseStudyFlows.length; j++) {
                    var caseStudyFlow = caseStudyFlows[j];
                    if (caseStudyFlow.isChecked) {
                        var hospital = this.getHospital(caseStudyFlow.hospitalId);
                        var doctor = this.getDoctor(caseStudyFlow.doctorId);
                        appInfo.hospitalName = hospital != null ? hospital.name : "";
                        appInfo.doctorName = doctor != null ? doctor.name : "";
                    }
                }
                appInfo.syncText = appInfo.isSynced ? gumi.Const.QC_SYNCED : gumi.Const.QC_NOT_SYNCED;
            }
            return appInfoList;
        };


        /**
        * Delete Application Info
        */
        Application.prototype.deleteAppInfo = function (appInfoId) {
            this.dm().deleteAppInfo(appInfoId);
        };

        Application.prototype.resetFlow = function () {
            this._caseStudyId = "1";
            this._hospitalId = "";
            this._doctorId = "";
            this._pgId = "";
            // Reset the answer list too
            this._answerGroupList = [];
        };

        Application.prototype.resetAnswerGroupList = function () {
            this._answerGroupList = [];
        };

        Application.prototype.cacheQuestionScope = function (questionGroupId, scope) {
            this._questionScopes[questionGroupId] = scope;
        };

        // Refresh all question scopes
        Application.prototype.refreshQuestionScopes = function (prefix) {

            for (var k in this._questionScopes) {
                if (!prefix || k.indexOf(prefix) == 0) {
                    var scope = this._questionScopes[k];
                    if(typeof scope.refresh === "function")
                        scope.refresh();
                }
            }
        };

        return Application;
    })();
    gumi.Application = Application;
})(gumi || (gumi = {}));
//# sourceMappingURL=application.js.map
