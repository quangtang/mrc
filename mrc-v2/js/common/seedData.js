﻿/// <reference path="consts.ts" />
var gumi;
(function (gumi) {
    var SeedData = (function () {
        function SeedData() {
        }
        SeedData.getSeedData = function (caseStudyId,recordId,isReplace) {

            var option = [
                { type: gumi.Const.OT_QS, id: "2.3.1", description: "ラタノプロスト/チモロールマレイン酸塩配合剤", isFreeText: false, isChecked: false },
                { type: gumi.Const.OT_QS, id: "2.3.2", description: "トラボプロスト/チモロールマレイン酸塩配合剤", isFreeText: false, isChecked: false }
            ];

            if(caseStudyId == 4 || caseStudyId == 5 || caseStudyId == 6){
                option[2] = { type: gumi.Const.OT_QS, id: "2.3.3", description: "タフルプロスト/チモロールマレイン酸塩点眼液", isFreeText: false, isChecked: false }
            }

            return [

                {
                    type: gumi.Const.OT_QG, id: recordId, index: "1", caseStudyId: caseStudyId, description: "", isRadio: false, maxChoice: 3,
                    // Child main questions
                    children: [
                        {
                            type: gumi.Const.OT_QM, id: "1.1", description: "眼圧下降効果", isChecked: false, isPg: false, count: "",
                            // Child sub questions
                            children: [
                                { type: gumi.Const.OT_QS, id: "1.1.1", description: "眼圧下降量(下降幅)が大きい", isFreeText: false, isChecked: false },
                                { type: gumi.Const.OT_QS, id: "1.1.2", description: "目標眼圧に対する達成可能性が高い", isFreeText: false, isChecked: false },
                                { type: gumi.Const.OT_QS, id: "1.1.3", description: "眼圧日内変動の抑制も考慮した眼圧下降", isFreeText: false, isChecked: false },
                                { type: gumi.Const.OT_QS, id: "1.1.4", description: "長期に渡る安定した眼圧下降", isFreeText: false, isChecked: false },
                                { type: gumi.Const.OT_QS, id: "1.1.5", description: "その他", isFreeText: true, isChecked: false }
                            ]
                        },
                        {
                            type: gumi.Const.OT_QM, id: "1.2", description: "アドヒアランス", isChecked: false, isPg: false,
                            // Child sub questions
                            children: [
                                { type: gumi.Const.OT_QS, id: "1.2.1", description: "点眼回数が少ない", isFreeText: false, isChecked: false },
                                { type: gumi.Const.OT_QS, id: "1.2.2", description: "点眼本数が少ない", isFreeText: false, isChecked: false },
                                { type: gumi.Const.OT_QS, id: "1.2.3", description: "点眼容器が使いやすい", isFreeText: false, isChecked: false },
                                { type: gumi.Const.OT_QS, id: "1.2.4", description: "その他", isFreeText: true, isChecked: false }
                            ]
                        },
                        {
                            type: gumi.Const.OT_QM, id: "1.3", description: "安全性", isChecked: false, isPg: false,
                            // Child sub questions
                            children: [
                                { type: gumi.Const.OT_QS, id: "1.3.1", description: "角膜障害の発生が少ない", isFreeText: false, isChecked: false },
                                { type: gumi.Const.OT_QS, id: "1.3.2", description: "充血の程度が軽い", isFreeText: false, isChecked: false },
                                { type: gumi.Const.OT_QS, id: "1.3.3", description: "色素沈着や眼瞼のくぼみの発生が少ない", isFreeText: false, isChecked: false },
                                { type: gumi.Const.OT_QS, id: "1.3.4", description: "眼刺激性が低い", isFreeText: false, isChecked: false },
                                { type: gumi.Const.OT_QS, id: "1.3.5", description: "全身性副作用へのリスクが少ない", isFreeText: false, isChecked: false },
                                { type: gumi.Const.OT_QS, id: "1.3.6", description: "その他", isFreeText: true, isChecked: false }
                            ]
                        },
                        {
                            type: gumi.Const.OT_QM, id: "1.4", description: "薬価", isChecked: false, isPg: false,
                            // Child sub questions
                            children: [
                                { type: gumi.Const.OT_QS, id: "1.4.1", description: "薬価が安い", isFreeText: false, isChecked: false }
                            ]
                        },
                        {
                            type: gumi.Const.OT_QM, id: "1.5", description: "眼圧下降以外の効果", isChecked: false, isPg: false,
                            // Child sub questions
                            children: [
                                { type: gumi.Const.OT_QS, id: "1.5.1", description: "神経保護効果を期待できる", isFreeText: false, isChecked: false },
                                { type: gumi.Const.OT_QS, id: "1.5.2", description: "眼血流改善作用を期待できる", isFreeText: false, isChecked: false },
                                { type: gumi.Const.OT_QS, id: "1.5.3", description: "その他", isFreeText: true, isChecked: false }
                            ]
                        }
                    ]
                },
                // Question group 2
                {
                    type: gumi.Const.OT_QG, id: recordId+1, index: "2", caseStudyId:caseStudyId, description: "", isRadio: true, maxChoice: 1,
                    // Child main questions
                    children: [
                        {
                            type: gumi.Const.OT_QM, id: "2.1", description: "経過観察", isChecked: false, isPg: false,
                            // Child sub questions
                            children: []
                        },
                        {
                            type: gumi.Const.OT_QM, id: "2.2", description: isReplace?"PG単剤":"他のPG単剤切り替え", isChecked: false, isPg: true,
                            // Child sub questions
                            children: [
                                { type: gumi.Const.OT_QS, id: "2.2.1", description: "ラタノプロスト点眼液", isFreeText: false, isChecked: false },
                                { type: gumi.Const.OT_QS, id: "2.2.2", description: "タフルプロスト点眼液", isFreeText: false, isChecked: false },
                                { type: gumi.Const.OT_QS, id: "2.2.3", description: "トラボプロスト点眼液", isFreeText: false, isChecked: false },
                                { type: gumi.Const.OT_QS, id: "2.2.4", description: "ビマトプロスト点眼液", isFreeText: false, isChecked: false }

                            ]
                        },
                        {
                            type: gumi.Const.OT_QM, id: "2.3", description: isReplace?"PG/β配合剤":"PG/β配合剤に切り替え", isChecked: false, isPg: false,
                            // Child sub questions
                            children: option
                        },
                        {
                            type: gumi.Const.OT_QM, id: "2.4", description: isReplace?"β遮断薬単剤":"β遮断薬単剤追加", isChecked: false, isPg: false,
                            // Child sub questions
                            children: [
                                { type: gumi.Const.OT_QS, id: "2.4.1", description: "チモロールマレイン酸塩点眼液", isFreeText: false, isChecked: false },
                                { type: gumi.Const.OT_QS, id: "2.4.2", description: "チモロールマレイン酸塩持続性点眼液", isFreeText: false, isChecked: false },
                                { type: gumi.Const.OT_QS, id: "2.4.3", description: "カルテオロール塩酸塩点眼液", isFreeText: false, isChecked: false },
                                { type: gumi.Const.OT_QS, id: "2.4.4", description: "カルテオロール塩酸塩持続性点眼液", isFreeText: false, isChecked: false }
                            ]
                        },
                        {
                            type: gumi.Const.OT_QM, id: "2.5", description: isReplace?"CAI単剤・α刺激薬":"CAI単剤・α関連薬追加", isChecked: false, isPg: false,
                            // Child sub questions
                            children: [
                                { type: gumi.Const.OT_QS, id: "2.5.1", description: "ドルゾラミド塩酸塩点眼液", isFreeText: false, isChecked: false },
                                { type: gumi.Const.OT_QS, id: "2.5.2", description: "ブリンゾラミド懸濁性点眼液", isFreeText: false, isChecked: false },
                                { type: gumi.Const.OT_QS, id: "2.5.3", description: "ブリモニジン酒石酸塩点眼液", isFreeText: false, isChecked: false },
                                { type: gumi.Const.OT_QS, id: "2.5.4", description: "ブナゾシン塩酸塩点眼液", isFreeText: false, isChecked: false }
                            ]
                        },
                        {
                            type: gumi.Const.OT_QM, id: "2.6", description: isReplace?"β/CAI配合剤":"β/CAI配合剤追加", isChecked: false, isPg: false,
                            // Child sub questions
                            children: [
                                { type: gumi.Const.OT_QS, id: "2.6.1", description: "ドルゾラミド塩酸塩/チモロールマレイン酸塩点眼液", isFreeText: false, isChecked: false },
                                { type: gumi.Const.OT_QS, id: "2.6.2", description: "ブリンゾラミド/チモロールマレイン酸塩配合懸濁性点眼液", isFreeText: false, isChecked: false }
                            ]
                        },
                        {
                            type: gumi.Const.OT_QM, id: "2.7", description: "その他", isChecked: false, isPg: false,
                            // Child sub questions
                            children: [
                                { type: gumi.Const.OT_QS, id: "2.7.1", description: "", isFreeText: true, isChecked: false }
                            ]
                        }
                    ]
                }
            ];

        };


        SeedData.getPgData = function () {
            return [
                // PG Info
                { type: gumi.Const.OT_PI, id: "1", name: "ラタノプロスト点眼液" },
                { type: gumi.Const.OT_PI, id: "2", name: "タフルプロスト点眼液" },
                { type: gumi.Const.OT_PI, id: "3", name: "トラボプロスト点眼液" },
                { type: gumi.Const.OT_PI, id: "4", name: "ビマトプロスト点眼液" }
            ];
        };
        return SeedData;
    })();
    gumi.SeedData = SeedData;
})(gumi || (gumi = {}));
//# sourceMappingURL=seedData.js.map
