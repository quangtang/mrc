/**
* Defines all constants in the application
*/
var gumi;
(function (gumi) {
    var Const = (function () {
        function Const() {
        }
        //Const.SERVER_URL = "http://111.87.74.121/app/api/";
        Const.SERVER_URL = "http://111.87.74.121/app_test/api/";
        Const.APPCODE = "mrcs";

        Const.DELI = "|";

        Const.OT_USER = "U";
        Const.OT_LI = "LI";

        Const.OT_CS = "CS";
        Const.OT_PI = "PI";
        Const.OT_II = "II";

        Const.OT_HI = "HI";
        Const.OT_DI = "DI";

        Const.OT_QG = "QG";

        Const.OT_QM = "QM";

        Const.OT_QS = "QS";

        Const.OT_DH = "DH";

        Const.QM_DOT = ".";


        Const.QC_NOT_SYNCED = "未送信";
        Const.QC_SYNCED = "送信済";
        Const.QC_COLON = ":";
        Const.QC_PG = "◯◯◯◯◯◯";



        Const.OT_AI = "AI";

        Const.OT_SI = "SI";

        Const.OT_CF = "CF";

        return Const;
    })();
    gumi.Const = Const;
})(gumi || (gumi = {}));
//# sourceMappingURL=consts.js.map
