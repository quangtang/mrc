mrcsApp.controller("pdfController", ["$scope", "appFactory",
    function($scope, appFactory){
        $scope.refresh = function(){
            appFactory._backButtonConfirm = false;
            $scope.backPage = function(){
                appFactory.backPage = "#data01-page";
            };

            var caseStudyFlowList = appFactory.getCaseStudyFlowList();
            var caseStudyID = appFactory._caseStudyId;

            var caseStudyFlow = _.filter(caseStudyFlowList, function(cs){
                return (cs.isChecked && cs.csId == caseStudyID)
            })[0];

            if(!_.isUndefined(caseStudyFlow)){
                // Get the medicine name
                var medicineName =  caseStudyFlow.pgId == 2 || caseStudyFlow.pgId == 0?"main":"Other Medicines";

                // Get the last answer data from users
                var userAnswers = caseStudyFlow.answers;

                if(!_.isUndefined(userAnswers) && !_.isEmpty(userAnswers)){

                    var answer01 = [];
                    var answer02MainId;


                    if(userAnswers[1]){
                        answer02MainId = userAnswers[1].answers[0].mainId;
                    }else{
                        answer02MainId = userAnswers[0].answers[0].mainId;
                    }




                    _.each(userAnswers[0].answers, function(v){
                        answer01.push(v.subId);
                    });

                    $scope.pdfFilesList = [];
                    if(answer02MainId == "2.1" || answer02MainId == "2.2" || answer02MainId == "2.5" || answer02MainId == "2.6"){
                        // Get the anything type PDF list
                        var anythingPDFList = _.findWhere(PDF_SETTINGS.medicines, { name: "Anything" });
                        $scope.pdfFilesList = _.findWhere(anythingPDFList.question02, { mainId : answer02MainId }).pdfFilesList;
                    } else{
                        _.each(PDF_SETTINGS.medicines, function(m){
                            // Get the pdf files base on medicine name
                            if(m.name == medicineName){
                                // Get the pdf files base on answer for question02
                                _.each(m.question02, function(q){
                                    if (_.contains(q.mainId,answer02MainId)){
                                        $scope.pdfFilesList = q.pdfFilesList
                                    }
                                });
                            }
                        });
                    }


                    if(answer01.length > 0 && answer01.length <= 3){
                        $scope.pdfFile = _.find($scope.pdfFilesList, function(pdfFile){
                            var mustConditions = pdfFile.mustConditions;
                            var diffConditions = pdfFile.diffConditions;
                            var containConditions = pdfFile.containConditions;
                            var matchDiffConditions;

                            // Get the answers match with MUST conditions !
                            var matchMustCondition = _.intersection(answer01, mustConditions);

                            if(matchMustCondition.length == mustConditions.length){
                                if(containConditions){
                                    var matchContainConditions = _.intersection(answer01, containConditions);
                                    if(matchContainConditions.length > 0){
                                        matchDiffConditions = _.intersection(diffConditions, answer01);
                                        if(matchDiffConditions.length == 0){
                                            return pdfFile
                                        }
                                    }
                                }else{
                                    matchDiffConditions = _.intersection(diffConditions, answer01);
                                    if(matchDiffConditions.length == 0){
                                        return pdfFile
                                    }
                                }
                            }
                        });

                        if(answer02MainId == "2.6" && caseStudyID == 6){
                            $scope.pdfFile = {
                                filename: "165.pdf",
                                pages: [
                                    {
                                        pageNumber: 1,
                                        title: "Dummy title page 165",
                                        desc: "Dummy description page 165"
                                    }
                                ],
                                mustConditions : [],
                                diffConditions : []
                            }
                        }

                        if(answer02MainId != "2.6" && answer02MainId != "2.3" && caseStudyID == 6){
                            $scope.pdfFile = {
                                filename: "166.pdf",
                                pages: [
                                    {
                                        pageNumber: 1,
                                        title: "Dummy title page 166",
                                        desc: "Dummy description page 166"
                                    }
                                ],
                                mustConditions : [],
                                diffConditions : []
                            }
                        }
                    }
                }
            }

            //$scope.pdfFile.filename = "test.pdf";
            $scope.popupPdf = function(filename, pageNumber){
                console.log($scope.pdfFile);

                if(_.size($scope.pdfFile) > 0){

                    var opt = {
                        url: PDF_SETTINGS.pdfFolderURL + filename,
                        pageNumber: pageNumber||1
                    };

                    try{
                        GumiServices.execAction(function(){
                            console.log("success");
                        }, function(){
                            console.log("error");
                        }, "readPDF", opt);
                    }catch (e){
                        console.log(opt);
                        console.log(e.message);
                    }

                }
            };

            $scope.gotoPage = function(nextPage){
                appFactory._backButtonConfirm = true;
                // Refresh confirm page
                appFactory.refreshQuestionScopes('question-confirm');
                //Forward to case

                $scope.changePage(nextPage);
            }

        }
    }]
);