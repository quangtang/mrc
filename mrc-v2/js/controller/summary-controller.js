mrcsApp.controller('SummaryController',
    function ($scope, appFactory) {

        $scope.btnCharts = [
            {
                title:"症例①",
                group:"1",
                groupName_01:"Q1",
                group_01:"1",
                groupName_02:"Q2",
                group_02:"2",
                isBarChart:"1"
            },{
                title:"症例②",
                group:"2",
                groupName_01:"Q1",
                group_01:"3",
                groupName_02:"Q2",
                group_02:"4",
                isBarChart:"3"
            },{
                title:"症例③",
                group:"3",
                groupName_01:"Q1",
                group_01:"5",
                groupName_02:"Q2",
                group_02:"6",
                isBarChart:"5"
            },{
                title:"症例④",
                group:"4",
                groupName_01:"Q1",
                group_01:"7",
                groupName_02:"Q2",
                group_02:"8",
                isBarChart:"7"
            },{
                title:"症例⑤",
                group:"5",
                groupName_01:"Q1",
                group_01:"9",
                groupName_02:"Q2",
                group_02:"10",
                isBarChart:"9"
            },{
                title:"症例⑥",
                group:"6",
                groupName_01:"Q1",
                group_01:"11",
                groupName_02:"Q2",
                group_02:"12",
                isBarChart:"11"
            }
        ];
        $scope.groupNumber = {
            firstGroup:1,
            secondGroup:2,
            index:"1",
            group:"1",
            barChart:true
        };

        $scope.legend = [];

        $scope.changedCase = function(event,index,option){
            event.preventDefault();
            $scope.groupNumber.index = index;
            $scope.groupNumber.group = option.group;
            $scope.groupNumber.barChart = option.isBarChart === index;
            $scope.groupNumber.firstGroup = parseInt(option.group_01);
            $scope.groupNumber.secondGroup = parseInt(option.group_02);
            if(!$scope.groupNumber.barChart){
                $scope.legend = [];
                _.each(appFactory._ds.getObject(gumi.Const.OT_QG,index).children,function(d){
                    $scope.legend.push(d.description);
                });
            }

            drawChart();
        };


        $scope.GG = {
            group:{}
        };

        function calculateAnswersData() {
            //Get the server answer first
            var sa = appFactory._dm.getServerAnswer();

            if (sa && sa.groupAnswers) {
                //tra ve ket qua kong can thong qua du lieu local
                $scope.GG.group = sa.groupAnswers.group;
            }else{
                //Dau tien, dua tren cau truc cua cac cau hoi, ta tao ra du lieu ban dau cho tat ca cac cau hoi
                var allQuestionGroups = appFactory._ds.getObjectList(gumi.Const.OT_QG);

                //console.log("allQuestionGroups : ", allQuestionGroups);
                _.each(allQuestionGroups, function (qg) {
                    $scope.GG.group[qg.id] = {
                        questionGroupId: qg.id,
                        counter: 0,
                        description: qg.description,
                        percent:0,
                        main:{}

                    };
                    _.each(qg.children, function (qm) {
                        $scope.GG.group[qg.id].main[qm.id] = {
                            type:gumi.Const.OT_AI,
                            questionMainId: qm.id,
                            questionGroupId: qg.id,
                            counter: 0,
                            description: qm.description,
                            percent:0,
                            sub:{}
                        };
                        _.each(qm.children, function (qs) {
                            $scope.GG.group[qg.id].main[qm.id].sub[qs.id] = {
                                questionSubId: qs.id,
                                questionMainId: qm.id,
                                questionGroupId: qg.id,
                                counter: 0,
                                description: qs.description,
                                percent:0
                            };
                        });
                    });
                });
            }


            var appInfoList = appFactory._ds.getObjectList(gumi.Const.OT_AI);


            if (isNotEmptyArray(appInfoList)) {
                //Duyet tung app info, co bao nhieu appinfo
                for (var i = 0,ii = appInfoList.length; i < ii; i++) {
                    // Check this appInfo (Doctor) already synced or not ?
                    if(!appInfoList[i].isSynced){
                        var ai = appInfoList[i];

                        //Duyet tung case study flow
                        var csfs = ai.caseStudyFlows;

                        if (isNotEmptyArray(csfs)) {
                            for (var j = 0,jj= csfs.length; j < jj; j++) {
                                var csf = csfs[j];
                                var groupFirstId = csf.csId*2 - 1;
                                // Check this caseStudy already synced or not ?
                                if(!csf.isSynced){
                                    //Duyet tung group answer
                                    var groupAnswers = csf.answers;

                                    if (isNotEmptyArray(groupAnswers)) {

                                        for (var k = 0,kk = groupAnswers.length; k < kk; k++) {
                                            var groupAnswer = groupAnswers[k];
                                            var currentGroup = groupFirstId + k;
                                            //cong counter cho group tuong ung

                                            $scope.GG.group[currentGroup].counter++;

                                            //Duyet tung main/sub answer
                                            var subAnswers = groupAnswer.answers;
                                            if (isNotEmptyArray(subAnswers)) {
                                                for (var l = 0,ll = subAnswers.length; l < ll; l++) {
                                                    var subAnswer = subAnswers[l];
                                                    //cong counter cho main tuong ung
                                                    if(subAnswer.mainId){
                                                        $scope.GG.group[currentGroup].main[subAnswer.mainId].counter++;
                                                        //cong counter cho sub tuong
                                                        if(subAnswer.subId)
                                                            $scope.GG.group[currentGroup].main[subAnswer.mainId].sub[subAnswer.subId].counter++;
                                                    }


                                                }
                                            }

                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                //sau khi tinh count cua main va sub thi ta tinh phan tram cho moi sub trong main
            }

            calculatePercentOfSub();
            drawChart();

        }

        function calculatePercentOfSub(){

            for(var k in $scope.GG.group){
                for(var kk in $scope.GG.group[k].main){
                    var mainCounter = $scope.GG.group[k].main[kk].counter;

                    if(angular.equals({}, $scope.GG.group[k].main[kk].sub)){
                        $scope.GG.group[k].main[kk].class = "single";
                    }else{
                        $scope.GG.group[k].main[kk].class = "multi";
                    }
                    for(var kkk in $scope.GG.group[k].main[kk].sub){
                        var percent = 0;
                        if(mainCounter != 0){
                            percent = ($scope.GG.group[k].main[kk].sub[kkk].counter/mainCounter)*100;
                        }
                        $scope.GG.group[k].main[kk].sub[kkk].percent = percent;

                    }
                }
            }

        }

        function drawChart() {

            //lay total de tinh phan tram cho sub
            var totalQS1 = 0;
            _.size($scope.GG.group[$scope.groupNumber.firstGroup].main);
            for(var k in $scope.GG.group[$scope.groupNumber.firstGroup].main){
                totalQS1 += parseInt($scope.GG.group[$scope.groupNumber.firstGroup].main[k].counter);
            }



            var chart = $("#sumary .demo-container,#sumary .sumary01-right>img");
            if(totalQS1 === 0){
                chart.hide();
                for(var k in $scope.GG.group[$scope.groupNumber.firstGroup].main){
                    $scope.GG.group[$scope.groupNumber.firstGroup].main[k].percent = 0;
                }
            }else{
                chart.show();
                for(var k in $scope.GG.group[$scope.groupNumber.firstGroup].main){
                    $scope.GG.group[$scope.groupNumber.firstGroup].main[k].percent = ($scope.GG.group[$scope.groupNumber.firstGroup].main[k].counter/totalQS1)*100;
                }
            }

            var dataBarChart = [];
            for(var k in $scope.GG.group[$scope.groupNumber.firstGroup].main){
                dataBarChart.push({
                    data: [ [$scope.GG.group[$scope.groupNumber.firstGroup].main[k].description, ($scope.GG.group[$scope.groupNumber.firstGroup].main[k].percent).toFixed(1)] ]
                });
            }

            //ve bieu do cot
            $.plot("#placeholder-c1-01", dataBarChart, {
                series:
                {
                    bars: {
                        show: true,
                        barWidth: 1.5,
                        showNumbers: true,
                        numbers : {
                            show:true,
                            yAlign: function(y) {
                                return y+4;
                            }
                        }
                    },
                    percent: {
                        show: true,
                        color:["#a0d468", "#51aafc", "#fc6e51", "#da51fc", "#ffce54"]
                    }
                },
                colors: ["#a0d468", "#51aafc", "#fc6e51", "#da51fc", "#ffce54"],
                xaxis: {
                    mode: "categories",
                    tickLength: 0,
                    font:10,
                    ticks: [
                        [0, "眼圧下降効果"],
                        [1, "アドヒアランス"],
                        [2, "安全性"],
                        [3, "薬価"],
                        [4, "眼圧下降以外の効果"]
                    ]
                },
                grid: {
                    borderWidth: 1
                }
            });

            //chart pie
            var dataPieChart = [];
            for(var k in $scope.GG.group[$scope.groupNumber.secondGroup].main) {
                dataPieChart.push({
                    data:$scope.GG.group[$scope.groupNumber.secondGroup].main[k].counter
                });
            }

            //ve bieu do tron
            $.plot("#placeholder-c1-02", dataPieChart, {
                series: {
                    pie: {
                        show: true
                    },
                    bars: {
                        numbers: {}
                    }
                },
                legend: {
                    show: false
                },
                colors: ["#a0d468", "#51aafc", "#fc6e51", "#da51fc", "#ffce54","#ff0000"]
            });

        }

        $scope.refresh = function () {
            $scope.showBackChart = appFactory._showBackChart || false;
            calculateAnswersData();
            $scope.updateScope($scope);

        };

        $scope.resetBackChart = function(nextPage){
            appFactory._showBackChart = false;
            $scope.changePage(nextPage);
        }
    }
);
