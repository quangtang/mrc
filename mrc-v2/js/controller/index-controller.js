mrcsApp.controller('IndexController',
    function ($scope, appFactory,$timeout) {
        var isClickSync = false;
        $scope.refresh = function(){

            $scope.backPage = function(){
                appFactory.backPage = "#index-page";
            };

            appFactory._mrID = appFactory._mainMrId;
            appFactory._userID = appFactory._mainMrId;
            appFactory._userPASS = appFactory._mainPassword;

            _.each(appFactory._ds.getObjectList(gumi.Const.OT_AI),function(appInfo){
                if(appInfo.mrId != appFactory._mrID){
                    appFactory._ds.removeObject(appInfo.type,appInfo.id);
                }
            });
            var appInfoSynced = _.filter(appFactory._ds.getObjectList(gumi.Const.OT_AI), function(appInfo) {
                return appInfo.isSynced === false;
            });

            $scope.countSyn = appInfoSynced?appInfoSynced.length:0;

            prepareAnswerList();
            $scope.updateScope($scope);

            function getDeletedDoctorAnswers() {
                var ret = {};
                ret['login'] = appFactory._userID;
                ret['password'] = appFactory._userPASS;
                var listDoctorDeletedInfo = appFactory._dm.getDoctorDeletedList(appFactory._mrID);
                var requestCounter = -1;
                _.each(listDoctorDeletedInfo,function(doctor){
                    _.each(doctor.idAnswer,function(id){
                        requestCounter++;
                        ret['id'+requestCounter] = id;
                    });
                });
                ret['count'] = requestCounter == -1?0:requestCounter+1;
                return ret;
            }


            // enquete API
            function getEnqueteData() {
                var ret = {};
                ret['login'] = appFactory._userID;
                ret['password'] = appFactory._userPASS;

                //requestCounter it mean [x]: x se tang theo so luong bac si trong database
                var requestCounter = -1;
                //Dau tien, ta phai lay ket qua tra loi tu local truoc
                var appInfoList = appFactory._ds.getObjectList(gumi.Const.OT_AI);
                _.each(appInfoList, function(appInfo) {
                    if(!appInfo.isSynced){

                        for (var csIdx = 0; csIdx<appInfo.caseStudyFlows.length; csIdx++) {
                            var cs = appInfo.caseStudyFlows[csIdx];
                            if(!cs.isSynced && cs.answers){
                                requestCounter++;

                                ret["dr_id"+requestCounter] = appInfo.dr_id;
                               //ret['id'+requestCounter] = appInfo.dr_id+ "_" + cs.csId;
                               ret['id'+requestCounter] = cs.doctorId+ "_" + cs.csId;
                                var currentDoctor = appFactory.getDoctor(cs.doctorId);

                                if(currentDoctor.isfreeDoctor){
                                    ret["dr_name"+requestCounter] = currentDoctor.name;
                                    ret["dr_place"+requestCounter] = appFactory.getHospital(currentDoctor.hospitalId).name;
                                }

                                ret["place_code"+requestCounter] = currentDoctor.hospitalId;
                                ret["enq_type"+requestCounter] = cs.csId;
                                ret["choice_pg"+requestCounter] = cs.pgId;

                                for (var agIdx = 0; agIdx<cs.answers.length; agIdx++) {
                                    var ag = cs.answers[agIdx];
                                    if(ag){
                                        //lay sub 1
                                        for (var asIdx = 0; asIdx<ag.answers.length; asIdx++) {
                                            var as = ag.answers[asIdx];
                                            if(as.mainId){
                                                //neu groupId chia het cho 2 co nghia la question gruop 2 nguoc lai la 1
                                                //console.log("ag.groupId:",ag.groupId);
                                                var q = ag.groupId%2 == 0?2:1;

                                                ret["q"+q + "_" + (asIdx + 1) + "_major" + requestCounter] = _.last((as.mainId).split("."));
                                                if(as.subId){
                                                    ret["q"+q + "_" + (asIdx + 1) + "_minor" + requestCounter] = _.last((as.subId).split("."));
                                                    if(as.isFreeText)
                                                        ret["q"+q + "_" + (asIdx + 1) + "_text" + requestCounter] = as.freeText;
                                                }
                                            }
                                        }
                                    }
                                }

                                ret["datetime"+requestCounter] = cs.createdDate;
                            }
                        }
                    }

                });
                //tong hop so lan tra loi cua cac bac si
                ret['count'] = requestCounter == -1?0:requestCounter+1;
                console.log("requestParams:",ret);
                //throw "do not send";
                return ret;
            }


            function prepareAnswerList() {
                $scope.GG = {group:{}};
                //Dau tien, dua tren cau truc cua cac cau hoi, ta tao ra du lieu ban dau cho tat ca cac cau hoi
                var allQuestionGroups = appFactory._ds.getObjectList(gumi.Const.OT_QG);
                _.each(allQuestionGroups, function (qg) {
                    $scope.GG.group[qg.id] = {
                        questionGroupId: qg.id,
                        counter: 0,
                        description: qg.description,
                        percent:0,
                        main:{}

                    };
                    _.each(qg.children, function (qm) {
                        $scope.GG.group[qg.id].main[qm.id] = {
                            type:gumi.Const.OT_AI,
                            questionMainId: qm.id,
                            questionGroupId: qg.id,
                            counter: 0,
                            description: qm.description,
                            percent:0,
                            sub:{}
                        };
                        _.each(qm.children, function (qs) {
                            $scope.GG.group[qg.id].main[qm.id].sub[qs.id] = {
                                questionSubId: qs.id,
                                questionMainId: qm.id,
                                questionGroupId: qg.id,
                                counter: 0,
                                description: qs.description,
                                percent:0
                            };
                        });
                    });
                });
            }


            function processServerAnswerList(answerData) {
//                console.log("processServerAnswerList",answerData);
                    var caseStudyList = answerData.enquete_list.enquete;
                    if(Object.prototype.toString.call(caseStudyList) !== "[object Array]"){
                        caseStudyList = [caseStudyList];
                    }

                    //khoi tao gia tri cho bien group

                    _.each(caseStudyList, function (caseStudy, key) {
                        //lay id cua moi caseStudy
                        var csId = caseStudy.id;
                        if(csId === (gumi.MAX_CASE_SRUDY + 1)) return;

                        //Trong tung caseStudy se co dang caseStudy.total ={q1:{},q2:{}}
                        var qgList = caseStudy.questions.total;
                        //chi lay question 1 va question 2
                        var qgIdList = _.keys(qgList).splice(0,2);
                        _.each(qgIdList, function (sQgId) {
                            var qgId = sQgId.substring("q".length);
                            // sQgId se la q1 or q2
                            //group co tu 1->12 nen cu moi lan lap thi group se tang theo gia tri [1,2] [3,4] [5,6]...
                            var groupId = parseInt(qgId) === 1?csId*2 - 1:csId*2;

                            var questionGroup = qgList[sQgId];
                            var qmIdList = _.keys(questionGroup);
                            _.each(qmIdList, function (sQmId) {
                                if(sQmId != "dr_count"){
                                    //lay so phia sau category tuong ung la so 1,2,3,4,5,6... trong main
                                    var qmId = sQmId.substring("category".length);

                                    var questionMain = questionGroup[sQmId];
                                    //khoi tao counter cho main answer
                                    var questionSubTotal = 0;
                                    var qsIdList = _.keys(questionMain);
                                    var idMain = qgId+"."+qmId;
                                    _.each(qsIdList, function(sQsId) {
                                        var qsId = (parseInt(sQsId.substring("ans".length))+1) + "";
                                        var questionSubCount = parseInt(questionMain[sQsId]);

                                        //cong cac gia tri counter cua sub lai cho main counter
                                        questionSubTotal += questionSubCount;

                                        var idSub = idMain+"."+qsId;

                                        if ($scope.GG.group[groupId].main[idMain].sub[idSub]) {
                                            $scope.GG.group[groupId].main[idMain].sub[idSub].counter += questionSubCount;
                                        }
                                    });
                                    $scope.GG.group[groupId].main[idMain].counter +=  questionSubTotal;
                                }else{
                                    $scope.GG.group[groupId].counter = questionGroup["dr_count"];
                                }
                            });
                        });
                    });
                    //luu trong local storage de qua view ve bieu do su dung
                    appFactory._dm.setServerAnswer($scope.GG);

                    // Update "sync" flags in local data
                    clearSyncAnser();
            }



            function clearSyncAnser(){

                _.each(appFactory._ds.getObjectList(gumi.Const.OT_AI), function(appInfo) {
                    appFactory.deleteAppInfo(appInfo.id);
                });

                //finished clear sync then call get history list
                deleteHistoryList();
            }
            function cleardoctorHostory(){
                var listDoctorDeletedInfo = appFactory._dm.getDoctorDeletedList(appFactory._mrID);
                _.each(listDoctorDeletedInfo,function(doctor){
//                    console.log("clea");
                    appFactory._ds.removeObject(doctor.type,doctor.id);
                });
            }

            //cap nhat thong tin benh vien va bac si vao database
            function updateHospitalAndDoctor(data){
                //console.log(data);
                console.log("clear hospital and doctor before update");
                _.each(appFactory.getHospitalList(appFactory._mainMrId),function(hospital){
                    var hospitalId = hospital.id;
                    appFactory._ds.removeObject(hospital.type,hospitalId);
                    _.each(appFactory.getDoctorList(hospitalId),function(doctor){
                        appFactory._ds.removeObject(doctor.type,doctor.id);
                    });
                });

                if( Object.prototype.toString.call(data) === '[object Object]' ) {
                    data = [data];
                }
                _.each(data,function(hs){
                    //save hospital to database
                    appFactory._ds.setObject({
                        type: gumi.Const.OT_HI,
                        id: hs.id,
                        mrId: appFactory._mainMrId,
                        name: hs.name,
                        isFree:false
                    });
                    _.each(hs.doctor_list,function(dt){

                        if(dt.id){
                            //save doctor
                            appFactory._ds.setObject({
                                type: gumi.Const.OT_DI,
                                id: hs.id+"_"+dt.id,
                                dr_id:dt.id,
                                hospitalId: hs.id,
                                name: dt.name,
                                isfreeDoctor:false,
                                isFreeHospital:false
                            });
                        }else{
                            _.each(dt,function(dtt){
                                //save doctor
                                appFactory._ds.setObject({
                                    type: gumi.Const.OT_DI,
                                    id: hs.id+"_"+dtt.id,
                                    hospitalId: hs.id,
                                    dr_id:dtt.id,
                                    name: dtt.name,
                                    isfreeDoctor:false,
                                    isFreeHospital:false
                                });
                            });
                        }
                    });

                });
            }


            function saveHistoryList(drInfo){

                function setCaseStudyList(csId,pgId,hospitalId,doctorId,createdDate,mainAnswers,isChecked){
                    var id = csId;
                    if(isChecked){
                        id = appFactory._userID + '-' + hospitalId + '-' + doctorId;
                    }

                    var currentPg = appFactory.getPg(pgId);
                    return {
                        id: id,
                        type: gumi.Const.OT_CF,
                        csId: csId+1+"",
                        hospitalId: hospitalId,
                        doctorId: doctorId,
                        pgId: pgId,
                        pgName: currentPg != null?currentPg.name:"",
                        syncText: gumi.Const.QC_SYNCED,
                        isSynced: true,
                        createdDate: createdDate,
                        answers: mainAnswers,
                        isChecked: isChecked
                    };
                }

                function setMainAnswer(groupId,answers){
                    return {
                        groupId: groupId,
                        answers:answers
                    }
                }

                function setSubAnswer(csId,groupId,mainId,subId,isFreeText,freeText){
                    var currentQuestionGroupId = groupId == 1?(parseInt(csId) + 1)*2 - 1:(parseInt(csId) + 1)*2;

                    var  subContent = {
                        mainId: groupId +"."+mainId,
                        mainDesc: "",
                        subDesc: "",
                        isFreeText:isFreeText,
                        freeText: freeText
                    };

                    var currentMain = _.find(appFactory._ds.getObject(gumi.Const.OT_QG,currentQuestionGroupId).children,function(d){
                        return d.id == subContent.mainId
                    });

                    subContent.mainDesc = currentMain.description;


                    if(subId.length > 0){
                        subContent.subId = groupId +"."+mainId+"."+subId;
                        var currentSub = _.find(currentMain.children,function(d){
                            return d.id == subContent.subId
                        });
                        subContent.subDesc = currentSub.description;
                    }

                    return subContent;
                }
                function saveAppInfo(hospitalId,doctorId,caseStudyList,createdDate,dr_id) {
                    var appInfo = {
                        id: appFactory._mrID + '-' + appFactory._userID + '-' + hospitalId + '-' + doctorId,
                        type: gumi.Const.OT_AI,
                        mrId: appFactory._mrID,
                        userId: appFactory._userID,
                        caseStudyFlows: caseStudyList,
                        isSynced: true,
                        syncText: "",
                        dr_id:dr_id,
                        hospitalId: hospitalId,
                        doctorId: doctorId,
                        createdDate:createdDate,
                        isServer:true
                    };
                   // console.log(appInfo);
                    localStorage.setItem("isSave","1");
                    appFactory.saveAnswer(appInfo);
                }
                if(!angular.isDefined(drInfo)) return;
                if(!angular.isArray(drInfo)){
                    drInfo = [drInfo];
                }
                var newDrInfo = [];
                //loai bo bang cau hoi da bi xoa voi delete_flag = 1

                _.each(drInfo,function(item){
                    if(item.delete_flag == 0 && item.dr_id != ""){
                        newDrInfo.push(item);
                    }
                });

                //clear bien khong can dung o duoi
                drInfo = [];

                // group list benh vien theo place_code
                var hospitalGroup = _.groupBy(newDrInfo, function(dr){ return dr.place_code;});
                var doctorList = [];
                _.each(hospitalGroup,function(hp){
                    //group tung bac si lai voi nhau
                    var groupDoctorInHospital = _.groupBy(hp, function(dr){ return dr.dr_id;});

                    //loai bo key ma chi lay object
                    _.each(groupDoctorInHospital,function(d){
                        doctorList.push(d);
                    });

                });

                //clear bien khong can dung o duoi
                hospitalGroup = [];
//                console.log("doctorList:",doctorList);
                //kiem tra de dien vao luc nao cung du 3 case study
                function checkCase(caseS){
                    var index = -1;
                    for(var i = 0;i <caseS.length;i++){
                        if(!caseS[i]){
                            index = i;
                            break;
                        }
                    }
                    return index;
                }

                //luu tung bac si
                _.each(doctorList,function(doctor){

                    var caseStudy = [],
                        caseSq =[false,false,false,false,false,false],
                        timeGetList,
                        doctorId,
                        hospitalId,
                        dr_id;//cap nhat thong tin id cho appInfo

                    _.each(doctor,function(cs){

                        var pgId = cs.choice_pg,
                            freeDoc = cs.dr_name,
                            freeHos = cs.dr_place,
                            cId = cs.enq_type - 1,
                            createdDate = cs.datetime,
                            maxTime = moment(createdDate,"YYYY-MM-DD HH:mm:ss")._d.getTime();

                        dr_id = cs.dr_id;
                        hospitalId = cs.place_code;
                        //luu doctorId bang cach dinh kem id benh vien vi moi bac si co the thuoc nhieu benh vien khac nhau
                        doctorId = hospitalId + "_"+ dr_id;

                        if(!timeGetList)
                            timeGetList = maxTime;
                        else if(maxTime > timeGetList){
                            timeGetList = maxTime;
                        }

                        if(freeDoc.length > 0){
                            var currentHospital = _.find(appFactory.getHospitalList(appFactory._userID),function(hospital){
                                return hospital.hospitalId == hospitalId;
                            });
//                            console.log("currentHospital",currentHospital);

                            if(angular.isDefined(currentHospital)){

                                appFactory._ds.setObject({
                                    type: gumi.Const.OT_DI,
                                    id:doctorId,
                                    hospitalId:hospitalId,
                                    dr_id:dr_id,
                                    name:freeDoc,
                                    isfreeDoctor:true,
                                    isFreeHospital:currentHospital.isFree
                                });

                            }else{

                                appFactory._ds.setObject({
                                    type: gumi.Const.OT_HI,
                                    id:hospitalId ,
                                    mrId:appFactory._userID,
                                    name:freeHos,
                                    isFree:true

                                });
                                appFactory._ds.setObject({
                                    type: gumi.Const.OT_DI,
                                    id:doctorId,
                                    dr_id:dr_id,
                                    hospitalId:hospitalId,
                                    name:freeDoc,
                                    isfreeDoctor:true,
                                    isFreeHospital:true
                                });

                            }

                        }


                        caseSq[cId] = true;
                        var mainAnswer = [],
                            answerM1 = [],
                            answerM2 = [];

                        for(var j = 1;j < gumi.MAX_CASE_SRUDY; j++){
                            var item = "q1_"+j+"_";
                            if(angular.isDefined(cs[item+"major"]) && angular.isDefined(cs[item+"text"])){
                                answerM1.push(setSubAnswer(cId,"1",cs[item+"major"],cs[item+"minor"],true,cs[item+"text"]));
                            }else{
                                if(angular.isDefined(cs[item+"major"]))
                                    answerM1.push(setSubAnswer(cId,"1",cs[item+"major"],cs[item+"minor"],false,""));
                            }
                        }
                        var groupQ2 = 1;
                        if(answerM1.length == 0){
                            groupQ2 = 0;
                        }else{
                            groupQ2 = 1;
                            mainAnswer[0] = setMainAnswer("1",answerM1);
                        }

                        if(angular.isDefined(cs["q2_1_major"]) && angular.isDefined(cs["q2_1_text"])){
                            answerM2.push(setSubAnswer(cId,"2",cs["q2_1_major"],cs["q2_1_minor"],true,cs["q2_1_text"]));
                            mainAnswer[groupQ2] = setMainAnswer("2",answerM2);
                        }else{
                            if(angular.isDefined(cs["q2_1_major"])){
                                answerM2.push(setSubAnswer(cId,"2",cs["q2_1_major"],cs["q2_1_minor"],false,""));
                                mainAnswer[groupQ2] = setMainAnswer("2",answerM2);
                            }
                        }

                        caseStudy[cId] = setCaseStudyList(cId,pgId,hospitalId,doctorId,createdDate,mainAnswer,true);
                    });

                    //luon luon tao case cho du 3 case
                    while(checkCase(caseSq) != -1){
                        var csId = checkCase(caseSq);
                        caseSq[csId] = true;
                        caseStudy[csId] = setCaseStudyList(csId,"1",hospitalId,doctorId,"",null,false);
                    }
                    
                    saveAppInfo(hospitalId,doctorId,caseStudy,timeGetList,dr_id);

                });

            }

            //lay list tra loi cau hoi tu server
            function getHistoryList(){
                var ret = {};
                ret['login'] = appFactory._userID;
                ret['password'] = appFactory._userPASS;

                $scope.syncServer("history",ret,function(data){
                    $scope.updateScope($scope);
                    $("#sync-alert-box").bPopup({
                        closeClass:"b-close",
                        speed: 0,
                        onClose:function(){
                            saveHistoryList(data.history_list.history_data);
                            isClickSync = false;
                        }
                    });

                },function(xhr,status,error){
                    errorHandler();
                });

            }

            function getUpdateDataHistoryAndDoctor(){
                var ret = {};
                ret['login'] = appFactory._mainMrId;
                ret['password'] = appFactory._mainPassword;

                $scope.syncServer("update",ret,function(data){
                    updateHospitalAndDoctor(data.facility_list.facility);
                    getHistoryList();
                },function(err){
                    errorHandler();
                });
            }
            //yeu cau delete bang cau hoi da bi xoa o local
            function deleteHistoryList(){
                var ret = getDeletedDoctorAnswers();

                if(ret.count != 0){

                    $scope.syncServer("delete",ret,function(data){
                        cleardoctorHostory();
                        getUpdateDataHistoryAndDoctor();
                    },function(xhr,status,error){
                        errorHandler();
                    });

                }else{
                    getUpdateDataHistoryAndDoctor();
                }

            }

            //gui bang cau tra loi len server
            $scope.doEnquete = function(){
                isClickSync = true;
                $scope.syncServer("enquete",getEnqueteData(),function(data){
                    processServerAnswerList(data);
                    $scope.countSyn = 0;
                    $scope.statusMessage = "同期が完了しました"; // success
                },function(xhr,status,error){
                    $scope.statusMessage = "同期に失敗しました" ; // failed
                    errorHandler();
                });
            };

            function errorHandler(){
                $("#sync-alert-box").bPopup({
                    closeClass:"b-close",
                    speed: 0,
                    onOpen:function(){
                        $scope.updateScope($scope);
                    }
                });
                isClickSync = false;
            }


            $scope.confirmSync = function(event){
                event.preventDefault();

                if(!isClickSync){
                    isOnline(function(status){
                        if(status){
                            $('.content-popup').bPopup({
                                closeClass:"c-close",
                                speed: 0
                            });
                        }else{
                            $("#network").bPopup({
                                closeClass:"b-close",
                                speed: 0
                            });
                        }
                    });
                }

            }
        }
    }
);




