mrcsApp.controller('CaseStudyController',function ($scope, appFactory,$timeout) {

		// Show/hide top buttons
		$scope.isEdity = appFactory._isEdity;
        $scope.pgSelected = "1";

            /*	case study detail*/

	    function getPgList(s) {

	        //PG Order by case study
	        var ordersByCase = s === 2?[2, 0, 1, 3]:[0, 1, 2, 3];

	        //Get the original list
	        var orgList = _.sortBy(appFactory.showPGlist(), function (item) { return item.id; });

	        //Re-order the list by case
            var list = [];
	        for (var i = 0,j = ordersByCase.length; i < j; i++) {
                list[i] = orgList[ordersByCase[i]];
	        }
            return list;
	    }

        $scope.pgList01 = getPgList(1);
        $scope.pgList02 = getPgList(2);

	    $scope.refresh = function() {
            var defaultSelectPg = appFactory._caseStudyId == 6?"0":appFactory._caseStudyId == 4?"2":appFactory._caseStudyId%2 === 0?"3":"1";

            var currentIdPg;

            var currentCase = appFactory._caseStudyFlowList.length > 0?appFactory._caseStudyFlowList[appFactory._caseStudyId - 1]:{};

            currentIdPg = currentCase.isChecked?currentCase.pgId:defaultSelectPg;

	    	if (appFactory._isEdity) {
                $(".btn-case-study-back-top").hide();
			}else{
                $(".btn-case-study-back-top").show();
                //currentIdPg = defaultSelectPg;
			}

            $scope.pgSelected = currentIdPg.toString();
            $scope.updateScope($scope);
            try{
                var curSelectBox = angular.element("#casestudy"+appFactory._caseStudyId+"-page").find("select.pgChoise");
                var text = curSelectBox.find(":selected").text();
                curSelectBox.prev().text(text);
            }catch (e){
                console.log(e);
            }
	    };

		$scope.gotoquest = function(nextPage, caseStudyId){
			appFactory._pgId = $scope.pgSelected;

			// Refresh selected PG
			appFactory.refreshQuestionScopes('question-confirm');
			// Force question-02 scope to refresh
			appFactory.refreshQuestionScopes('2');

            $scope.changePage(nextPage);
		}

        $scope.showPopup = function(caseId,itemIndex){
            angular.element(".popup-case-"+caseId+"-item-"+itemIndex).bPopup({
                closeClass:"close",
                speed: 0
            });
        }
	}
);
