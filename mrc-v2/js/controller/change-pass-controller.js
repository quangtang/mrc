mrcsApp.controller('ChangepassController',
	function ($scope, appFactory){
		$scope.change = {
            username: "",
            password: "",
            newpassword:"",
            repassword:""
        };
        function getEnqueteData() {
            var ret = {};
            ret['login'] = $scope.change.username;
            ret['password'] = $scope.change.password;
            ret['new_password'] = $scope.change.newpassword;
            return ret;
        }
        function changePassword(data,nextPage){
            appFactory._ds.setObject({
                type: gumi.Const.OT_USER,
                id: $scope.change.username,
                password:$scope.change.newpassword,
                remember:false
            });

            $scope.changePage(nextPage);
        }
		$scope.updateUser = function(nextPage){
            if($scope.change.newpassword == $scope.change.repassword){
                isOnline(function(status){
                    //k lam gi het
                    if(status){
                        $scope.syncServer("changePass",
                            getEnqueteData(),
                            function(result){
                                changePassword(result,nextPage);
                            },
                            function(result){
                                $('.content-popup-16').bPopup({
                                    closeClass:"c-close",
                                    onOpen:function(){
                                        $scope.message = result.message?result.message : result.statusText;
                                        $scope.updateScope($scope);
                                    },
                                    speed: 0
                                });
                            }
                        );
                    }else{
                        $("#network").bPopup({
                            closeClass:"b-close",
                            speed: 0
                        });
                    }

                });
            }else{
                $("#login-alert-box").bPopup({
                    closeClass:"b-close",
                    speed: 0
                });
            }

       	}
  });

