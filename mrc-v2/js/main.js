$(document).on("pagebeforeshow",function(){
    $('.popup-01').click(function() {
        $('.content-popup-01').bPopup({
            closeClass:"c-close",
            position: [242, 240],
            onOpen:function(){
                var moveLogin = angular.element("#move").scope();
                moveLogin.CheckLastUserInfo();
            },
            speed: 0,
            follow: [false, false]
        });
    });
    $('.popup-03').click(function() {
        $('.content-popup-03').bPopup({
            closeClass:"close",
            speed: 0
        });
    });
    $('.popup-03-01').click(function() {
        $('.content-popup-03-01').bPopup({
            closeClass:"close",
            speed: 0
        });
    });
    $('.popup-04').click(function() {
        $('.content-popup-04').bPopup({
            closeClass:"close",
            speed: 0
        });
    });

    $('.popup-12').click(function() {
        $('.content-popup-12').bPopup({
            closeClass:"b-close",
            speed: 0
        });
    });
    $('.popup-13').click(function() {
        $('.content-popup-13').bPopup({
            closeClass:"close",
            speed: 0
        });
    });
    $('.popup-11').click(function() {
        $('.content-popup-11').bPopup({
            closeClass:"b-close",
            speed: 0
        });
    });
    $('.popup-14').click(function() {
        $('.content-popup-14').bPopup({
            closeClass:"b-close",
            speed: 0
        });
    });

    $('.btn-gototop-confirm-q1').click(function() {
        $('#confirm-question-gototop-q1').bPopup({
            closeClass:"c-close",
            speed: 0
        });
    });
    $('.btn-gototop-confirm-q2').click(function() {
        $('#confirm-question-gototop-q2').bPopup({
            closeClass:"c-close",
            speed: 0
        });
    });


});
app.initialize();
$(function () {
    if(( /(iphone|ipod)/i.test(navigator.userAgent) )) {
        $("html").css({
            "-webkit-transform":"scale(0.41)",
            "transform":"scale(0.41)",
            "margin":"-94px auto 0 -201px"
        });
    }

    $(":mobile-pagecontainer").on("pagecontainerhide", function (event, ui) {
        var pageId = ui.nextPage[0].id;



        //Push the isShowSyncInfo to the angluar scope of the specified element show-sync-info
        //特定のページの時フッター右下アイコンを表示する
        var syncFooterScope = angular.element($("#show-sync-info")).scope();
        var syncFooterScopeSummary = angular.element($("#show-summary")).scope();

        if(pageId !== "content-summary") syncFooterScopeSummary.resetBackChart(false);

        if (pageId === "index-page") {
            syncFooterScope.isShowSyncInfo = pageId === "index-page";
            syncFooterScopeSummary.isShowSyncSummary = pageId === "index-page";
        } else if (pageId === "data01-page") {
            syncFooterScopeSummary.isShowSyncSummary = pageId === "data01-page";
            syncFooterScopeSummary.resetBackChart(true);
        } else if (pageId === "entry-page") {
            var entryScope = angular.element("#entry-page").scope();
            entryScope.freeHospitalCall();
        } else if (pageId === "content-summary") {
            syncFooterScopeSummary.isShowSyncSummary = false;

        } else if (pageId === "list-page") {
            syncFooterScope.isShowSyncInfo = false;
        }else {
            //表示しないページ
            syncFooterScope.isShowSyncInfo = pageId === "none-page";
            syncFooterScopeSummary.isShowSyncSummary = pageId === "none-page";
        }
        if (!syncFooterScope.$$phase || syncFooterScope.$$phase == "")
            syncFooterScope.$apply();
        if (!syncFooterScopeSummary.$$phase || syncFooterScopeSummary.$$phase == "")
            syncFooterScopeSummary.$apply();


        var currentScope = angular.element("#"+pageId).scope();
        if(typeof currentScope.refresh === "function"){
            if (pageId === '#question01-page' || pageId === '#question02-page') {
                $(".question-group").collapsible({ collapsed: true });
            }else{
                currentScope.refresh();
            }
        }
    });
});
// CSS for Login Info popup
function move(){
    $("#move").css({"position":"fixed","top":"19.5%"});
}

