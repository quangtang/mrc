mrcsApp.controller('CaseController',
	function ($scope, appFactory, $sce) {
        //console.log("CaseController");
		// Show/hide top buttons
		$scope.isEdity = appFactory._isEdity;
		appFactory.cacheQuestionScope('list-case', $scope);
		$scope.refresh = function() {
		    $scope.isEdity = appFactory._isEdity;
            setSelectedAnswer();
		};


        //show case da answer hay chua
        function setSelectedAnswer(){
            $scope.caseSelectList = {
                "case1":false,
                "case2":false,
                "case3":false,
                "case4":false,
                "case5":false,
                "case6":false
            };

            if(appFactory._caseStudyFlowList.length != 0){
                _.each(appFactory._caseStudyFlowList,function(cs){
                        if(cs.answers && cs.answers != null){
                            _.each(cs.answers,function(c){
                                if(c.groupId == 2) $scope.caseSelectList["case"+cs.csId] = true;
                            });

                        }
                });
            }

            $scope.updateScope($scope);
        }


		/* case top */
		//var caseidselected = null;

		$scope.chooseCase = function(nextPage, caseId){
			//Reset the answer group list 'appFactory._answerGroupList = []'
	    	appFactory.resetAnswerGroupList();
			
			//store caseId selected
		    appFactory._caseStudyId = caseId;
            appFactory.__caseStudyId = appFactory._caseStudyId;

            var caseDefault = appFactory.getDefaultCaseStudyFlowList();

            function deleteCaseSelectedNotSubmit(){
                _.each(appFactory._caseStudyFlowList,function(d,i){

                    if(d.csId != caseId && !isEmpty(d.answers) && d.answers.length == 1 && d.answers[0].groupId == 1){
                        appFactory._caseStudyFlowList[i]= _.find(caseDefault,function(dq){
                            return dq.csId == d.csId;
                        });
                    }
                });
            }

            //check truong truong hop tra loi question 1 nhung lai va chon case khac thi question chua hoan thanh nen can xoa di
            deleteCaseSelectedNotSubmit();
            if (appFactory._caseStudyFlowList.length > 0 && appFactory._caseStudyFlowList[appFactory._caseStudyId -1].isChecked) {
                appFactory._answerGroupList = appFactory._caseStudyFlowList[appFactory._caseStudyId -1].answers;
            }

            // Refresh Case Study
            appFactory.refreshQuestionScopes('case-caseStudy');

            //Force question-01 scope to refresh
            appFactory.refreshQuestionScopes('1');

            $scope.changePage(nextPage);
		}
		
	}
);
