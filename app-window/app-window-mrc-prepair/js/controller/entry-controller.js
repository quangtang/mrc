mrcsApp.controller('EntryController',
	function ($scope, appFactory, $timeout) {
        var oldInfo = {};
        //Put all initialize code here
        $scope.refresh = function () {
            //set lai mrId var userId tu trang login, de k phai trung thong tin tu trang info
            appFactory._mrID = appFactory._mainMrId;
            appFactory._userID = appFactory._mainMrId;
            appFactory._userPASS = appFactory._mainPassword;
           
            // get hospital list by Mr ID
            $scope.hospitalList = getHospitalWithoutFree();
            $scope.doctorList = [];
            oldInfo.h = appFactory._hospitalId;
            oldInfo.d = appFactory._doctorId;

            //giu lai lich su selectbox
            $scope.selectedHosptial = "";
            $scope.selectedDoctor = "";

            //thay doi list bac si khi moi lan chon lai benh vien
            $scope.onSelectedHospitalChange = function () {
                $scope.doctorList = getDoctorWithoutFree($scope.selectedHosptial);
                $scope.selectedDoctor = "";
                $scope.freeDoctor.name = "";
                $scope.freeHospital.name = "";
                $timeout(function(){
                    $("#ddldoctor").selectmenu("refresh");
                    $("#entry-page .dropdownlist").css("transform","translate(0)");
                }, 100);
            }

            if($scope.selectedHosptial.length > 0){
                $scope.doctorList = getDoctorWithoutFree($scope.selectedHosptial);
                $scope.updateScope($scope);
            }
            // object
            $scope.freeHospital = {
                type: gumi.Const.OT_HI,
                id:"",
                mrId:appFactory._userID,
                name:"",
                isFree:true

            };
            $scope.freeDoctor = {
                type: gumi.Const.OT_DI,
                id:"",
                hospitalId:"",
                dr_id:"",
                name:"",
                isfreeDoctor:true,
                isFreeHospital:true
            };
        };

        function getHospitalWithoutFree(){
            var hospitals = [];
            _.each(appFactory.getHospitalList(appFactory._userID),function(h){
                if(!h.isFree){
                    hospitals.push(h);
                }
            });
            return hospitals;
        }

        function getDoctorWithoutFree(hId){
            var doctors =[];
            _.each(appFactory.getDoctorList(hId),function(d){
                if(!d.isfreeDoctor){
                    doctors.push(d);
                }
            });
            return doctors;
        }


        function timeId(){
            var today = new Date();
            return today.getTime();
        }

        $scope.onClickNext = function() {
            var nextpage = "#case-page",
                idByTime = timeId();

            //neu truong hop nhap free benh vien
            if ($scope.selectedHosptial.length == 0) {

                $scope.freeDoctor.isFreeHospital = true;
                //clear doctor name from doctor selectbox
                $scope.selectedDoctor = "";

                //neu khong nhap free benh vien co nghia field rong nen show confirm
                if($scope.freeHospital.name.length == 0){
                    $("#confirm-doctor").bPopup({
                        closeClass:"b-close",
                        speed: 0
                    });
                }else{

                    //neu nhap benh vien ma chua nhap bac si cung confirm
                    if($scope.freeDoctor.name.length == 0){
                        $("#confirm-doctor").bPopup({
                            closeClass:"b-close",
                            speed: 0
                        });
                    }else{
                        //tim trong database thu xem co benh vien nay chua
                        var currentHospital = _.find(appFactory.getHospitalList(appFactory._userID),function(hospital){
                            return hospital.name == $scope.freeHospital.name;
                        });

                        // neu benh vien da co san trong database thi chi cap kiem tra bac si trong database
                        if(currentHospital){
                            $scope.freeHospital = currentHospital;
                            //kiem tra bac si trong benh vien o database
                            var currentDoctor1 = _.find(appFactory.getDoctorList($scope.freeHospital.id),function(doctor){
                                return doctor.name == $scope.freeDoctor.name;
                            });


                            if(currentDoctor1){
                                $scope.freeDoctor = currentDoctor1;
                            }else{
                                //neu da co benh vien roi ma bac si chua co trong database thi luu bac si moi
                                $scope.freeDoctor.hospitalId = $scope.freeHospital.id;
                                $scope.freeDoctor.id = $scope.freeDoctor.hospitalId + "_"+ idByTime;
                                $scope.freeDoctor.dr_id = idByTime;
                                appFactory._ds.setObject($scope.freeDoctor);
                            }
                        }else{

                            //luu bac si moi vao database
                            $scope.freeHospital.id = idByTime;
                            appFactory._ds.setObject($scope.freeHospital);

                            //luu thong tin bac si moi vao database
                            $scope.freeDoctor.hospitalId = $scope.freeHospital.id;
                            $scope.freeDoctor.id = $scope.freeDoctor.hospitalId +"_"+ idByTime;
                            $scope.freeDoctor.dr_id = idByTime;
                            appFactory._ds.setObject($scope.freeDoctor);
                        }
                        gotoPage(nextpage,$scope.freeHospital.id,$scope.freeDoctor.id);
                    }
                }
            //truong hop co select benh vien da co trong database
            }else {
                //clear hospital in freeHospital
                $scope.freeHospital.name = "";
                $scope.freeDoctor.isFreeHospital = false;
                if($scope.freeDoctor.name.length == 0 && $scope.selectedDoctor.length == 0){
                    $("#confirm-doctor").bPopup({
                        closeClass:"b-close",
                        speed: 0
                    });
                }else{

                    //truong hop select bac si da co san trong database
                    if($scope.freeDoctor.name.length == 0 && $scope.selectedDoctor.length != 0){
                        gotoPage(nextpage,$scope.selectedHosptial,$scope.selectedDoctor);
                    }else{

                        var currentDoctor = _.find(appFactory.getDoctorList($scope.selectedHosptial),function(doctor){
                            return doctor.name == $scope.freeDoctor.name;
                        });

                        if(currentDoctor){
                            $scope.freeDoctor = currentDoctor;
                        }else{

                            $scope.freeDoctor.hospitalId = $scope.selectedHosptial;
                            $scope.freeDoctor.id = $scope.freeDoctor.hospitalId +"_"+ idByTime;
                            $scope.freeDoctor.dr_id = idByTime;
                            appFactory._ds.setObject($scope.freeDoctor);
                        }
                        gotoPage(nextpage,$scope.selectedHosptial,$scope.freeDoctor.id);
                    }
                }

            }
        };

        $scope.freeHospitalCall = function(){
            $scope.selectedHosptial = "";
            $scope.selectedDoctor = "";
            $timeout(function(){
                $("#ddlhospital,#ddldoctor").selectmenu("refresh");
                $("#entry-page .dropdownlist").css("transform","translate(0)");
            }, 100);
        }

        $scope.freeDoctorCall = function(){
            $scope.selectedDoctor = "";
            $timeout(function(){
                $("#ddldoctor").selectmenu("refresh");
                $("#entry-page .dropdownlist").css("transform","translate(0)");
            }, 100);
        }

        $scope.retrieveDoctorList = function(hospitalId){
            if(hospitalId.length>0){
                $scope.freeDoctor.name = "";
                $scope.updateScope($scope);
            }

        };

        function gotoPage(nextPage,hospitalId,doctorId){

            // Reset the case study flow first
            appFactory.resetFlow();

            //lay appInfo tu database de tiep tuc edit neu co
            var appInfo = _.find(appFactory.getAppInfoList(),function(ai){
                return ai.hospitalId == hospitalId && ai.doctorId == doctorId;
            });

            appFactory._caseStudyFlowList = appInfo?appInfo.caseStudyFlows:[];

            appFactory._hospitalId = hospitalId;
            appFactory._doctorId = doctorId;

            // Reset flow
            appFactory._isEdity = false;
            appFactory.refreshQuestionScopes('list-case');

            $scope.changePage(nextPage);

        }
        $scope.refresh();
	}
);
