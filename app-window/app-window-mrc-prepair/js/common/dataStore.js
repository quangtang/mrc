/// <reference path="consts.ts" />
var gumi = gumi || {};
(function (gumi) {
    /**
    * Dam nhan viec luu tru du lieu tu application xuong database hoac local storage
    */
    gumi.DataStore = (function () {
        function DataStore(appCode, toJson, fromJson) {
            /**
            * The application code. Used to indicate the data come from which application
            */
            this._appCode = "mrcs";
            if (appCode)
                this._appCode = appCode;

            this._toJson = toJson;
            this._fromJson = fromJson;
        }
        DataStore.prototype._set = function (key, value) {
            var jsonValue = this._toJson(value);
            localStorage.setItem(key, jsonValue);
        };

        DataStore.prototype._get = function (key) {
            var sValue = localStorage.getItem(key);
            return this._fromJson(sValue);
        };

        DataStore.prototype._remove = function (key) {
            localStorage.removeItem(key);
        };

        DataStore.prototype._getAll = function (objType, selector) {
            var otKey = this._getKey(objType);

            for (var i = 0; i < localStorage.length; i++) {
                var key = localStorage.key(i);
                if (key.indexOf(otKey) === 0) {
                    var obj = this._get(key);
                    if (selector)
                        selector(obj);
                }
            }
        };
        DataStore.prototype._getKey = function (objType) {
            return this._appCode + gumi.Const.DELI + objType;
        };
        DataStore.prototype.setObject = function (objValue) {
            if (!objValue.type)
                throw 'Cannot save the object without type';

            if (!objValue.id)
                throw 'Cannot save the object without ID';

            var key = this._getKey(objValue.type) + gumi.Const.DELI + objValue.id;
            this._set(key, objValue);
        };
        DataStore.prototype.getObject = function (objType, objId) {
            var key = this._getKey(objType) + gumi.Const.DELI + objId;
            return this._get(key);
        };
        DataStore.prototype.removeObject = function (objType, objId) {
            var key = this._getKey(objType) + gumi.Const.DELI + objId;
            this._remove(key);
        };
        DataStore.prototype.getObjectList = function (objType) {
            var list = [];
            this._getAll(objType, function (obj) {
                list.push(obj);
            });

            return list;
        };
        DataStore.prototype.query = function (objType, filter) {
            var list = [];
            this._getAll(objType, function (obj) {
                if (!filter || filter(obj))
                    list.push(obj);
            });
            return list;
        };
        DataStore.prototype.queryOne = function (objType, filter) {
            var list = this.query(objType, filter);
            if (list && list.length > 0)
                return list[0];
            return null;
        };
        return DataStore;
    })();
})(gumi);
//# sourceMappingURL=dataStore.js.map
