var PDF_SETTINGS = {
    pdfViewerURL: "pdfjs/web/viewer.html?file=",
    pdfFolderURL: "pdf/",
    pdfOpenAtPage: "#page=",
    pdfIFrameURL: "",
    allPDFFiles : [],
    medicines : [
        {
            name : "main",
            type : "main",
            question02 : [
                {
                    mainId : ["2.3"],
                    pdfFilesList : [
                        {
                            filename: "001.pdf",
                            pages: [
                                {
                                    pageNumber: 1,
                                    title: "Dummy title page 001",
                                    desc: "Dummy description page 001	"
                                },
                                {
                                    pageNumber: 2,
                                    title: "Dummy title page 001",
                                    desc: "Dummy description page 001	"
                                }
                            ],
                            mustConditions : ["1.3.1","1.1.3"],
                            diffConditions : ["1.3.2","1.3.3","1.1.2","1.1.4"]
                        },
                        {
                            filename: "002.pdf",
                            pages: [
                                {
                                    pageNumber: 1,
                                    title: "Dummy title page 002",
                                    desc: "Dummy description page 002"
                                }
                            ],
                            mustConditions : ["1.3.1","1.1.3","1.1.2"],
                            diffConditions : []
                        },
                        {
                            filename: "003.pdf",
                            pages: [
                                {
                                    pageNumber: 1,
                                    title: "Dummy title page 003",
                                    desc: "Dummy description page 003"
                                }
                            ],
                            mustConditions : ["1.3.1","1.1.3","1.1.4"],
                            diffConditions : []
                        },
                        {
                            filename: "004.pdf",
                            pages: [
                                {
                                    pageNumber: 1,
                                    title: "Dummy title page 004",
                                    desc: "Dummy description page 004"
                                }
                            ],
                            mustConditions : ["1.3.1","1.1.2"],
                            diffConditions : ["1.3.2","1.3.3","1.1.3","1.1.4"]
                        },
                        {
                            filename: "005.pdf",
                            pages: [
                                {
                                    pageNumber: 1,
                                    title: "Dummy title page 005",
                                    desc: "Dummy description page 005"
                                }
                            ],
                            mustConditions : ["1.3.1","1.1.2","1.1.4"],
                            diffConditions : []
                        },
                        {
                            filename: "006.pdf",
                            pages: [
                                {
                                    pageNumber: 1,
                                    title: "Dummy title page 006",
                                    desc: "Dummy description page 006"
                                }
                            ],
                            mustConditions : ["1.3.1","1.1.4"],
                            diffConditions : ["1.3.2","1.3.3","1.1.2","1.1.3"]
                        },
                        {
                            filename: "008.pdf",
                            pages: [
                                {
                                    pageNumber: 1,
                                    title: "Dummy title page 008",
                                    desc: "Dummy description page 008"
                                }
                            ],
                            mustConditions : ["1.3.1"],
                            diffConditions : ["1.3.2","1.3.3","1.1.2","1.1.4","1.1.3"]
                        },
                        {
                            filename: "009.pdf",
                            pages: [
                                {
                                    pageNumber: 1,
                                    title: "Dummy title page 009",
                                    desc: "Dummy description page 009"
                                }
                            ],
                            mustConditions : ["1.3.1","1.3.2","1.1.3"],
                            diffConditions : []
                        },
                        {
                            filename: "012.pdf",
                            pages: [
                                {
                                    pageNumber: 1,
                                    title: "Dummy title page 012",
                                    desc: "Dummy description page 012"
                                }
                            ],
                            mustConditions : ["1.3.1","1.3.2","1.1.2"],
                            diffConditions : []
                        },
                        {
                            filename: "014.pdf",
                            pages: [
                                {
                                    pageNumber: 1,
                                    title: "Dummy title page 014",
                                    desc: "Dummy description page 014"
                                }
                            ],
                            mustConditions : ["1.3.1","1.3.2","1.1.4"],
                            diffConditions : []
                        },
                        {
                            filename: "016.pdf",
                            pages: [
                                {
                                    pageNumber: 1,
                                    title: "Dummy title page 016",
                                    desc: "Dummy description page 016"
                                }
                            ],
                            mustConditions : ["1.3.1","1.3.2"],
                            diffConditions : ["1.1.3","1.3.3","1.1.2","1.1.4"]
                        },
                        {
                            filename: "017.pdf",
                            pages: [
                                {
                                    pageNumber: 1,
                                    title: "Dummy title page 017",
                                    desc: "Dummy description page 017"
                                }
                            ],
                            mustConditions : ["1.3.1","1.1.3","1.3.3"],
                            diffConditions : []
                        },
                        {
                            filename: "020.pdf",
                            pages: [
                                {
                                    pageNumber: 1,
                                    title: "Dummy title page 020",
                                    desc: "Dummy description page 020"
                                }
                            ],
                            mustConditions : ["1.3.1","1.3.3","1.1.2"],
                            diffConditions : []
                        },
                        {
                            filename: "022.pdf",
                            pages: [
                                {
                                    pageNumber: 1,
                                    title: "Dummy title page 022",
                                    desc: "Dummy description page 022"
                                }
                            ],
                            mustConditions : ["1.3.1","1.3.3","1.1.4"],
                            diffConditions : []
                        },
                        {
                            filename: "024.pdf",
                            pages: [
                                {
                                    pageNumber: 1,
                                    title: "Dummy title page 024",
                                    desc: "Dummy description page 024"
                                }
                            ],
                            mustConditions : ["1.3.1","1.3.3"],
                            diffConditions : ["1.3.2","1.1.3","1.1.2","1.1.4"]
                        },
                        {
                            filename: "025.pdf",
                            pages: [
                                {
                                    pageNumber: 1,
                                    title: "Dummy title page 025",
                                    desc: "Dummy description page 025"
                                }
                            ],
                            mustConditions : ["1.3.2","1.1.3"],
                            diffConditions : ["1.3.1","1.3.3","1.1.2","1.1.4"]
                        },
                        {
                            filename: "026.pdf",
                            pages: [
                                {
                                    pageNumber: 1,
                                    title: "Dummy title page 026",
                                    desc: "Dummy description page 026"
                                }
                            ],
                            mustConditions : ["1.3.2","1.1.3","1.1.2"],
                            diffConditions : []
                        },
                        {
                            filename: "027.pdf",
                            pages: [
                                {
                                    pageNumber: 1,
                                    title: "Dummy title page 027",
                                    desc: "Dummy description page 027"
                                }
                            ],
                            mustConditions : ["1.3.2","1.1.3","1.1.4"],
                            diffConditions : []
                        },
                        {
                            filename: "028.pdf",
                            pages: [
                                {
                                    pageNumber: 1,
                                    title: "Dummy title page 028",
                                    desc: "Dummy description page 028"
                                }
                            ],
                            mustConditions : ["1.3.2","1.1.2"],
                            diffConditions : ["1.3.1","1.3.3","1.1.3","1.1.4"]
                        },
                        {
                            filename: "029.pdf",
                            pages: [
                                {
                                    pageNumber: 1,
                                    title: "Dummy title page 029",
                                    desc: "Dummy description page 029"
                                }
                            ],
                            mustConditions : ["1.3.2","1.1.2","1.1.4"],
                            diffConditions : []
                        },
                        {
                            filename: "030.pdf",
                            pages: [
                                {
                                    pageNumber: 1,
                                    title: "Dummy title page 030",
                                    desc: "Dummy description page 030"
                                }
                            ],
                            mustConditions : ["1.3.2","1.1.4"],
                            diffConditions : ["1.3.1","1.3.3","1.1.2","1.1.3"]
                        },
                        {
                            filename: "032.pdf",
                            pages: [
                                {
                                    pageNumber: 1,
                                    title: "Dummy title page 032",
                                    desc: "Dummy description page 032"
                                }
                            ],
                            mustConditions : ["1.3.2"],
                            diffConditions : ["1.3.1","1.3.3","1.1.2","1.1.4","1.1.3"]
                        },
                        {
                            filename: "033.pdf",
                            pages: [
                                {
                                    pageNumber: 1,
                                    title: "Dummy title page 033",
                                    desc: "Dummy description page 033"
                                }
                            ],
                            mustConditions : ["1.3.3","1.3.2","1.1.3"],
                            diffConditions : []
                        },
                        {
                            filename: "036.pdf",
                            pages: [
                                {
                                    pageNumber: 1,
                                    title: "Dummy title page 036",
                                    desc: "Dummy description page 036"
                                }
                            ],
                            mustConditions : ["1.3.2","1.3.3","1.1.2"],
                            diffConditions : []
                        },
                        {
                            filename: "038.pdf",
                            pages: [
                                {
                                    pageNumber: 1,
                                    title: "Dummy title page 038",
                                    desc: "Dummy description page 038"
                                }
                            ],
                            mustConditions : ["1.3.2","1.3.3","1.1.4"],
                            diffConditions : []
                        },
                        {
                            filename: "040.pdf",
                            pages: [
                                {
                                    pageNumber: 1,
                                    title: "Dummy title page 040",
                                    desc: "Dummy description page 040"
                                }
                            ],
                            mustConditions : ["1.3.2","1.3.3"],
                            diffConditions : ["1.3.1","1.1.3","1.1.2","1.1.4"]
                        },
                        {
                            filename: "041.pdf",
                            pages: [
                                {
                                    pageNumber: 1,
                                    title: "Dummy title page 041",
                                    desc: "Dummy description page 041"
                                }
                            ],
                            mustConditions : ["1.3.3","1.1.3"],
                            diffConditions : ["1.3.2","1.3.1","1.1.2","1.1.4"]
                        },
                        {
                            filename: "042.pdf",
                            pages: [
                                {
                                    pageNumber: 1,
                                    title: "Dummy title page 042",
                                    desc: "Dummy description page 042"
                                }
                            ],
                            mustConditions : ["1.3.3","1.1.3","1.1.2"],
                            diffConditions : []
                        },
                        {
                            filename: "043.pdf",
                            pages: [
                                {
                                    pageNumber: 1,
                                    title: "Dummy title page 043",
                                    desc: "Dummy description page 043"
                                }
                            ],
                            mustConditions : ["1.3.3","1.1.3","1.1.4"],
                            diffConditions : []
                        },
                        {
                            filename: "044.pdf",
                            pages: [
                                {
                                    pageNumber: 1,
                                    title: "Dummy title page 044",
                                    desc: "Dummy description page 044"
                                }
                            ],
                            mustConditions : ["1.3.3","1.1.2"],
                            diffConditions : ["1.3.2","1.3.1","1.1.3","1.1.4"]
                        },
                        {
                            filename: "045.pdf",
                            pages: [
                                {
                                    pageNumber: 1,
                                    title: "Dummy title page 045",
                                    desc: "Dummy description page 045"
                                }
                            ],
                            mustConditions : ["1.3.3","1.1.2","1.1.4"],
                            diffConditions : []
                        },
                        {
                            filename: "046.pdf",
                            pages: [
                                {
                                    pageNumber: 1,
                                    title: "Dummy title page 046",
                                    desc: "Dummy description page 046"
                                }
                            ],
                            mustConditions : ["1.3.3","1.1.4"],
                            diffConditions : ["1.3.2","1.3.1","1.1.2","1.1.3"]
                        },
                        {
                            filename: "048.pdf",
                            pages: [
                                {
                                    pageNumber: 1,
                                    title: "Dummy title page 048",
                                    desc: "Dummy description page 048"
                                }
                            ],
                            mustConditions : ["1.3.3"],
                            diffConditions : ["1.3.2","1.3.1","1.1.2","1.1.4","1.1.3"]
                        },
                        {
                            filename: "056.pdf",
                            pages: [
                                {
                                    pageNumber: 1,
                                    title: "Dummy title page 056",
                                    desc: "Dummy description page 056"
                                }
                            ],
                            mustConditions : ["1.3.1","1.3.3","1.3.2"],
                            diffConditions : []
                        },
                        {
                            filename: "057.pdf",
                            pages: [
                                {
                                    pageNumber: 1,
                                    title: "Dummy title page 056",
                                    desc: "Dummy description page 056"
                                }
                            ],
                            mustConditions : ["1.1.3"],
                            containConditions : ["1.3.4","1.3.5","1.3.6"],
                            diffConditions : ["1.3.1","1.3.2","1.3.3","1.1.2","1.1.4"]
                        },
                        {
                            filename: "058.pdf",
                            pages: [
                                {
                                    pageNumber: 1,
                                    title: "Dummy title page 056",
                                    desc: "Dummy description page 056"
                                }
                            ],
                            mustConditions : ["1.1.3","1.1.2"],
                            containConditions : ["1.3.4","1.3.5","1.3.6"],
                            diffConditions : ["1.3.1","1.3.2","1.3.3","1.1.4"]
                        },
                        {
                            filename: "059.pdf",
                            pages: [
                                {
                                    pageNumber: 1,
                                    title: "Dummy title page 056",
                                    desc: "Dummy description page 056"
                                }
                            ],
                            mustConditions : ["1.1.3","1.1.4"],
                            containConditions : ["1.3.4","1.3.5","1.3.6"],
                            diffConditions : ["1.3.1","1.3.2","1.3.3","1.1.2"]
                        },
                        {
                            filename: "060.pdf",
                            pages: [
                                {
                                    pageNumber: 1,
                                    title: "Dummy title page 056",
                                    desc: "Dummy description page 056"
                                }
                            ],
                            mustConditions : ["1.1.2"],
                            containConditions : ["1.3.4","1.3.5","1.3.6"],
                            diffConditions : ["1.3.1","1.3.2","1.3.3","1.1.3","1.1.4"]
                        },
                        {
                            filename: "061.pdf",
                            pages: [
                                {
                                    pageNumber: 1,
                                    title: "Dummy title page 056",
                                    desc: "Dummy description page 056"
                                }
                            ],
                            mustConditions : ["1.1.2","1.1.4"],
                            containConditions : ["1.3.4","1.3.5","1.3.6"],
                            diffConditions : ["1.3.1","1.3.2","1.3.3","1.1.3"]
                        },
                        {
                            filename: "062.pdf",
                            pages: [
                                {
                                    pageNumber: 1,
                                    title: "Dummy title page 056",
                                    desc: "Dummy description page 056"
                                }
                            ],
                            mustConditions : ["1.1.4"],
                            containConditions : ["1.3.4","1.3.5","1.3.6"],
                            diffConditions : ["1.3.1","1.3.2","1.3.3","1.1.3","1.1.2"]
                        },
                        {
                            filename: "064.pdf",
                            pages: [
                                {
                                    pageNumber: 1,
                                    title: "Dummy title page 056",
                                    desc: "Dummy description page 056"
                                }
                            ],
                            mustConditions : [],
                            containConditions : ["1.3.4","1.3.5","1.3.6"],
                            diffConditions : ["1.3.1","1.3.2","1.3.3","1.1.3","1.1.2","1.1.4"]
                        },
                        {
                            filename: "065.pdf",
                            pages: [
                                {
                                    pageNumber: 1,
                                    title: "Dummy title page 065",
                                    desc: "Dummy description page 065"
                                }
                            ],
                            mustConditions : ["1.1.3"],
                            diffConditions : ["1.3.1","1.3.3","1.3.2","1.3.4","1.3.5","1.3.6","1.1.2","1.1.4"]
                        },
                        {
                            filename: "066.pdf",
                            pages: [
                                {
                                    pageNumber: 1,
                                    title: "Dummy title page 066",
                                    desc: "Dummy description page 066"
                                }
                            ],
                            mustConditions : ["1.1.3","1.1.2"],
                            diffConditions : ["1.3.1","1.3.3","1.3.2","1.3.4","1.3.5","1.3.6","1.1.4"]
                        },
                        {
                            filename: "067.pdf",
                            pages: [
                                {
                                    pageNumber: 1,
                                    title: "Dummy title page 067",
                                    desc: "Dummy description page 067"
                                }
                            ],
                            mustConditions : ["1.1.3","1.1.4"],
                            diffConditions : ["1.3.1","1.3.3","1.3.2","1.3.4","1.3.5","1.3.6","1.1.2"]
                        },
                        {
                            filename: "068.pdf",
                            pages: [
                                {
                                    pageNumber: 1,
                                    title: "Dummy title page 068",
                                    desc: "Dummy description page 068"
                                }
                            ],
                            mustConditions : ["1.1.2"],
                            diffConditions : ["1.3.1","1.3.3","1.3.2","1.3.4","1.3.5","1.3.6","1.1.3","1.1.4"]
                        },
                        {
                            filename: "069.pdf",
                            pages: [
                                {
                                    pageNumber: 1,
                                    title: "Dummy title page 069",
                                    desc: "Dummy description page 069"
                                }
                            ],
                            mustConditions : ["1.1.2","1.1.4"],
                            diffConditions : ["1.3.1","1.3.3","1.3.2","1.3.4","1.3.5","1.3.6","1.1.3"]
                        },
                        {
                            filename: "070.pdf",
                            pages: [
                                {
                                    pageNumber: 1,
                                    title: "Dummy title page 070",
                                    desc: "Dummy description page 070"
                                }
                            ],
                            mustConditions : ["1.1.4"],
                            diffConditions : ["1.3.1","1.3.3","1.3.2","1.3.4","1.3.5","1.3.6","1.1.2","1.1.3"]
                        },
                        {
                            filename: "071.pdf",
                            pages: [
                                {
                                    pageNumber: 1,
                                    title: "Dummy title page 071",
                                    desc: "Dummy description page 071"
                                }
                            ],
                            mustConditions : ["1.1.2","1.1.4","1.1.3"],
                            diffConditions : []
                        },
                        {
                            filename: "072.pdf",
                            pages: [
                                {
                                    pageNumber: 1,
                                    title: "Dummy title page 072",
                                    desc: "Dummy description page 072"
                                }
                            ],
                            mustConditions : [],
                            diffConditions : ["1.3.1","1.3.3","1.3.2","1.1.3","1.1.2","1.1.4","1.3.4","1.3.5","1.3.6"]
                        }
                    ]
                },
                {
                    mainId : ["2.1","2.4","2.7"],
                    pdfFilesList: [
                        {
                            filename: "073.pdf",
                            pages: [
                                {
                                    pageNumber: 1,
                                    title: "Dummy title page 001",
                                    desc: "Dummy description page 001	"
                                }
                            ],
                            mustConditions : ["1.3.1","1.1.3"],
                            diffConditions : ["1.3.2","1.3.3","1.1.2","1.1.4"]
                        },
                        {
                            filename: "074.pdf",
                            pages: [
                                {
                                    pageNumber: 1,
                                    title: "Dummy title page 002",
                                    desc: "Dummy description page 002"
                                }
                            ],
                            mustConditions : ["1.3.1","1.1.3","1.1.2"],
                            diffConditions : []
                        },
                        {
                            filename: "075.pdf",
                            pages: [
                                {
                                    pageNumber: 1,
                                    title: "Dummy title page 003",
                                    desc: "Dummy description page 003"
                                }
                            ],
                            mustConditions : ["1.3.1","1.1.3","1.1.4"],
                            diffConditions : []
                        },
                        {
                            filename: "076.pdf",
                            pages: [
                                {
                                    pageNumber: 1,
                                    title: "Dummy title page 004",
                                    desc: "Dummy description page 004"
                                }
                            ],
                            mustConditions : ["1.3.1","1.1.2"],
                            diffConditions : ["1.3.2","1.3.3","1.1.3","1.1.4"]
                        },
                        {
                            filename: "077.pdf",
                            pages: [
                                {
                                    pageNumber: 1,
                                    title: "Dummy title page 005",
                                    desc: "Dummy description page 005"
                                }
                            ],
                            mustConditions : ["1.3.1","1.1.2","1.1.4"],
                            diffConditions : []
                        },
                        {
                            filename: "078.pdf",
                            pages: [
                                {
                                    pageNumber: 1,
                                    title: "Dummy title page 006",
                                    desc: "Dummy description page 006"
                                }
                            ],
                            mustConditions : ["1.3.1","1.1.4"],
                            diffConditions : ["1.3.2","1.3.3","1.1.2","1.1.3"]
                        },
                        {
                            filename: "080.pdf",
                            pages: [
                                {
                                    pageNumber: 1,
                                    title: "Dummy title page 008",
                                    desc: "Dummy description page 008"
                                }
                            ],
                            mustConditions : ["1.3.1"],
                            diffConditions : ["1.3.2","1.3.3","1.1.2","1.1.4","1.1.3"]
                        },
                        {
                            filename: "081.pdf",
                            pages: [
                                {
                                    pageNumber: 1,
                                    title: "Dummy title page 009",
                                    desc: "Dummy description page 009"
                                }
                            ],
                            mustConditions : ["1.3.1","1.3.2","1.1.3"],
                            diffConditions : []
                        },
                        {
                            filename: "084.pdf",
                            pages: [
                                {
                                    pageNumber: 1,
                                    title: "Dummy title page 012",
                                    desc: "Dummy description page 012"
                                }
                            ],
                            mustConditions : ["1.3.1","1.3.2","1.1.2"],
                            diffConditions : []
                        },
                        {
                            filename: "086.pdf",
                            pages: [
                                {
                                    pageNumber: 1,
                                    title: "Dummy title page 014",
                                    desc: "Dummy description page 014"
                                }
                            ],
                            mustConditions : ["1.3.1","1.3.2","1.1.4"],
                            diffConditions : []
                        },
                        {
                            filename: "088.pdf",
                            pages: [
                                {
                                    pageNumber: 1,
                                    title: "Dummy title page 016",
                                    desc: "Dummy description page 016"
                                }
                            ],
                            mustConditions : ["1.3.1","1.3.2"],
                            diffConditions : ["1.1.3","1.3.3","1.1.2","1.1.4"]
                        },
                        {
                            filename: "089.pdf",
                            pages: [
                                {
                                    pageNumber: 1,
                                    title: "Dummy title page 017",
                                    desc: "Dummy description page 017"
                                }
                            ],
                            mustConditions : ["1.3.1","1.1.3","1.3.3"],
                            diffConditions : []
                        },
                        {
                            filename: "092.pdf",
                            pages: [
                                {
                                    pageNumber: 1,
                                    title: "Dummy title page 020",
                                    desc: "Dummy description page 020"
                                }
                            ],
                            mustConditions : ["1.3.1","1.3.3","1.1.2"],
                            diffConditions : []
                        },
                        {
                            filename: "094.pdf",
                            pages: [
                                {
                                    pageNumber: 1,
                                    title: "Dummy title page 022",
                                    desc: "Dummy description page 022"
                                }
                            ],
                            mustConditions : ["1.3.1","1.3.3","1.1.4"],
                            diffConditions : []
                        },
                        {
                            filename: "096.pdf",
                            pages: [
                                {
                                    pageNumber: 1,
                                    title: "Dummy title page 024",
                                    desc: "Dummy description page 024"
                                }
                            ],
                            mustConditions : ["1.3.1","1.3.3"],
                            diffConditions : ["1.3.2","1.1.3","1.1.2","1.1.4"]
                        },
                        {
                            filename: "097.pdf",
                            pages: [
                                {
                                    pageNumber: 1,
                                    title: "Dummy title page 025",
                                    desc: "Dummy description page 025"
                                }
                            ],
                            mustConditions : ["1.3.2","1.1.3"],
                            diffConditions : ["1.3.1","1.3.3","1.1.2","1.1.4"]
                        },
                        {
                            filename: "098.pdf",
                            pages: [
                                {
                                    pageNumber: 1,
                                    title: "Dummy title page 026",
                                    desc: "Dummy description page 026"
                                }
                            ],
                            mustConditions : ["1.3.2","1.1.3","1.1.2"],
                            diffConditions : []
                        },
                        {
                            filename: "099.pdf",
                            pages: [
                                {
                                    pageNumber: 1,
                                    title: "Dummy title page 027",
                                    desc: "Dummy description page 027"
                                }
                            ],
                            mustConditions : ["1.3.2","1.1.3","1.1.4"],
                            diffConditions : []
                        },
                        {
                            filename: "100.pdf",
                            pages: [
                                {
                                    pageNumber: 1,
                                    title: "Dummy title page 028",
                                    desc: "Dummy description page 028"
                                }
                            ],
                            mustConditions : ["1.3.2","1.1.2"],
                            diffConditions : ["1.3.1","1.3.3","1.1.3","1.1.4"]
                        },
                        {
                            filename: "101.pdf",
                            pages: [
                                {
                                    pageNumber: 1,
                                    title: "Dummy title page 029",
                                    desc: "Dummy description page 029"
                                }
                            ],
                            mustConditions : ["1.3.2","1.1.2","1.1.4"],
                            diffConditions : []
                        },
                        {
                            filename: "102.pdf",
                            pages: [
                                {
                                    pageNumber: 1,
                                    title: "Dummy title page 030",
                                    desc: "Dummy description page 030"
                                }
                            ],
                            mustConditions : ["1.3.2","1.1.4"],
                            diffConditions : ["1.3.1","1.3.3","1.1.2","1.1.3"]
                        },
                        {
                            filename: "104.pdf",
                            pages: [
                                {
                                    pageNumber: 1,
                                    title: "Dummy title page 032",
                                    desc: "Dummy description page 032"
                                }
                            ],
                            mustConditions : ["1.3.2"],
                            diffConditions : ["1.3.1","1.3.3","1.1.2","1.1.4","1.1.3"]
                        },
                        {
                            filename: "105.pdf",
                            pages: [
                                {
                                    pageNumber: 1,
                                    title: "Dummy title page 033",
                                    desc: "Dummy description page 033"
                                }
                            ],
                            mustConditions : ["1.3.3","1.3.2","1.1.3"],
                            diffConditions : []
                        },
                        {
                            filename: "108.pdf",
                            pages: [
                                {
                                    pageNumber: 1,
                                    title: "Dummy title page 036",
                                    desc: "Dummy description page 036"
                                }
                            ],
                            mustConditions : ["1.3.2","1.3.3","1.1.2"],
                            diffConditions : []
                        },
                        {
                            filename: "110.pdf",
                            pages: [
                                {
                                    pageNumber: 1,
                                    title: "Dummy title page 038",
                                    desc: "Dummy description page 038"
                                }
                            ],
                            mustConditions : ["1.3.2","1.3.3","1.1.4"],
                            diffConditions : []
                        },
                        {
                            filename: "112.pdf",
                            pages: [
                                {
                                    pageNumber: 1,
                                    title: "Dummy title page 040",
                                    desc: "Dummy description page 040"
                                }
                            ],
                            mustConditions : ["1.3.2","1.3.3"],
                            diffConditions : ["1.3.1","1.1.3","1.1.2","1.1.4"]
                        },
                        {
                            filename: "113.pdf",
                            pages: [
                                {
                                    pageNumber: 1,
                                    title: "Dummy title page 041",
                                    desc: "Dummy description page 041"
                                }
                            ],
                            mustConditions : ["1.3.3","1.1.3"],
                            diffConditions : ["1.3.2","1.3.1","1.1.2","1.1.4"]
                        },
                        {
                            filename: "114.pdf",
                            pages: [
                                {
                                    pageNumber: 1,
                                    title: "Dummy title page 042",
                                    desc: "Dummy description page 042"
                                }
                            ],
                            mustConditions : ["1.3.3","1.1.3","1.1.2"],
                            diffConditions : []
                        },
                        {
                            filename: "115.pdf",
                            pages: [
                                {
                                    pageNumber: 1,
                                    title: "Dummy title page 043",
                                    desc: "Dummy description page 043"
                                }
                            ],
                            mustConditions : ["1.3.3","1.1.3","1.1.4"],
                            diffConditions : []
                        },
                        {
                            filename: "116.pdf",
                            pages: [
                                {
                                    pageNumber: 1,
                                    title: "Dummy title page 044",
                                    desc: "Dummy description page 044"
                                }
                            ],
                            mustConditions : ["1.3.3","1.1.2"],
                            diffConditions : ["1.3.2","1.3.1","1.1.3","1.1.4"]
                        },
                        {
                            filename: "117.pdf",
                            pages: [
                                {
                                    pageNumber: 1,
                                    title: "Dummy title page 045",
                                    desc: "Dummy description page 045"
                                }
                            ],
                            mustConditions : ["1.3.3","1.1.2","1.1.4"],
                            diffConditions : []
                        },
                        {
                            filename: "118.pdf",
                            pages: [
                                {
                                    pageNumber: 1,
                                    title: "Dummy title page 046",
                                    desc: "Dummy description page 046"
                                }
                            ],
                            mustConditions : ["1.3.3","1.1.4"],
                            diffConditions : ["1.3.2","1.3.1","1.1.2","1.1.3"]
                        },
                        {
                            filename: "120.pdf",
                            pages: [
                                {
                                    pageNumber: 1,
                                    title: "Dummy title page 048",
                                    desc: "Dummy description page 048"
                                }
                            ],
                            mustConditions : ["1.3.3"],
                            diffConditions : ["1.3.2","1.3.1","1.1.2","1.1.4","1.1.3"]
                        },
                        {
                            filename: "128.pdf",
                            pages: [
                                {
                                    pageNumber: 1,
                                    title: "Dummy title page 056",
                                    desc: "Dummy description page 056"
                                }
                            ],
                            mustConditions : ["1.3.1","1.3.3","1.3.2"],
                            diffConditions : []
                        },
                        {
                            filename: "129.pdf",
                            pages: [
                                {
                                    pageNumber: 1,
                                    title: "Dummy title page 056",
                                    desc: "Dummy description page 056"
                                }
                            ],
                            mustConditions : ["1.1.3"],
                            containConditions : ["1.3.4","1.3.5","1.3.6"],
                            diffConditions : ["1.3.1","1.3.2","1.3.3","1.1.2","1.1.4"]
                        },
                        {
                            filename: "130.pdf",
                            pages: [
                                {
                                    pageNumber: 1,
                                    title: "Dummy title page 056",
                                    desc: "Dummy description page 056"
                                }
                            ],
                            mustConditions : ["1.1.3","1.1.2"],
                            containConditions : ["1.3.4","1.3.5","1.3.6"],
                            diffConditions : ["1.3.1","1.3.2","1.3.3","1.1.4"]
                        },
                        {
                            filename: "131.pdf",
                            pages: [
                                {
                                    pageNumber: 1,
                                    title: "Dummy title page 056",
                                    desc: "Dummy description page 056"
                                }
                            ],
                            mustConditions : ["1.1.3","1.1.4"],
                            containConditions : ["1.3.4","1.3.5","1.3.6"],
                            diffConditions : ["1.3.1","1.3.2","1.3.3","1.1.2"]
                        },
                        {
                            filename: "132.pdf",
                            pages: [
                                {
                                    pageNumber: 1,
                                    title: "Dummy title page 056",
                                    desc: "Dummy description page 056"
                                }
                            ],
                            mustConditions : ["1.1.2"],
                            containConditions : ["1.3.4","1.3.5","1.3.6"],
                            diffConditions : ["1.3.1","1.3.2","1.3.3","1.1.3","1.1.4"]
                        },
                        {
                            filename: "133.pdf",
                            pages: [
                                {
                                    pageNumber: 1,
                                    title: "Dummy title page 056",
                                    desc: "Dummy description page 056"
                                }
                            ],
                            mustConditions : ["1.1.2","1.1.4"],
                            containConditions : ["1.3.4","1.3.5","1.3.6"],
                            diffConditions : ["1.3.1","1.3.2","1.3.3","1.1.3"]
                        },
                        {
                            filename: "134.pdf",
                            pages: [
                                {
                                    pageNumber: 1,
                                    title: "Dummy title page 056",
                                    desc: "Dummy description page 056"
                                }
                            ],
                            mustConditions : ["1.1.4"],
                            containConditions : ["1.3.4","1.3.5","1.3.6"],
                            diffConditions : ["1.3.1","1.3.2","1.3.3","1.1.3","1.1.2"]
                        },
                        {
                            filename: "136.pdf",
                            pages: [
                                {
                                    pageNumber: 1,
                                    title: "Dummy title page 056",
                                    desc: "Dummy description page 056"
                                }
                            ],
                            mustConditions : [],
                            containConditions : ["1.3.4","1.3.5","1.3.6"],
                            diffConditions : ["1.3.1","1.3.2","1.3.3","1.1.3","1.1.2","1.1.4"]
                        },
                        {
                            filename: "138.pdf",
                            pages: [
                                {
                                    pageNumber: 1,
                                    title: "Dummy title page 065",
                                    desc: "Dummy description page 065"
                                }
                            ],
                            mustConditions : ["1.1.3"],
                            diffConditions : ["1.3.1","1.3.3","1.3.2","1.3.4","1.3.5","1.3.6","1.1.2","1.1.4"]
                        },
                        {
                            filename: "139.pdf",
                            pages: [
                                {
                                    pageNumber: 1,
                                    title: "Dummy title page 066",
                                    desc: "Dummy description page 066"
                                }
                            ],
                            mustConditions : ["1.1.3","1.1.2"],
                            diffConditions : ["1.3.1","1.3.3","1.3.2","1.3.4","1.3.5","1.3.6","1.1.4"]
                        },
                        {
                            filename: "140.pdf",
                            pages: [
                                {
                                    pageNumber: 1,
                                    title: "Dummy title page 067",
                                    desc: "Dummy description page 067"
                                }
                            ],
                            mustConditions : ["1.1.3","1.1.4"],
                            diffConditions : ["1.3.1","1.3.3","1.3.2","1.3.4","1.3.5","1.3.6","1.1.2"]
                        },
                        {
                            filename: "141.pdf",
                            pages: [
                                {
                                    pageNumber: 1,
                                    title: "Dummy title page 068",
                                    desc: "Dummy description page 068"
                                }
                            ],
                            mustConditions : ["1.1.2"],
                            diffConditions : ["1.3.1","1.3.3","1.3.2","1.3.4","1.3.5","1.3.6","1.1.3","1.1.4"]
                        },
                        {
                            filename: "142.pdf",
                            pages: [
                                {
                                    pageNumber: 1,
                                    title: "Dummy title page 069",
                                    desc: "Dummy description page 069"
                                }
                            ],
                            mustConditions : ["1.1.2","1.1.4"],
                            diffConditions : ["1.3.1","1.3.3","1.3.2","1.3.4","1.3.5","1.3.6","1.1.3"]
                        },
                        {
                            filename: "143.pdf",
                            pages: [
                                {
                                    pageNumber: 1,
                                    title: "Dummy title page 070",
                                    desc: "Dummy description page 070"
                                }
                            ],
                            mustConditions : ["1.1.4"],
                            diffConditions : ["1.3.1","1.3.3","1.3.2","1.3.4","1.3.5","1.3.6","1.1.2","1.1.3"]
                        },
                        {
                            filename: "144.pdf",
                            pages: [
                                {
                                    pageNumber: 1,
                                    title: "Dummy title page 071",
                                    desc: "Dummy description page 071"
                                }
                            ],
                            mustConditions : ["1.1.2","1.1.4","1.1.3"],
                            diffConditions : []
                        },
                        {
                            filename: "145.pdf",
                            pages: [
                                {
                                    pageNumber: 1,
                                    title: "Dummy title page 072",
                                    desc: "Dummy description page 072"
                                }
                            ],
                            mustConditions : [],
                            diffConditions : ["1.3.1","1.3.3","1.3.2","1.1.3","1.1.2","1.1.4","1.3.4","1.3.5","1.3.6"]
                        }
                    ]
                }
            ]
        },
        {
            name : "Other Medicines",
            type : "sub",
            question02 : [
                {
                    mainId : ["2.3"],
                    pdfFilesList : [
                        {
                            filename: "146.pdf",
                            pages: [
                                {
                                    pageNumber: 1,
                                    title: "Dummy title page 146",
                                    desc: "Dummy description page 146"
                                }
                            ],
                            mustConditions : ["1.1.3"],
                            diffConditions : ["1.1.2","1.1.4"]
                        },
                        {
                            filename: "147.pdf",
                            pages: [
                                {
                                    pageNumber: 1,
                                    title: "Dummy title page 147",
                                    desc: "Dummy description page 147"
                                }
                            ],
                            mustConditions : ["1.1.3","1.1.2"],
                            diffConditions : ["1.1.4"]
                        },
                        {
                            filename: "148.pdf",
                            pages: [
                                {
                                    pageNumber: 1,
                                    title: "Dummy title page 148",
                                    desc: "Dummy description page 148"
                                }
                            ],
                            mustConditions : ["1.1.3","1.1.4"],
                            diffConditions : ["1.1.2"]
                        },
                        {
                            filename: "149.pdf",
                            pages: [
                                {
                                    pageNumber: 1,
                                    title: "Dummy title page 149",
                                    desc: "Dummy description page 149"
                                }
                            ],
                            mustConditions : ["1.1.2"],
                            diffConditions : ["1.1.3","1.1.4"]
                        },
                        {
                            filename: "150.pdf",
                            pages: [
                                {
                                    pageNumber: 1,
                                    title: "Dummy title page 150",
                                    desc: "Dummy description page 150"
                                }
                            ],
                            mustConditions : ["1.1.2","1.1.4"],
                            diffConditions : ["1.1.3"]
                        },
                        {
                            filename: "151.pdf",
                            pages: [
                                {
                                    pageNumber: 1,
                                    title: "Dummy title page 151",
                                    desc: "Dummy description page 151"
                                }
                            ],
                            mustConditions : ["1.1.4"],
                            diffConditions : ["1.1.2","1.1.3"]
                        },
                        {
                            filename: "152.pdf",
                            pages: [
                                {
                                    pageNumber: 1,
                                    title: "Dummy title page 151",
                                    desc: "Dummy description page 151"
                                }
                            ],
                            mustConditions : ["1.1.3","1.1.2","1.1.4"],
                            diffConditions : []
                        },
                        {
                            filename: "153.pdf",
                            pages: [
                                {
                                    pageNumber: 1,
                                    title: "Dummy title page 151",
                                    desc: "Dummy description page 151"
                                }
                            ],
                            mustConditions : [],
                            diffConditions : ["1.1.3","1.1.2","1.1.4"]
                        }
                    ]
                },
                {
                    mainId : ["2.4","2.7"],
                    pdfFilesList : [
                        {
                            filename: "154.pdf",
                            pages: [
                                {
                                    pageNumber: 1,
                                    title: "Dummy title page 154",
                                    desc: "Dummy description page 154"
                                }
                            ],
                            mustConditions : ["1.1.3"],
                            diffConditions : ["1.1.2","1.1.4"]
                        },
                        {
                            filename: "155.pdf",
                            pages: [
                                {
                                    pageNumber: 1,
                                    title: "Dummy title page 155",
                                    desc: "Dummy description page 155"
                                }
                            ],
                            mustConditions : ["1.1.3","1.1.2"],
                            diffConditions : ["1.1.4"]
                        },
                        {
                            filename: "156.pdf",
                            pages: [
                                {
                                    pageNumber: 1,
                                    title: "Dummy title page 156",
                                    desc: "Dummy description page 156"
                                }
                            ],
                            mustConditions : ["1.1.3","1.1.4"],
                            diffConditions : ["1.1.2"]
                        },
                        {
                            filename: "157.pdf",
                            pages: [
                                {
                                    pageNumber: 1,
                                    title: "Dummy title page 157",
                                    desc: "Dummy description page 157"
                                }
                            ],
                            mustConditions : ["1.1.2"],
                            diffConditions : ["1.1.3","1.1.4"]
                        },
                        {
                            filename: "158.pdf",
                            pages: [
                                {
                                    pageNumber: 1,
                                    title: "Dummy title page 158",
                                    desc: "Dummy description page 158"
                                }
                            ],
                            mustConditions : ["1.1.2","1.1.4"],
                            diffConditions : ["1.1.3"]
                        },
                        {
                            filename: "159.pdf",
                            pages: [
                                {
                                    pageNumber: 1,
                                    title: "Dummy title page 159",
                                    desc: "Dummy description page 159"
                                }
                            ],
                            mustConditions : ["1.1.4"],
                            diffConditions : ["1.1.2","1.1.3"]
                        },
                        {
                            filename: "160.pdf",
                            pages: [
                                {
                                    pageNumber: 1,
                                    title: "Dummy title page 160",
                                    desc: "Dummy description page 160"
                                }
                            ],
                            mustConditions : ["1.1.3","1.1.2","1.1.4"],
                            diffConditions : []
                        },
                        {
                            filename: "161.pdf",
                            pages: [
                                {
                                    pageNumber: 1,
                                    title: "Dummy title page 161",
                                    desc: "Dummy description page 161"
                                }
                            ],
                            mustConditions : [],
                            diffConditions : ["1.1.3","1.1.2","1.1.4"]
                        }
                    ]
                }
            ]
        },
        {
            name : "Anything",
            type : "any",
            question02 : [
                {
                    mainId: "2.1",
                    pdfFilesList : [
                        {
                            filename: "162.pdf",
                            pages: [
                                {
                                    pageNumber: 1,
                                    title: "Dummy title page 162",
                                    desc: "Dummy description page 162"
                                }
                            ],
                            mustConditions : [],
                            diffConditions : []
                        }
                    ]
                },
                {
                    mainId: "2.2",
                    pdfFilesList : [
                        {
                            filename: "162.pdf",
                            pages: [
                                {
                                    pageNumber: 1,
                                    title: "Dummy title page 162",
                                    desc: "Dummy description page 162"
                                }
                            ],
                            mustConditions : [],
                            diffConditions : []
                        }
                    ]
                },
                {
                    mainId: "2.5",
                    pdfFilesList : [
                        {
                            filename: "163.pdf",
                            pages: [
                                {
                                    pageNumber: 1,
                                    title: "Dummy title page 163",
                                    desc: "Dummy description page 163"
                                }
                            ],
                            mustConditions : [],
                            diffConditions : []
                        }
                    ]
                },
                {
                    mainId: "2.6",
                    pdfFilesList : [
                        {
                            filename: "164.pdf",
                            pages: [
                                {
                                    pageNumber: 1,
                                    title: "Dummy title page 164",
                                    desc: "Dummy description page 164"
                                }
                            ],
                            mustConditions : [],
                            diffConditions : []
                        }
                    ]
                }
            ]
        }
    ]
};
//
//
//// Construct an list of all PDF files
//_.each(PDF_SETTINGS.medicines, function(v, k){
//    PDF_SETTINGS.allPDFFiles.push(_.pluck(v.question02, "pdfFilesList"));
//});
//PDF_SETTINGS.allPDFFiles = _.flatten(PDF_SETTINGS.allPDFFiles);