// Enter full screen
// this variable use for application run on node webkit otherwise not use
var nGui,
    nwWindow;
try{
    nGui = require('nw.gui');
    nwWindow = nGui.Window.get();
    nwWindow.enterFullscreen();
}catch(error){
    console.log(error);
}


// check internet connectivity by the way calling to google site
function isOnline(onSuccess){
    var apiUrl = gumi.Const.SERVER_URL + "login";
    $.ajax({
        type: "POST",
        url: apiUrl,
        data: {},
        success: function(result,status,xhr){
            console.log("Success:"+status);
            onSuccess(true);
        },
        error:function(xhr,status,error){
            console.log("Error:"+status);
            onSuccess(false);
        }
    });
}



//call popup pdf file 'plugin of Mozilla'
function popupPDF(opt){
    var dirPaths = "";
    try{
        var path = require('path');
        var nwPath = process.execPath;
        dirPaths = path.dirname(nwPath) + "/";
    }catch (e){
        console.log(e);
    }

    PDF_SETTINGS.pdfIFrameURL = dirPaths + PDF_SETTINGS.pdfViewerURL + opt.url + PDF_SETTINGS.pdfOpenAtPage + opt.pageNumber;
    opt.scope.pdfIFrameURL = PDF_SETTINGS.pdfIFrameURL;

    /*
     * FIX : Issue on mobile safari,
     * open popup with ifram, when close the popup, the app freezed half screen size
     * Sorry I dont know what the **** Apple did there, so I try to appended the iframe using jQuery,
     * After close popup, remove the fu*king STUPID IFRAME :)
     */

    var iFrameElement = $("<iframe/>", {
        src : opt.scope.pdfIFrameURL
    });
    var pdfPopupID = "#pdf-popup-01";


    $(pdfPopupID).find("iframe").remove();
    iFrameElement.appendTo(pdfPopupID);


    $(pdfPopupID).bPopup({
        closeClass:"b-close",
        speed: 0,
        onClose : function(){
            $(pdfPopupID).find("iframe").remove();
        }
    });
}


// check array and not empty
function isNotEmptyArray(ar){
    return Object.prototype.toString.call(ar) === "[object Array]" && ar.length > 0;
}

//check variable null or empty or undefined
function isEmpty(val){
    return (val === undefined || val == null || val.length <= 0);
}
