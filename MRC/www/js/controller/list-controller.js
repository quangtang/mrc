mrcsApp.controller('ListController',
	function ($scope, appFactory) {
		appFactory.cacheQuestionScope('confirm-list', $scope);

		$scope.deleteAppInfo = function() {
           var doctor = appFactory._ds.getObject(gumi.Const.OT_AI,appFactory._appInfoId);
//            console.log(doctor);
//            console.log(doctor.isServer);
            if(doctor.isServer){
                var doctorClear = {
                    type:gumi.Const.OT_DH,
                    id:doctor.doctorId,
                    idAnswer:[],
                    mrId:appFactory._mrID
                };
                _.each(doctor.caseStudyFlows,function(cs){
                    if(cs.answers){
                        var id = doctorClear.id + "_" + cs.csId;
                        doctorClear.idAnswer.push(id);
                    }
                });

//                console.log(doctorClear);

                appFactory._ds.setObject(doctorClear);
            }

			appFactory.deleteAppInfo(appFactory._appInfoId);
            $scope.appInfoList = _.sortBy(appFactory.getAppInfoList(), function(appInfo){ return -appInfo.createdDate; });
            $scope.updateScope($scope);
		}

        $scope.refresh = function(){
            $scope.appInfoList = _.sortBy(appFactory.getAppInfoList(), function(appInfo){ return -appInfo.createdDate; });
            //console.log("list:",$scope.appInfoList);
            $scope.updateScope($scope);
        }
		
		$scope.editAppInfo = function(appInfo, nextPage) {
			if (appInfo == null || appInfo.caseStudyFlows == null) {
				return;
			}

			appFactory._hospitalId = appInfo.caseStudyFlows[0].hospitalId;
			appFactory._doctorId = appInfo.caseStudyFlows[0].doctorId;
			appFactory._caseStudyFlowList = appInfo.caseStudyFlows;
			appFactory._isEdity = true;

			// Refresh confirm page
			appFactory.refreshQuestionScopes('question-confirm');
			
			// Refresh case page
			appFactory.refreshQuestionScopes('list-case');
			
			// Forward to next question or confirm

            $scope.changePage(nextPage);
		};
		
		$scope.showConfirmPopup = function(appInfoId) {
			appFactory._appInfoId = appInfoId;

			$('.content-popup-15').bPopup({
				closeClass:"b-close",
				speed: 0,
				onClose: function() {

				}
			});
		}
	}
);
