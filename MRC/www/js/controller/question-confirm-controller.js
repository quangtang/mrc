mrcsApp.controller('QuestionConfirmController',

	function ($scope, appFactory) {
		if (appFactory._isEdity) {
			var hospital = appFactory.getHospital(appFactory._hospitalId);
			var doctor = appFactory.getDoctor(appFactory._doctorId);
			var hospitalName = hospital != null ? hospital.name : "";
			var doctorName = doctor != null ? doctor.name : "";
			var pgName = gumi.Const.QC_PG;
			$scope.hospitalName = hospitalName;
			$scope.doctorName = doctorName;
			$scope.pgName = pgName;
		}
		
	    appFactory.cacheQuestionScope('question-confirm', $scope);

		$scope.refresh = function() {
            $scope.isSave = false;
            $scope.isBackButton = appFactory._backButtonConfirm || false;

	        $scope.colon  = gumi.Const.QC_COLON;
            $scope.caseStudyFlows = appFactory.getCaseStudyFlowList();
			$scope.orderCol = 'csId';
			$scope.isEdity = appFactory._isEdity;

			if (appFactory._isEdity) {
				var hospital = appFactory.getHospital(appFactory._hospitalId);
				var doctor = appFactory.getDoctor(appFactory._doctorId);
				var hospitalName = hospital != null ? hospital.name : "";
				var doctorName = doctor != null ? doctor.name : "";
				var pgName = gumi.Const.QC_PG;

				$scope.hospitalName = hospitalName;
				$scope.doctorName = doctorName;
				$scope.pgName = pgName;
			}
            else{
                $scope.caseStudyFlows = _.filter(appFactory.getCaseStudyFlowList(), function(cs){ return cs.csId == appFactory._caseStudyId; });
            }

            sortListQuestion($scope.caseStudyFlows);

		};

        //sort truong hop cau hoi Q1 va Q2 k theo thu tu
        function sortListQuestion(list){
            for(var i = 0; i < list.length;i++){
                if(list[i].answers != null && list[i].answers[0] != null && list[i].answers[1] != null){
                    if(list[i].answers[1].groupId === "1" && list[i].answers[1].answers != null && list[i].answers[1].answers.length >0){
                        var group2 = list[i].answers[0];
                        list[i].answers[0] = list[i].answers[1];
                        list[i].answers[1] = group2;
                        group2 = null;
                    }
                }
            }
        }

		// Save CaseStudyFlow
		$scope.saveAnswer = function() {
            //save all
            var caseStudyFlow = appFactory.getCaseStudyFlowList();
            var isNeedSync = false;
            _.each(caseStudyFlow,function(cs){
                if(cs.answers){
                    if(!cs.isSynced)isNeedSync = true;
                }
            });
            if(isNeedSync){
                var currentDoctor = appFactory.getDoctor(appFactory._doctorId);
                var appInfo = {
                    id: appFactory._mrID + '-' + appFactory._userID + '-' + appFactory._hospitalId + '-' + appFactory._doctorId ,
                    type: gumi.Const.OT_AI,
                    mrId: appFactory._mrID,
                    userId: appFactory._userID,
                    caseStudyFlows: caseStudyFlow,
                    isSynced: false,
                    syncText: "",
                    dr_id:currentDoctor.dr_id,
                    hospitalId: appFactory._hospitalId,
                    doctorId: appFactory._doctorId,
                    createdDate:(new Date()).getTime(),
                    isSave:true
                };

                var currentAppInfo = appFactory._ds.getObject(gumi.Const.OT_AI,appInfo.id);

                if(currentAppInfo && currentAppInfo.isServer){
                    appInfo.isServer = true;
                }else{
                    appInfo.isServer = false;
                }

                appFactory.saveAnswer(appInfo);
            }
			appFactory.refreshQuestionScopes('confirm-data01');

			//Forward to data01 or list
			var nextPage = '#data01-page';
			if (appFactory._isEdity) {
				nextPage = '#list-page';
			}
            localStorage.setItem("isSave","1");
            $scope.gotoPage(nextPage);
		};
		
		$scope.backQuestion = function(nextPage) {
			//Remove answer-2
			var index = appFactory._answerGroupList.length - 1;
			appFactory.removeAnswerGroup(index);
            $scope.gotoPage(nextPage);
		};

        $scope.btnGotoTopConfirm = function(){
            var isSave = localStorage.getItem("isSave");
            if(isSave == "0"){
                $('#confirm-question-gototop').bPopup({
                    closeClass:"c-close",
                    speed: 0
                });
            }else{
                $scope.gotoPage("#index-page");
            }
        };

        $scope.confirmBackEdit = function(nextPage){
            var isSave = localStorage.getItem("isSave");
            if (isSave == "1"){
                $scope.gotoPage(nextPage);
            }else{
                $('#confirm-back-edit').bPopup({
                    closeClass:"b-close",
                    speed: 0
                });
            }
        };

		$scope.gotoCase = function(nextPage) {

            $scope.saveAnswer();

			//Forward to case
            $scope.gotoPage(nextPage);
		};

        $scope.agreeClear = function(){
            //clear tat ca cau tra loi
            appFactory.resetAnswerGroupList();
            //neu da luu vao database roi thi xoa trong database
            appFactory.deleteAppInfo(appFactory._mrID + '-' + appFactory._userID + '-' + appFactory._hospitalId + '-' + appFactory._doctorId );
            $scope.gotoPage( "#index-page");
        };

        $scope.gotoPage = function(nextPage) {
            //Forward to case

            $scope.changePage(nextPage);
        };

        $scope.refresh();

	}
	
);