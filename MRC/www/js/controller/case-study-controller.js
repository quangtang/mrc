mrcsApp.controller('CaseStudyController',
	function ($scope, appFactory, $sce) {


		// Show/hide top buttons
		$scope.isEdity = appFactory._isEdity;
		
	    /*	case study detail*/
	    function getPgList(caseId) {
	        if (!caseId)
	            return null;

	        //PG Order by case study
	        var ordersByCase = [];
	        ordersByCase['1'] = [0, 1, 2, 3];
	        ordersByCase['2'] = [2, 0, 1, 3];
	        ordersByCase['3'] = [0, 1, 2, 3];

	        //Get the original list
	        var orgList = appFactory.showPGlist();
	        orgList = _.sortBy(orgList, function (item) { return item.id; });

	        var orders = ordersByCase[caseId];

	        //Re-order the list by case
	        var list = [];
	        for (var i = 0; i < orders.length; i++) {
	            list[i] = orgList[orders[i]];
	        }
	        return list;
	    }

	    $scope.pglist = getPgList();


	    
	    // Set selected PG
	    appFactory.cacheQuestionScope('case-caseStudy', $scope);

	    $scope.refresh = function() {
          
	    	// Show/hide top buttons
			if (appFactory._isEdity) {
				$("#div-casestudy1-top").hide();
				$("#div-casestudy2-top").hide();
				$("#div-casestudy3-top").hide();
			} else {
				$("#div-casestudy1-top").show();
				$("#div-casestudy2-top").show();
				$("#div-casestudy3-top").show();
			}

	    	// From list.html flow
	    	if (appFactory._isEdity) {
	    		var dfPg = '';
				switch(appFactory._caseStudyId)
				{
					case '1':
						if (appFactory._caseStudyFlowList[0].isChecked) {
							dfPg = appFactory._caseStudyFlowList[0].pgId;
						} else {
							dfPg = gumi.Const.DF_PI_01;
						}						
						$scope.pg1 = dfPg;
						// Refresh UI element
						$("#pgList-1").selectmenu(); // Initializes
						$("#pgList-1").val(dfPg).selectmenu("refresh", true);
						break;
					case '2':
						if (appFactory._caseStudyFlowList[1].isChecked) {
							dfPg = appFactory._caseStudyFlowList[1].pgId;
						} else {
							dfPg = gumi.Const.DF_PI_02;
						}
						$scope.pg2 = dfPg;
						// Refresh UI element
						$("#pgList-2").selectmenu(); // Initializes
						$("#pgList-2").val(dfPg).selectmenu("refresh", true);
						break;
					case '3':
						if (appFactory._caseStudyFlowList[2].isChecked) {
							dfPg = appFactory._caseStudyFlowList[2].pgId;
						} else {
							dfPg = gumi.Const.DF_PI_03;
						}
						$scope.pg3 = dfPg;
						// Refresh UI element
						$("#pgList-3").selectmenu(); // Initializes
						$("#pgList-3").val(dfPg).selectmenu("refresh", true);
						break;
					default:
						break;
				}

			} else { // From entry.html flow
				switch(appFactory._caseStudyId)
				{
					case '1':
						$scope.pg1 = gumi.Const.DF_PI_01;
						// Refresh UI element
						$("#pgList-1").selectmenu(); // Initializes
						$("#pgList-1").val($scope.pg1).selectmenu("refresh", true);
						break;
					case '2':
						$scope.pg2 = gumi.Const.DF_PI_02;
						// Refresh UI element
						$("#pgList-2").selectmenu(); // Initializes
						$("#pgList-2").val($scope.pg2).selectmenu("refresh", true);
						break;
					case '3':
						$scope.pg3 = gumi.Const.DF_PI_03;
						// Refresh UI element
						$("#pgList-3").selectmenu(); // Initializes
						$("#pgList-3").val($scope.pg3).selectmenu("refresh", true);
						break;
					default:
						break;
				}
			}
	    };
	    
		$scope.showCaseDetail = function(caseidselected){
		    $scope.pglist = getPgList(caseidselected);

		    $scope.casedetail = appFactory.showCaseData(caseidselected);
			$scope.imagelist = $scope.casedetail.imageList;
			
			//display data case study
			$scope.diagnosis1 = $sce.trustAsHtml($scope.casedetail.diagnosis1);
			$scope.diagnosis2 = $sce.trustAsHtml($scope.casedetail.diagnosis2);
			$scope.age = $sce.trustAsHtml($scope.casedetail.age);
			$scope.gender = $sce.trustAsHtml($scope.casedetail.gender);
			$scope.complication = $sce.trustAsHtml($scope.casedetail.complication);
			$scope.eyesight = $sce.trustAsHtml($scope.casedetail.eyesight);
			$scope.centralCornealThickness = $sce.trustAsHtml($scope.casedetail.centralCornealThickness);
			$scope.intraocularPressure = $sce.trustAsHtml($scope.casedetail.intraocularPressure);
		}

		
		// Go to question page
		var indexquestselected = 0;
		$scope.gotoquest = function(nextPage, caseStudyId){
			// Set selected PG belong to case study
			var pgId = "1";
			switch(caseStudyId)
			{
				case 1:
					pgId = $("#pgList-1").val();
					break;
				case 2:
					pgId = $("#pgList-2").val();
					break;
				case 3:
					pgId = $("#pgList-3").val();
					break;
				default:
					break;
			}
			appFactory._pgId = pgId;

			// Refresh selected PG
			appFactory.refreshQuestionScopes('question-confirm');
			// Force question-02 scope to refresh
			appFactory.refreshQuestionScopes('2');

            $scope.changePage(nextPage);
		}
	}
);
