mrcsApp.controller('QuestionController',
	function ($scope, appFactory) {
		// Show/hide top buttons
		$scope.isEdity = appFactory._isEdity;

		// Set default question
		$scope.questionGroupIndex = "1";

        $scope.questionGroup = null;
		
        // To be called outside the controller
        $scope.refresh = function () {
            appFactory._backButtonConfirm = false;
        	// Show/hide top buttons
			$scope.isEdity = appFactory._isEdity;
			
			// Set Question by index
            $scope.questionGroup = appFactory.getQuestionGroup($scope.questionGroupIndex);
//            console.log(" $scope.questionGroup", $scope.questionGroup);
//            console.log("appFactory._caseStudyFlowList", appFactory._caseStudyFlowList);
//            console.log("appFactory._answerGroupList", appFactory._answerGroupList);
           $scope.updateScope($scope);
        };

		// Set Question by index
		$scope.setQuestionGroup = function (index) {
		    // Cache this instance to the application
		    appFactory.cacheQuestionScope(index, $scope);

		    $scope.questionGroupIndex = index;
		};

		/**
		 * Checked existed free text in group question
		 */
		function checkedExistedFreeTextInGroup(questionGroup) {
			var flag = false;
        	_.each(questionGroup.children, function (qm) {
            	_.each(qm.children, function(qs) {
	    			if (isNotEmptyFreeText(qs)) {
	    				flag = true;
	    				return;
	    			}
	        	});
			});
			return flag;
		}

		/**
		 * Checked existed free text in main question
		 */
		function checkedExistedFreeTextInMain(qm) {
			var flag = false;
			_.each(qm.children, function(qs) {
    			if (isNotEmptyFreeText(qs)) {
    				flag = true;
    				return;
    			}
        	});
        	return flag;
		}

		/**
         * Check/uncheck main question
         */
        function clickMainQuestion(qm, isChecked) {
        	qm.isChecked = isChecked;
            _.each(qm.children, function(qs) {
                qs.isChecked = isChecked;
            });
        }

        // Check/uncheck main question
        $scope.checkedQuestionMain = function(qmId) {
            // Use underscore to manipulate the collection
            _.each($scope.questionGroup.children, function (qm) {
				if (!$scope.questionGroup.isRadio) { // Checkbox
					if (qm.id !== qmId) {
						return;
					}
                    // Selected main quesiton
                    if (qm.isChecked) { // Uncheck if checked
                    	// Cannot uncheck if existed freeText
                    	var existedFreeText = checkedExistedFreeTextInMain(qm);
                    	if (existedFreeText) {
                    		return;
                    	}
                		// Uncheck main question
                    	clickMainQuestion(qm, false);
                    }
				} else { // Radio
					if (qm.id === qmId) { // Selected main quesiton
						// Cannot uncheck
						if (qm.isChecked) {
							return;
						}
						// Check if group have no sub questions or group has only 1 free text
						if (qm.children.length == 0 || (qm.children.length == 1 && qm.children[0].isFreeText)) {
							// Check main question
							clickMainQuestion(qm, true);
						}
					} else { // Others questions
						if (qm.isChecked) { // Uncheck if checked
							// Cannot uncheck if existed freeText
							var existedFreeText = checkedExistedFreeTextInGroup($scope.questionGroup);
							if (existedFreeText) {
								return;
							}
							// Uncheck main question
							clickMainQuestion(qm, false);
						}
					}
				}
			});
        }

        /**
         * Count of selected sub questions in group
         */
        function countSubQuestionInGroup(questionGroup) {
        	var count = 0;
        	_.each(questionGroup.children, function (qm) {
            	_.each(qm.children, function(qs) {
            		if (qs.isChecked) {
            			++count;
            		}
            	});
            	if (questionGroup.isRadio && qm.children.length == 0 && qm.isChecked) {
            		++count;
            	}
	        });

	        return count;
        }

        /**
         * Count checked sub questions in main question
         */
        function countCheckedSubQuestion(qm) {
        	var count = 0;
        	_.each(qm.children, function(qs) {
            	if (qs.isChecked) {
            		++count;
            	}
            });
            return count;
        }

        /**
         * Check if freeText is not empty
         */
        function isNotEmptyFreeText(qs) {
        	return qs.isFreeText && qs.freeText !== "" && qs.freeText !== undefined;
        }

        /**
         * Is Check action?
         */
        function isCheckAction(questionGroup, qmId, qsId) {
        	var flag = false;
        	_.each(questionGroup.children, function (qm) {
            	_.each(qm.children, function(qs) {
            		if (qm.id === qmId && qs.id === qsId) {
            			flag = qs.isChecked;
            		}
            	});
	        });
	        return !flag;
        }

        /**
         * Clear freeText of sub question
         */
        function clearFreeText(questionGroup, qmId, qsId) {
        	_.each(questionGroup.children, function (qm) {
            	_.each(qm.children, function(qs) {
            		if (qm.id === qmId && qs.id === qsId) {
            			if (isNotEmptyFreeText(qs)) {
            				qs.freeText = "";
            			}
            		}
            	});
	        });
        }

        /**
         * Check max sub question choices
         */
        function checkMaxSubQuestion(questionGroup, qmId, qsId) {
        	var flag = false;
    		var maxChoices = questionGroup.maxChoice;

    		var subCount = countSubQuestionInGroup(questionGroup);
    		var checkFlag = isCheckAction(questionGroup, qmId, qsId);
    		if (subCount == maxChoices && checkFlag) {
    			flag = true;
    		}
        	return flag;
        }

        // The user ticked on this question
        $scope.checkedQuestionSub = function(qmId, qsId) {
        	// Check max sub question choices
        	if (!$scope.questionGroup.isRadio) { // Check
        		var flag = checkMaxSubQuestion($scope.questionGroup, qmId, qsId);
        		if (flag) {
        			clearFreeText($scope.questionGroup, qmId, qsId);
	        		$('.content-popup-11').bPopup();
	        		return;
	        	}
	        }

	        // Cannot check if existed freeText in radio group
	        var existedFreeText = checkedExistedFreeTextInGroup($scope.questionGroup);

            // Use underscore to manipulate the collection
            _.each($scope.questionGroup.children, function (qm) {
                if (qm.id === qmId) { // Current main question
                	if (!$scope.questionGroup.isRadio) { // Check
                		// Allow choose sub questions more than 1
                        _.each(qm.children, function(qs) {
                        	if (qs.id === qsId) {
                        		if (!qs.isFreeText) { // No freeText
	                        		qs.isChecked = !qs.isChecked;
	                        	} else { // FreeText
		                        	qs.isChecked = isNotEmptyFreeText(qs) ? true : false;
	                        	}
                        	}
                        });

                        // Check main question if existed selected sub question
                        var total = countCheckedSubQuestion(qm);
                    	qm.isChecked = total == 0 ? false: true;
                	} else { // Radio
						_.each(qm.children, function(qs) {
							if (!qs.isFreeText) { // No freeText
								// Cann't check if existed free text
								if (existedFreeText) {
									return;
								}
								// Check selected sub question
								qs.isChecked = qs.id === qsId;
								qm.isChecked = true;

							} else {
								// Check if freeText is not empty
								if (isNotEmptyFreeText(qs)) {
	                        		qs.isChecked = true;
	                            	qm.isChecked = true;
	                        	} else {
	                            	qs.isChecked = false;
	                        		qm.isChecked = false;
	                            }
	                        }
                        });
					}
                } else { // Others main question
                	if ($scope.questionGroup.isRadio) { // Radio
                		// Cannot check if existed freeText
                		if (existedFreeText) {
                			return;
                		}
                		// Uncheck main question
                		clickMainQuestion(qm, false);
                	}
                }
            });
        };


        // Next button
		$scope.validateAndSubmit = function(nextPage) {
			// Check required answer
			var count = countSubQuestionInGroup($scope.questionGroup);

            //bat buoc tra loi cau hoi 2 xong moi co the next
			if (count == 0 && $scope.questionGroup.index == "2") {
				$('.content-popup-14').bPopup();
				return;
			}

            //khong tra loi cau hoi 1 cung duoc phep next
            if (count == 0 && $scope.questionGroup.index == "1") {
                appFactory.removeAnswerGroup(0);
                // Refresh confirm page
                appFactory.refreshQuestionScopes('question-confirm');
                $scope.gotoPage(nextPage);
                return;
            }

			// Collect all answer
			var answerList = [];
			_.each($scope.questionGroup.children,
			    function (qm) {
			        // If group no have sub questions
			        if (qm.children.length == 0 && qm.isChecked) {
			            answerList.push({
		    			    mainId: qm.id,
		    			    mainDesc: qm.description
			    		});
			        }

			        // Else group have sub questions
					_.each(qm.children, function(qs) {
						if (qs.isChecked) {
						    answerList.push({
							    mainId: qm.id,
							    subId: qs.id,
							    mainDesc: qm.description,
							    subDesc: qs.description,
							    isFreeText: qs.isFreeText,
							    freeText: qs.freeText
							});
						}
				    });
			    }
			);

			// Packed to the answerData object
			var answerData = {
				groupId: $scope.questionGroup.index,
				answers: answerList
			};


			// Set to the application
			appFactory.submitAnswer(answerData);

			// Get current date
			var currentDate = $scope.getCurrentDate();

			// Set Case Study Flow List
			var caseStudyFlow = {
				id: appFactory._userID + '-' + appFactory._hospitalId + '-' + appFactory._doctorId,
				type: gumi.Const.OT_CF,
				csId: appFactory._caseStudyId,
				hospitalId: appFactory._hospitalId,
		        doctorId: appFactory._doctorId,
				pgId: appFactory._pgId,
				createdDate: currentDate,
		        isSynced: false,
				isChecked: true,
				answers: appFactory.getAnswerGroupList(),
		        pgName: "",
		        syncText: ""
			};

            localStorage.setItem("isSave","0");
			appFactory.submitCaseStudyFlow(caseStudyFlow);
			// Refresh confirm page
			appFactory.refreshQuestionScopes('question-confirm');
            $scope.gotoPage(nextPage);
		};

		// Question01: Back button
		$scope.gotoCaseStudy = function() {
			var nextPage = "#casestudy"+appFactory._caseStudyId+"-page";
            $scope.gotoPage(nextPage);
		};

        $scope.agreeClear = function(){
            appFactory.resetAnswerGroupList();
            $scope.gotoPage("#index-page");
        };

        $scope.gotoPage = function(nextPage) {
            $scope.changePage(nextPage);
        };
	}
);

