mrcsApp.controller("dataPageController", ["$scope", "appFactory",
    function($scope, appFactory){
        $scope.pdfFilesList = [
            {
                "title":"TPRラバー＆合剤ラバー版",
                "filename":"database01.pdf"
            },
            {
                "title":"TPRラバー＆合剤ニュートラル版",
                "filename":"database02.pdf"
            },
            {
                "title":"TPRニュートラル＆合剤ラバー版",
                "filename":"database03.pdf"
            },
            {
                "title":"TPRニュートラル＆合剤ニュートラル版",
                "filename":"database04.pdf"
            },
            {
                "title":"経過観察およびPG単剤切り替え版",
                "filename":"database05.pdf"
            },
            {
                "title":"CAI・α選択版",
                "filename":"database06.pdf"
            },
            {
                "title":"β/CAI配合剤選択版",
                "filename":"database07.pdf"
            }
        ];


        $scope.popupPdf = function(filename, pageNumber){
            var opt = {
                url: "/database_pdf/" + filename,
                pageNumber: pageNumber
            };

            //console.log(opt);


            GumiServices.execAction(function(){
                console.log("success");
            }, function(){
                console.log("error");
            }, "readPDF", opt);
        }
    }
]);