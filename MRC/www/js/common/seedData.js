﻿/// <reference path="consts.ts" />
var gumi;
(function (gumi) {
    var SeedData = (function () {
        function SeedData() {
        }
        SeedData.getSeedData = function () {
            var data = [
                // Users
//                { type: gumi.Const.OT_USER, id: "000001", password: "password" },
//                { type: gumi.Const.OT_USER, id: "000002", password: "password" },
//                { type: gumi.Const.OT_USER, id: "000003", password: "password" },
//                { type: gumi.Const.OT_USER, id: "000004", password: "password" },
//                { type: gumi.Const.OT_USER, id: "000005", password: "password" },
                // Questions of Case Study 1
                // Question group 1
                {
                    type: gumi.Const.OT_QG, id: "1", index: "1", caseStudyId: "1", description: gumi.Const.QG_01_DESC, isRadio: false, maxChoice: 3,
                    // Child main questions
                    children: [
                        {
                            type: gumi.Const.OT_QM, id: "1.1", description: gumi.Const.QG_01_QM_11_DESC, isChecked: false, isPg: false, count: "",
                            // Child sub questions
                            children: [
                                { type: gumi.Const.OT_QS, id: "1.1.1", description: gumi.Const.QG_01_QM_11_QS_111_DESC, isFreeText: false, isChecked: false },
                                { type: gumi.Const.OT_QS, id: "1.1.2", description: gumi.Const.QG_01_QM_11_QS_112_DESC, isFreeText: false, isChecked: false },
                                { type: gumi.Const.OT_QS, id: "1.1.3", description: gumi.Const.QG_01_QM_11_QS_113_DESC, isFreeText: false, isChecked: false },
                                { type: gumi.Const.OT_QS, id: "1.1.4", description: gumi.Const.QG_01_QM_11_QS_114_DESC, isFreeText: false, isChecked: false },
                                { type: gumi.Const.OT_QS, id: "1.1.5", description: gumi.Const.QG_01_QM_11_QS_115_DESC, isFreeText: true, isChecked: false }
                            ]
                        },
                        {
                            type: gumi.Const.OT_QM, id: "1.2", description: gumi.Const.QG_01_QM_12_DESC, isChecked: false, isPg: false,
                            // Child sub questions
                            children: [
                                { type: gumi.Const.OT_QS, id: "1.2.1", description: gumi.Const.QG_01_QM_12_QS_121_DESC, isFreeText: false, isChecked: false },
                                { type: gumi.Const.OT_QS, id: "1.2.2", description: gumi.Const.QG_01_QM_12_QS_122_DESC, isFreeText: false, isChecked: false },
                                { type: gumi.Const.OT_QS, id: "1.2.3", description: gumi.Const.QG_01_QM_12_QS_123_DESC, isFreeText: false, isChecked: false },
                                { type: gumi.Const.OT_QS, id: "1.2.4", description: gumi.Const.QG_01_QM_12_QS_124_DESC, isFreeText: true, isChecked: false }
                            ]
                        },
                        {
                            type: gumi.Const.OT_QM, id: "1.3", description: gumi.Const.QG_01_QM_13_DESC, isChecked: false, isPg: false,
                            // Child sub questions
                            children: [
                                { type: gumi.Const.OT_QS, id: "1.3.1", description: gumi.Const.QG_01_QM_13_QS_131_DESC, isFreeText: false, isChecked: false },
                                { type: gumi.Const.OT_QS, id: "1.3.2", description: gumi.Const.QG_01_QM_13_QS_132_DESC, isFreeText: false, isChecked: false },
                                { type: gumi.Const.OT_QS, id: "1.3.3", description: gumi.Const.QG_01_QM_13_QS_133_DESC, isFreeText: false, isChecked: false },
                                { type: gumi.Const.OT_QS, id: "1.3.4", description: gumi.Const.QG_01_QM_13_QS_134_DESC, isFreeText: false, isChecked: false },
                                { type: gumi.Const.OT_QS, id: "1.3.5", description: gumi.Const.QG_01_QM_13_QS_135_DESC, isFreeText: false, isChecked: false },
                                { type: gumi.Const.OT_QS, id: "1.3.6", description: gumi.Const.QG_01_QM_13_QS_136_DESC, isFreeText: true, isChecked: false }
                            ]
                        },
                        {
                            type: gumi.Const.OT_QM, id: "1.4", description: gumi.Const.QG_01_QM_14_DESC, isChecked: false, isPg: false,
                            // Child sub questions
                            children: [
                                { type: gumi.Const.OT_QS, id: "1.4.1", description: gumi.Const.QG_01_QM_14_QS_141_DESC, isFreeText: false, isChecked: false }
                            ]
                        },
                        {
                            type: gumi.Const.OT_QM, id: "1.5", description: gumi.Const.QG_01_QM_15_DESC, isChecked: false, isPg: false,
                            // Child sub questions
                            children: [
                                { type: gumi.Const.OT_QS, id: "1.5.1", description: gumi.Const.QG_01_QM_15_QS_151_DESC, isFreeText: false, isChecked: false },
                                { type: gumi.Const.OT_QS, id: "1.5.2", description: gumi.Const.QG_01_QM_15_QS_152_DESC, isFreeText: false, isChecked: false },
                                { type: gumi.Const.OT_QS, id: "1.5.3", description: gumi.Const.QG_01_QM_15_QS_153_DESC, isFreeText: true, isChecked: false }
                            ]
                        }
                    ]
                },
                // Question group 2
                {
                    type: gumi.Const.OT_QG, id: "2", index: "2", caseStudyId: "1", description: gumi.Const.QG_02_DESC, isRadio: true, maxChoice: 1,
                    // Child main questions
                    children: [
                        {
                            type: gumi.Const.OT_QM, id: "2.1", description: gumi.Const.QG_02_QM_21_DESC, isChecked: false, isPg: false,
                            // Child sub questions
                            children: []
                        },
                        {
                            type: gumi.Const.OT_QM, id: "2.2", description: gumi.Const.QG_02_QM_22_DESC, isChecked: false, isPg: true,
                            // Child sub questions
                            children: [
                                { type: gumi.Const.OT_QS, id: "2.2.1", description: gumi.Const.QG_02_QM_22_QS_221_DESC, isFreeText: false, isChecked: false },
                                { type: gumi.Const.OT_QS, id: "2.2.2", description: gumi.Const.QG_02_QM_22_QS_222_DESC, isFreeText: false, isChecked: false },
                                { type: gumi.Const.OT_QS, id: "2.2.3", description: gumi.Const.QG_02_QM_22_QS_223_DESC, isFreeText: false, isChecked: false },
                                { type: gumi.Const.OT_QS, id: "2.2.4", description: gumi.Const.QG_02_QM_22_QS_224_DESC, isFreeText: false, isChecked: false }

                            ]
                        },
                        {
                            type: gumi.Const.OT_QM, id: "2.3", description: gumi.Const.QG_02_QM_23_DESC, isChecked: false, isPg: false,
                            // Child sub questions
                            children: [
                                { type: gumi.Const.OT_QS, id: "2.3.1", description: gumi.Const.QG_02_QM_23_QS_231_DESC, isFreeText: false, isChecked: false },
                                { type: gumi.Const.OT_QS, id: "2.3.2", description: gumi.Const.QG_02_QM_23_QS_232_DESC, isFreeText: false, isChecked: false }
                            ]
                        },
                        {
                            type: gumi.Const.OT_QM, id: "2.4", description: gumi.Const.QG_02_QM_24_DESC, isChecked: false, isPg: false,
                            // Child sub questions
                            children: [
                                { type: gumi.Const.OT_QS, id: "2.4.1", description: gumi.Const.QG_02_QM_24_QS_241_DESC, isFreeText: false, isChecked: false },
                                { type: gumi.Const.OT_QS, id: "2.4.2", description: gumi.Const.QG_02_QM_24_QS_242_DESC, isFreeText: false, isChecked: false },
                                { type: gumi.Const.OT_QS, id: "2.4.3", description: gumi.Const.QG_02_QM_24_QS_243_DESC, isFreeText: false, isChecked: false },
                                { type: gumi.Const.OT_QS, id: "2.4.4", description: gumi.Const.QG_02_QM_24_QS_244_DESC, isFreeText: false, isChecked: false }
                            ]
                        },
                        {
                            type: gumi.Const.OT_QM, id: "2.5", description: gumi.Const.QG_02_QM_25_DESC, isChecked: false, isPg: false,
                            // Child sub questions
                            children: [
                                { type: gumi.Const.OT_QS, id: "2.5.1", description: gumi.Const.QG_02_QM_25_QS_251_DESC, isFreeText: false, isChecked: false },
                                { type: gumi.Const.OT_QS, id: "2.5.2", description: gumi.Const.QG_02_QM_25_QS_252_DESC, isFreeText: false, isChecked: false },
                                { type: gumi.Const.OT_QS, id: "2.5.3", description: gumi.Const.QG_02_QM_25_QS_253_DESC, isFreeText: false, isChecked: false },
                                { type: gumi.Const.OT_QS, id: "2.5.4", description: gumi.Const.QG_02_QM_25_QS_254_DESC, isFreeText: false, isChecked: false }
                            ]
                        },
                        {
                            type: gumi.Const.OT_QM, id: "2.6", description: gumi.Const.QG_02_QM_26_DESC, isChecked: false, isPg: false,
                            // Child sub questions
                            children: [
                                { type: gumi.Const.OT_QS, id: "2.6.1", description: gumi.Const.QG_02_QM_26_QS_261_DESC, isFreeText: false, isChecked: false },
                                { type: gumi.Const.OT_QS, id: "2.6.2", description: gumi.Const.QG_02_QM_26_QS_262_DESC, isFreeText: false, isChecked: false }
                            ]
                        },
                        {
                            type: gumi.Const.OT_QM, id: "2.7", description: gumi.Const.QG_02_QM_27_DESC, isChecked: false, isPg: false,
                            // Child sub questions
                            children: [
                                { type: gumi.Const.OT_QS, id: "2.7.1", description: "", isFreeText: true, isChecked: false }
                            ]
                        }
                    ]
                },
                // Question of Case Study 2
                // Question group 1
                {
                    type: gumi.Const.OT_QG, id: "3", index: "1", caseStudyId: "2", description: gumi.Const.QG_01_DESC, isRadio: false, maxChoice: 3,
                    // Child main questions
                    children: [
                        {
                            type: gumi.Const.OT_QM, id: "1.1", description: gumi.Const.QG_01_QM_11_DESC, isChecked: false, isPg: false,
                            // Child sub questions
                            children: [
                                { type: gumi.Const.OT_QS, id: "1.1.1", description: gumi.Const.QG_01_QM_11_QS_111_DESC, isFreeText: false, isChecked: false },
                                { type: gumi.Const.OT_QS, id: "1.1.2", description: gumi.Const.QG_01_QM_11_QS_112_DESC, isFreeText: false, isChecked: false },
                                { type: gumi.Const.OT_QS, id: "1.1.3", description: gumi.Const.QG_01_QM_11_QS_113_DESC, isFreeText: false, isChecked: false },
                                { type: gumi.Const.OT_QS, id: "1.1.4", description: gumi.Const.QG_01_QM_11_QS_114_DESC, isFreeText: false, isChecked: false },
                                { type: gumi.Const.OT_QS, id: "1.1.5", description: gumi.Const.QG_01_QM_11_QS_115_DESC, isFreeText: true, isChecked: false }
                            ]
                        },
                        {
                            type: gumi.Const.OT_QM, id: "1.2", description: gumi.Const.QG_01_QM_12_DESC, isChecked: false, isPg: false,
                            // Child sub questions
                            children: [
                                { type: gumi.Const.OT_QS, id: "1.2.1", description: gumi.Const.QG_01_QM_12_QS_121_DESC, isFreeText: false, isChecked: false },
                                { type: gumi.Const.OT_QS, id: "1.2.2", description: gumi.Const.QG_01_QM_12_QS_122_DESC, isFreeText: false, isChecked: false },
                                { type: gumi.Const.OT_QS, id: "1.2.3", description: gumi.Const.QG_01_QM_12_QS_123_DESC, isFreeText: false, isChecked: false },
                                { type: gumi.Const.OT_QS, id: "1.2.4", description: gumi.Const.QG_01_QM_12_QS_124_DESC, isFreeText: true, isChecked: false }
                            ]
                        },
                        {
                            type: gumi.Const.OT_QM, id: "1.3", description: gumi.Const.QG_01_QM_13_DESC, isChecked: false, isPg: false,
                            // Child sub questions
                            children: [
                                { type: gumi.Const.OT_QS, id: "1.3.1", description: gumi.Const.QG_01_QM_13_QS_131_DESC, isFreeText: false, isChecked: false },
                                { type: gumi.Const.OT_QS, id: "1.3.2", description: gumi.Const.QG_01_QM_13_QS_132_DESC, isFreeText: false, isChecked: false },
                                { type: gumi.Const.OT_QS, id: "1.3.3", description: gumi.Const.QG_01_QM_13_QS_133_DESC, isFreeText: false, isChecked: false },
                                { type: gumi.Const.OT_QS, id: "1.3.4", description: gumi.Const.QG_01_QM_13_QS_134_DESC, isFreeText: false, isChecked: false },
                                { type: gumi.Const.OT_QS, id: "1.3.5", description: gumi.Const.QG_01_QM_13_QS_135_DESC, isFreeText: false, isChecked: false },
                                { type: gumi.Const.OT_QS, id: "1.3.6", description: gumi.Const.QG_01_QM_13_QS_136_DESC, isFreeText: true, isChecked: false }
                            ]
                        },
                        {
                            type: gumi.Const.OT_QM, id: "1.4", description: gumi.Const.QG_01_QM_14_DESC, isChecked: false, isPg: false,
                            // Child sub questions
                            children: [
                                { type: gumi.Const.OT_QS, id: "1.4.1", description: gumi.Const.QG_01_QM_14_QS_141_DESC, isFreeText: false, isChecked: false }
                            ]
                        },
                        {
                            type: gumi.Const.OT_QM, id: "1.5", description: gumi.Const.QG_01_QM_15_DESC, isChecked: false, isPg: false,
                            // Child sub questions
                            children: [
                                { type: gumi.Const.OT_QS, id: "1.5.1", description: gumi.Const.QG_01_QM_15_QS_151_DESC, isFreeText: false, isChecked: false },
                                { type: gumi.Const.OT_QS, id: "1.5.2", description: gumi.Const.QG_01_QM_15_QS_152_DESC, isFreeText: false, isChecked: false },
                                { type: gumi.Const.OT_QS, id: "1.5.3", description: gumi.Const.QG_01_QM_15_QS_153_DESC, isFreeText: true, isChecked: false }
                            ]
                        }
                    ]
                },
                // Question group 2
                {
                    type: gumi.Const.OT_QG, id: "4", index: "2", caseStudyId: "2", description: gumi.Const.QG_02_DESC, isRadio: true, maxChoice: 1,
                    // Child main questions
                    children: [
                        {
                            type: gumi.Const.OT_QM, id: "2.1", description: gumi.Const.QG_02_QM_21_DESC, isChecked: false, isPg: false,
                            // Child sub questions
                            children: []
                        },
                        {
                            type: gumi.Const.OT_QM, id: "2.2", description: gumi.Const.QG_02_QM_22_DESC, isChecked: false, isPg: true,
                            // Child sub questions
                            children: [
                                { type: gumi.Const.OT_QS, id: "2.2.1", description: gumi.Const.QG_02_QM_22_QS_221_DESC, isFreeText: false, isChecked: false },
                                { type: gumi.Const.OT_QS, id: "2.2.2", description: gumi.Const.QG_02_QM_22_QS_222_DESC, isFreeText: false, isChecked: false },
                                { type: gumi.Const.OT_QS, id: "2.2.3", description: gumi.Const.QG_02_QM_22_QS_223_DESC, isFreeText: false, isChecked: false },
                                { type: gumi.Const.OT_QS, id: "2.2.4", description: gumi.Const.QG_02_QM_22_QS_224_DESC, isFreeText: false, isChecked: false }
                            ]
                        },
                        {
                            type: gumi.Const.OT_QM, id: "2.3", description: gumi.Const.QG_02_QM_23_DESC, isChecked: false, isPg: false,
                            // Child sub questions
                            children: [
                                { type: gumi.Const.OT_QS, id: "2.3.1", description: gumi.Const.QG_02_QM_23_QS_231_DESC, isFreeText: false, isChecked: false },
                                { type: gumi.Const.OT_QS, id: "2.3.2", description: gumi.Const.QG_02_QM_23_QS_232_DESC, isFreeText: false, isChecked: false }
                            ]
                        },
                        {
                            type: gumi.Const.OT_QM, id: "2.4", description: gumi.Const.QG_02_QM_24_DESC, isChecked: false, isPg: false,
                            // Child sub questions
                            children: [
                                { type: gumi.Const.OT_QS, id: "2.4.1", description: gumi.Const.QG_02_QM_24_QS_241_DESC, isFreeText: false, isChecked: false },
                                { type: gumi.Const.OT_QS, id: "2.4.2", description: gumi.Const.QG_02_QM_24_QS_242_DESC, isFreeText: false, isChecked: false },
                                { type: gumi.Const.OT_QS, id: "2.4.3", description: gumi.Const.QG_02_QM_24_QS_243_DESC, isFreeText: false, isChecked: false },
                                { type: gumi.Const.OT_QS, id: "2.4.4", description: gumi.Const.QG_02_QM_24_QS_244_DESC, isFreeText: false, isChecked: false }
                            ]
                        },
                        {
                            type: gumi.Const.OT_QM, id: "2.5", description: gumi.Const.QG_02_QM_25_DESC, isChecked: false, isPg: false,
                            // Child sub questions
                            children: [
                                { type: gumi.Const.OT_QS, id: "2.5.1", description: gumi.Const.QG_02_QM_25_QS_251_DESC, isFreeText: false, isChecked: false },
                                { type: gumi.Const.OT_QS, id: "2.5.2", description: gumi.Const.QG_02_QM_25_QS_252_DESC, isFreeText: false, isChecked: false },
                                { type: gumi.Const.OT_QS, id: "2.5.3", description: gumi.Const.QG_02_QM_25_QS_253_DESC, isFreeText: false, isChecked: false },
                                { type: gumi.Const.OT_QS, id: "2.5.4", description: gumi.Const.QG_02_QM_25_QS_254_DESC, isFreeText: false, isChecked: false }
                            ]
                        },
                        {
                            type: gumi.Const.OT_QM, id: "2.6", description: gumi.Const.QG_02_QM_26_DESC, isChecked: false, isPg: false,
                            // Child sub questions
                            children: [
                                { type: gumi.Const.OT_QS, id: "2.6.1", description: gumi.Const.QG_02_QM_26_QS_261_DESC, isFreeText: false, isChecked: false },
                                { type: gumi.Const.OT_QS, id: "2.6.2", description: gumi.Const.QG_02_QM_26_QS_262_DESC, isFreeText: false, isChecked: false }
                            ]
                        },
                        {
                            type: gumi.Const.OT_QM, id: "2.7", description: gumi.Const.QG_02_QM_27_DESC, isChecked: false, isPg: false,
                            // Child sub questions
                            children: [
                                { type: gumi.Const.OT_QS, id: "2.7.1", description: "", isFreeText: true, isChecked: false }
                            ]
                        }
                    ]
                },
                // Question of Case Study 3
                // Question group 1
                {
                    type: gumi.Const.OT_QG, id: "5", index: "1", caseStudyId: "3", description: gumi.Const.QG_01_DESC, isRadio: false, maxChoice: 3,
                    // Child main questions
                    children: [
                        {
                            type: gumi.Const.OT_QM, id: "1.1", description: gumi.Const.QG_01_QM_11_DESC, isChecked: false,
                            // Child sub questions
                            children: [
                                { type: gumi.Const.OT_QS, id: "1.1.1", description: gumi.Const.QG_01_QM_11_QS_111_DESC, isFreeText: false, isChecked: false },
                                { type: gumi.Const.OT_QS, id: "1.1.2", description: gumi.Const.QG_01_QM_11_QS_112_DESC, isFreeText: false, isChecked: false },
                                { type: gumi.Const.OT_QS, id: "1.1.3", description: gumi.Const.QG_01_QM_11_QS_113_DESC, isFreeText: false, isChecked: false },
                                { type: gumi.Const.OT_QS, id: "1.1.4", description: gumi.Const.QG_01_QM_11_QS_114_DESC, isFreeText: false, isChecked: false },
                                { type: gumi.Const.OT_QS, id: "1.1.5", description: gumi.Const.QG_01_QM_11_QS_115_DESC, isFreeText: true, isChecked: false }
                            ]
                        },
                        {
                            type: gumi.Const.OT_QM, id: "1.2", description: gumi.Const.QG_01_QM_12_DESC, isChecked: false,
                            // Child sub questions
                            children: [
                                { type: gumi.Const.OT_QS, id: "1.2.1", description: gumi.Const.QG_01_QM_12_QS_121_DESC, isFreeText: false, isChecked: false },
                                { type: gumi.Const.OT_QS, id: "1.2.2", description: gumi.Const.QG_01_QM_12_QS_122_DESC, isFreeText: false, isChecked: false },
                                { type: gumi.Const.OT_QS, id: "1.2.3", description: gumi.Const.QG_01_QM_12_QS_123_DESC, isFreeText: false, isChecked: false },
                                { type: gumi.Const.OT_QS, id: "1.2.4", description: gumi.Const.QG_01_QM_12_QS_124_DESC, isFreeText: true, isChecked: false }
                            ]
                        },
                        {
                            type: gumi.Const.OT_QM, id: "1.3", description: gumi.Const.QG_01_QM_13_DESC, isChecked: false,
                            // Child sub questions
                            children: [
                                { type: gumi.Const.OT_QS, id: "1.3.1", description: gumi.Const.QG_01_QM_13_QS_131_DESC, isFreeText: false, isChecked: false },
                                { type: gumi.Const.OT_QS, id: "1.3.2", description: gumi.Const.QG_01_QM_13_QS_132_DESC, isFreeText: false, isChecked: false },
                                { type: gumi.Const.OT_QS, id: "1.3.3", description: gumi.Const.QG_01_QM_13_QS_133_DESC, isFreeText: false, isChecked: false },
                                { type: gumi.Const.OT_QS, id: "1.3.4", description: gumi.Const.QG_01_QM_13_QS_134_DESC, isFreeText: false, isChecked: false },
                                { type: gumi.Const.OT_QS, id: "1.3.5", description: gumi.Const.QG_01_QM_13_QS_135_DESC, isFreeText: false, isChecked: false },
                                { type: gumi.Const.OT_QS, id: "1.3.6", description: gumi.Const.QG_01_QM_13_QS_136_DESC, isFreeText: true, isChecked: false }
                            ]
                        },
                        {
                            type: gumi.Const.OT_QM, id: "1.4", description: gumi.Const.QG_01_QM_14_DESC, isChecked: false,
                            // Child sub questions
                            children: [
                                { type: gumi.Const.OT_QS, id: "1.4.1", description: gumi.Const.QG_01_QM_14_QS_141_DESC, isFreeText: false, isChecked: false }
                            ]
                        },
                        {
                            type: gumi.Const.OT_QM, id: "1.5", description: gumi.Const.QG_01_QM_15_DESC, isChecked: false,
                            // Child sub questions
                            children: [
                                { type: gumi.Const.OT_QS, id: "1.5.1", description: gumi.Const.QG_01_QM_15_QS_151_DESC, isFreeText: false, isChecked: false },
                                { type: gumi.Const.OT_QS, id: "1.5.2", description: gumi.Const.QG_01_QM_15_QS_152_DESC, isFreeText: false, isChecked: false },
                                { type: gumi.Const.OT_QS, id: "1.5.3", description: gumi.Const.QG_01_QM_15_QS_153_DESC, isFreeText: true, isChecked: false }
                            ]
                        }
                    ]
                },
                // Question group 2
                {
                    type: gumi.Const.OT_QG, id: "6", index: "2", caseStudyId: "3", description: gumi.Const.QG_02_DESC, isRadio: true, maxChoice: 1,
                    // Child main questions
                    children: [
                        {
                            type: gumi.Const.OT_QM, id: "2.1", description: gumi.Const.QG_02_QM_21_DESC, isChecked: false, isPg: false,
                            // Child sub questions
                            children: []
                        },
                        {
                            type: gumi.Const.OT_QM, id: "2.2", description: gumi.Const.QG_02_QM_22_DESC, isChecked: false, isPg: true,
                            // Child sub questions
                            children: [
                                { type: gumi.Const.OT_QS, id: "2.2.1", description: gumi.Const.QG_02_QM_22_QS_221_DESC, isFreeText: false, isChecked: false },
                                { type: gumi.Const.OT_QS, id: "2.2.2", description: gumi.Const.QG_02_QM_22_QS_222_DESC, isFreeText: false, isChecked: false },
                                { type: gumi.Const.OT_QS, id: "2.2.3", description: gumi.Const.QG_02_QM_22_QS_223_DESC, isFreeText: false, isChecked: false },
                                { type: gumi.Const.OT_QS, id: "2.2.4", description: gumi.Const.QG_02_QM_22_QS_224_DESC, isFreeText: false, isChecked: false }
                            ]
                        },
                        {
                            type: gumi.Const.OT_QM, id: "2.3", description: gumi.Const.QG_02_QM_23_DESC, isChecked: false, isPg: false,
                            // Child sub questions
                            children: [
                                { type: gumi.Const.OT_QS, id: "2.3.1", description: gumi.Const.QG_02_QM_23_QS_231_DESC, isFreeText: false, isChecked: false },
                                { type: gumi.Const.OT_QS, id: "2.3.2", description: gumi.Const.QG_02_QM_23_QS_232_DESC, isFreeText: false, isChecked: false }
                            ]
                        },
                        {
                            type: gumi.Const.OT_QM, id: "2.4", description: gumi.Const.QG_02_QM_24_DESC, isChecked: false, isPg: false,
                            // Child sub questions
                            children: [
                                { type: gumi.Const.OT_QS, id: "2.4.1", description: gumi.Const.QG_02_QM_24_QS_241_DESC, isFreeText: false, isChecked: false },
                                { type: gumi.Const.OT_QS, id: "2.4.2", description: gumi.Const.QG_02_QM_24_QS_242_DESC, isFreeText: false, isChecked: false },
                                { type: gumi.Const.OT_QS, id: "2.4.3", description: gumi.Const.QG_02_QM_24_QS_243_DESC, isFreeText: false, isChecked: false },
                                { type: gumi.Const.OT_QS, id: "2.4.4", description: gumi.Const.QG_02_QM_24_QS_244_DESC, isFreeText: false, isChecked: false }
                            ]
                        },
                        {
                            type: gumi.Const.OT_QM, id: "2.5", description: gumi.Const.QG_02_QM_25_DESC, isChecked: false, isPg: false,
                            // Child sub questions
                            children: [
                                { type: gumi.Const.OT_QS, id: "2.5.1", description: gumi.Const.QG_02_QM_25_QS_251_DESC, isFreeText: false, isChecked: false },
                                { type: gumi.Const.OT_QS, id: "2.5.2", description: gumi.Const.QG_02_QM_25_QS_252_DESC, isFreeText: false, isChecked: false },
                                { type: gumi.Const.OT_QS, id: "2.5.3", description: gumi.Const.QG_02_QM_25_QS_253_DESC, isFreeText: false, isChecked: false },
                                { type: gumi.Const.OT_QS, id: "2.5.4", description: gumi.Const.QG_02_QM_25_QS_254_DESC, isFreeText: false, isChecked: false }
                            ]
                        },
                        {
                            type: gumi.Const.OT_QM, id: "2.6", description: gumi.Const.QG_02_QM_26_DESC, isChecked: false, isPg: false,
                            // Child sub questions
                            children: [
                                { type: gumi.Const.OT_QS, id: "2.6.1", description: gumi.Const.QG_02_QM_26_QS_261_DESC, isFreeText: false, isChecked: false },
                                { type: gumi.Const.OT_QS, id: "2.6.2", description: gumi.Const.QG_02_QM_26_QS_262_DESC, isFreeText: false, isChecked: false }
                            ]
                        },
                        {
                            type: gumi.Const.OT_QM, id: "2.7", description: gumi.Const.QG_02_QM_27_DESC, isChecked: false, isPg: false,
                            // Child sub questions
                            children: [
                                { type: gumi.Const.OT_QS, id: "2.7.1", description: "", isFreeText: true, isChecked: false }
                            ]
                        }
                    ]
                },
                // CASE STUDIES
                // CASE STUDY 01
                {
                    type: gumi.Const.OT_CS,
                    id: "1",
                    name: gumi.Const.CS_01_NAME,
                    diagnosis1: gumi.Const.CS_01_DIAGNOSIS1,
                    diagnosis2: gumi.Const.CS_01_DIAGNOSIS2,
                    eyesight: gumi.Const.CS_01_EYESIGHT,
                    centralCornealThickness: gumi.Const.CS_01_CENTRALCORNEALTHICKNESS,
                    intraocularPressure: gumi.Const.CS_01_INTRAOCULARPRESSURE,
                    age: gumi.Const.CS_01_AGE,
                    gender: gumi.Const.CS_01_GENDER,
                    complication: gumi.Const.CS_01_COMPLICATION,
                    imageList: [
                        {
                            type: gumi.Const.OT_II,
                            id: "1",
                            title: gumi.Const.CS_01_II_01_TITLE,
                            path: gumi.Const.CS_01_II_01_PATH,
                            // Can be text or HTML
                            description: gumi.Const.CS_01_II_01_DESCRIPTION,
                            thumbPath: gumi.Const.CS_01_II_01_THUMBPATH
                        },
                        {
                            type: gumi.Const.OT_II,
                            id: "2",
                            title: gumi.Const.CS_01_II_02_TITLE,
                            path: gumi.Const.CS_01_II_02_PATH,
                            // Can be text or HTML
                            description: gumi.Const.CS_01_II_02_DESCRIPTION,
                            thumbPath: gumi.Const.CS_01_II_02_THUMBPATH
                        }
                    ]
                },
                // CASE STUDY 02
                {
                    type: gumi.Const.OT_CS,
                    id: "2",
                    name: gumi.Const.CS_02_NAME,
                    diagnosis1: gumi.Const.CS_02_DIAGNOSIS1,
                    diagnosis2: gumi.Const.CS_02_DIAGNOSIS2,
                    eyesight: gumi.Const.CS_02_EYESIGHT,
                    centralCornealThickness: gumi.Const.CS_02_CENTRALCORNEALTHICKNESS,
                    intraocularPressure: gumi.Const.CS_02_INTRAOCULARPRESSURE,
                    age: gumi.Const.CS_02_AGE,
                    gender: gumi.Const.CS_02_GENDER,
                    complication: gumi.Const.CS_02_COMPLICATION,
                    imageList: [
                        {
                            type: gumi.Const.OT_II,
                            id: "3",
                            title: gumi.Const.CS_02_II_01_TITLE,
                            path: gumi.Const.CS_02_II_01_PATH,
                            //Can be text or HTML
                            description: gumi.Const.CS_02_II_01_DESCRIPTION,
                            thumbPath: gumi.Const.CS_02_II_01_THUMBPATH
                        },
                        {
                            type: gumi.Const.OT_II,
                            id: "4",
                            title: gumi.Const.CS_02_II_02_TITLE,
                            path: gumi.Const.CS_02_II_02_PATH,
                            //Can be text or HTML
                            description: gumi.Const.CS_02_II_02_DESCRIPTION,
                            thumbPath: gumi.Const.CS_02_II_02_THUMBPATH
                        },
                        {
                            type: gumi.Const.OT_II,
                            id: "5",
                            title: gumi.Const.CS_02_II_03_TITLE,
                            path: gumi.Const.CS_02_II_03_PATH,
                            //Can be text or HTML
                            description: gumi.Const.CS_02_II_03_DESCRIPTION,
                            thumbPath: gumi.Const.CS_02_II_03_THUMBPATH
                        }
                    ]
                },
                // CASE STUDY 03
                {
                    type: gumi.Const.OT_CS,
                    id: "3",
                    name: gumi.Const.CS_03_NAME,
                    diagnosis1: gumi.Const.CS_03_DIAGNOSIS1,
                    diagnosis2: gumi.Const.CS_03_DIAGNOSIS2,
                    eyesight: gumi.Const.CS_03_EYESIGHT,
                    centralCornealThickness: gumi.Const.CS_03_CENTRALCORNEALTHICKNESS,
                    intraocularPressure: gumi.Const.CS_03_INTRAOCULARPRESSURE,
                    age: gumi.Const.CS_03_AGE,
                    gender: gumi.Const.CS_03_GENDER,
                    complication: gumi.Const.CS_03_COMPLICATION,
                    imageList: [
                        {
                            type: gumi.Const.OT_II,
                            id: "6",
                            title: gumi.Const.CS_03_II_01_TITLE,
                            path: gumi.Const.CS_03_II_01_PATH,
                            //Can be text or HTML
                            description: gumi.Const.CS_03_II_01_DESCRIPTION,
                            thumbPath: gumi.Const.CS_03_II_01_THUMBPATH
                        },
                        {
                            type: gumi.Const.OT_II,
                            id: "7",
                            title: gumi.Const.CS_03_II_02_TITLE,
                            path: gumi.Const.CS_03_II_02_PATH,
                            //Can be text or HTML
                            description: gumi.Const.CS_03_II_02_DESCRIPTION,
                            thumbPath: gumi.Const.CS_03_II_02_THUMBPATH
                        },
                        {
                            type: gumi.Const.OT_II,
                            id: "8",
                            title: gumi.Const.CS_03_II_03_TITLE,
                            path: gumi.Const.CS_03_II_03_PATH,
                            //Can be text or HTML
                            description: gumi.Const.CS_03_II_03_DESCRIPTION,
                            thumbPath: gumi.Const.CS_03_II_03_THUMBPATH
                        }
                    ]
                },
                // PG Info
                { type: gumi.Const.OT_PI, id: "1", name: gumi.Const.PI_01_NAME },
                { type: gumi.Const.OT_PI, id: "2", name: gumi.Const.PI_02_NAME },
                { type: gumi.Const.OT_PI, id: "3", name: gumi.Const.PI_03_NAME },
                { type: gumi.Const.OT_PI, id: "4", name: gumi.Const.PI_04_NAME }
                // Hospital list
//                { type: gumi.Const.OT_HI, id: "331896", mrId: "000001", name: gumi.Const.HI_01_NAME },
//                { type: gumi.Const.OT_HI, id: "313464", mrId: "000001", name: gumi.Const.HI_02_NAME },
//                { type: gumi.Const.OT_HI, id: "463877", mrId: "000001", name: gumi.Const.HI_03_NAME },
//                { type: gumi.Const.OT_HI, id: "538314", mrId: "000001", name: gumi.Const.HI_04_NAME },
//                { type: gumi.Const.OT_HI, id: "162693", mrId: "000001", name: gumi.Const.HI_05_NAME },
//                { type: gumi.Const.OT_HI, id: "549774", mrId: "000002", name: gumi.Const.HI_06_NAME },
//                { type: gumi.Const.OT_HI, id: "232850", mrId: "000002", name: gumi.Const.HI_07_NAME },
//                { type: gumi.Const.OT_HI, id: "166666", mrId: "000002", name: gumi.Const.HI_08_NAME },
//                { type: gumi.Const.OT_HI, id: "427195", mrId: "000003", name: gumi.Const.HI_09_NAME },
//                // Doctor list
//                { type: gumi.Const.OT_DI, id: "01305975", hospitalId: "331896", name: gumi.Const.DI_01_NAME },
//                { type: gumi.Const.OT_DI, id: "01367758", hospitalId: "313464", name: gumi.Const.DI_02_NAME },
//                { type: gumi.Const.OT_DI, id: "01291203", hospitalId: "313464", name: gumi.Const.DI_03_NAME },
//                { type: gumi.Const.OT_DI, id: "01179684", hospitalId: "463877", name: gumi.Const.DI_04_NAME },
//                { type: gumi.Const.OT_DI, id: "01393190", hospitalId: "538314", name: gumi.Const.DI_05_NAME },
//                { type: gumi.Const.OT_DI, id: "01373622", hospitalId: "538314", name: gumi.Const.DI_06_NAME },
//                { type: gumi.Const.OT_DI, id: "01357806", hospitalId: "162693", name: gumi.Const.DI_07_NAME },
//                { type: gumi.Const.OT_DI, id: "01413484", hospitalId: "162693", name: gumi.Const.DI_08_NAME },
//                { type: gumi.Const.OT_DI, id: "01485660", hospitalId: "162693", name: gumi.Const.DI_09_NAME },
//                { type: gumi.Const.OT_DI, id: "01413484", hospitalId: "549774", name: gumi.Const.DI_10_NAME },
//                { type: gumi.Const.OT_DI, id: "01470890", hospitalId: "232850", name: gumi.Const.DI_11_NAME },
//                { type: gumi.Const.OT_DI, id: "01299413", hospitalId: "232850", name: gumi.Const.DI_12_NAME },
//                { type: gumi.Const.OT_DI, id: "01325208", hospitalId: "232850", name: gumi.Const.DI_13_NAME },
//                { type: gumi.Const.OT_DI, id: "01445897", hospitalId: "232850", name: gumi.Const.DI_14_NAME },
//                { type: gumi.Const.OT_DI, id: "01254644", hospitalId: "232850", name: gumi.Const.DI_15_NAME },
//                { type: gumi.Const.OT_DI, id: "01427276", hospitalId: "166666", name: gumi.Const.DI_16_NAME },
//                { type: gumi.Const.OT_DI, id: "01180772", hospitalId: "427195", name: gumi.Const.DI_17_NAME },
//                { type: gumi.Const.OT_DI, id: "01385938", hospitalId: "427195", name: gumi.Const.DI_18_NAME },
//                { type: gumi.Const.OT_DI, id: "01380676", hospitalId: "427195", name: gumi.Const.DI_19_NAME },
//                { type: gumi.Const.OT_DI, id: "01372451", hospitalId: "427195", name: gumi.Const.DI_20_NAME },
//                { type: gumi.Const.OT_DI, id: "01133202", hospitalId: "427195", name: gumi.Const.DI_21_NAME },
//                { type: gumi.Const.OT_DI, id: "01408297", hospitalId: "427195", name: gumi.Const.DI_22_NAME },
//                { type: gumi.Const.OT_DI, id: "01231436", hospitalId: "427195", name: gumi.Const.DI_23_NAME }
            ];

            return data;
        };
        return SeedData;
    })();
    gumi.SeedData = SeedData;
})(gumi || (gumi = {}));
//# sourceMappingURL=seedData.js.map
