/**
* Defines all constants in the application
*/
var gumi;
(function (gumi) {
    var Const = (function () {
        function Const() {
        }
       Const.SERVER_URL = "http://111.87.74.121/app/api/";
        // Const.SERVER_URL = "http://111.87.74.121/app_test/api/";
        Const.APPCODE = "mrcs";

        Const.DELI = "|";

        Const.OT_USER = "U";
        Const.OT_LI = "LI";

        Const.OT_CS = "CS";
        Const.OT_PI = "PI";
        Const.OT_II = "II";

        Const.OT_HI = "HI";
        Const.OT_DI = "DI";

        Const.OT_QG = "QG";

        Const.OT_QM = "QM";

        Const.OT_QS = "QS";

        Const.OT_DH = "DH";

        Const.QM_DOT = ".";

        //question 1
        Const.QG_01_DESC = "本症例における今後の薬物治療を考える際に、重要視するポイントを３つまで教えてください。";
        Const.QG_01_QM_11_DESC = "眼圧下降効果";
        Const.QG_01_QM_11_QS_111_DESC = "眼圧下降量(下降幅)が大きい";
        Const.QG_01_QM_11_QS_112_DESC = "目標眼圧に対する達成可能性が高い";
        Const.QG_01_QM_11_QS_113_DESC = "眼圧日内変動の抑制も考慮した眼圧下降";
        Const.QG_01_QM_11_QS_114_DESC = "長期に渡る安定した眼圧下降";
        Const.QG_01_QM_11_QS_115_DESC = "その他";

        Const.QG_01_QM_12_DESC = "アドヒアランス";
        Const.QG_01_QM_12_QS_121_DESC = "点眼回数が少ない";
        Const.QG_01_QM_12_QS_122_DESC = "点眼本数が少ない";
        Const.QG_01_QM_12_QS_123_DESC = "点眼容器が使いやすい";
        Const.QG_01_QM_12_QS_124_DESC = "その他";

        Const.QG_01_QM_13_DESC = "安全性";
        Const.QG_01_QM_13_QS_131_DESC = "角膜障害の発生が少ない";
        Const.QG_01_QM_13_QS_132_DESC = "充血の程度が軽い";
        Const.QG_01_QM_13_QS_133_DESC = "色素沈着や眼瞼のくぼみの発生が少ない";
        Const.QG_01_QM_13_QS_134_DESC = "眼刺激性が低い";
        Const.QG_01_QM_13_QS_135_DESC = "全身性副作用へのリスクが少ない";
        Const.QG_01_QM_13_QS_136_DESC = "その他";

        Const.QG_01_QM_14_DESC = "薬価";
        Const.QG_01_QM_14_QS_141_DESC = "薬価が安い";

        Const.QG_01_QM_15_DESC = "眼圧下降以外の効果";
        Const.QG_01_QM_15_QS_151_DESC = "神経保護効果を期待できる";
        Const.QG_01_QM_15_QS_152_DESC = "眼血流改善作用を期待できる";
        Const.QG_01_QM_15_QS_153_DESC = "その他";

        //question 2
        Const.QG_02_DESC = "次の治療選択肢をお選びください(択一)";
        Const.QG_02_QM_21_DESC = "経過観察";

        Const.QG_02_QM_22_DESC = "他のPG単剤に切り替え";
        Const.QG_02_QM_22_QS_221_DESC = "ラタノプロスト点眼液";
        Const.QG_02_QM_22_QS_222_DESC = "タフルプロスト点眼液";
        Const.QG_02_QM_22_QS_223_DESC = "トラボプロスト点眼液";
        Const.QG_02_QM_22_QS_224_DESC = "ビマトプロスト点眼液";

        Const.QG_02_QM_23_DESC = "PG/β配合剤に切り替え";
        Const.QG_02_QM_23_QS_231_DESC = "ラタノプロスト・チモロールマレイン酸塩配合";
        Const.QG_02_QM_23_QS_232_DESC = "トラボプロスト/チモロールマレイン酸塩配合点眼液";

        Const.QG_02_QM_24_DESC = "β遮断薬単剤追加";
        Const.QG_02_QM_24_QS_241_DESC = "チモロールマレイン酸塩点眼液";
        Const.QG_02_QM_24_QS_242_DESC = "チモロールマレイン酸塩持続性点眼液";
        Const.QG_02_QM_24_QS_243_DESC = "カルテオロール塩酸塩点眼液";
        Const.QG_02_QM_24_QS_244_DESC = "カルテオロール塩酸塩持続性点眼液";

        Const.QG_02_QM_25_DESC = "CAI単剤・α関連薬追加";
        Const.QG_02_QM_25_QS_251_DESC = "ドルゾラミド塩酸塩点眼液";
        Const.QG_02_QM_25_QS_252_DESC = "ブリンゾラミド懸濁性点眼液";
        Const.QG_02_QM_25_QS_253_DESC = "ブリモニジン酒石酸塩点眼液";
        Const.QG_02_QM_25_QS_254_DESC = "ブナゾシン塩酸塩点眼液";

        Const.QG_02_QM_26_DESC = "β/CAI配合剤追加";
        Const.QG_02_QM_26_QS_261_DESC = "ドルゾラミド塩酸塩/チモロールマレイン酸塩点眼液";
        Const.QG_02_QM_26_QS_262_DESC = "ブリンゾラミド/チモロールマレイン酸塩配合懸濁性点眼液";

        Const.QG_02_QM_27_DESC = "その他";

        Const.QC_NOT_SYNCED = "未送信";
        Const.QC_SYNCED = "送信済";
        Const.QC_COLON = ":";
        Const.QC_PG = "◯◯◯◯◯◯";

        Const.OT_PDF = "PDF";

        Const.OT_PDFK = "PDFK";

        Const.OT_PDFD = "PDFD";

        Const.OT_PDFM = "OT_PDFM";

        Const.OT_AI = "AI";

        Const.OT_SI = "SI";

        Const.OT_CF = "CF";

        Const.PDF_01_NAME = "◯◯◯◯◯◯◯◯◯◯◯◯◯◯◯◯◯◯◯◯◯◯◯◯";
        Const.PDF_01_DESC = "□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□";
        Const.PDF_02_NAME = "◯◯◯◯◯◯◯◯◯◯◯◯";
        Const.PDF_02_DESC = "□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□";
        Const.PDF_03_NAME = "◯◯◯◯◯◯◯◯◯◯◯◯◯◯◯◯◯◯◯◯◯◯◯◯";
        Const.PDF_03_DESC = "□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□";
        Const.PDF_04_NAME = "◯◯◯◯◯◯◯◯◯◯◯◯";
        Const.PDF_04_DESC = "□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□";
        Const.PDF_05_NAME = "◯◯◯◯◯◯◯◯◯◯◯◯◯◯◯◯◯◯◯◯◯◯◯◯";
        Const.PDF_05_DESC = "□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□";
        Const.PDF_06_NAME = "◯◯◯◯◯◯◯◯◯◯◯◯";
        Const.PDF_06_DESC = "□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□";

        Const.PDFD_01_NAME = "NTG：DHを見たら治療を強化する？ しない？";
        Const.PDFD_01_DESC = "AAAAAAAAAAAAA BBBBBBBB CCCCCCC DDDDDDDDDD FFFFFF EEEEE";
        Const.PDFD_02_NAME = "PG単剤使用でドライアイ合併例どうする？";
        Const.PDFD_02_DESC = "AAAAAAAAAAAAA BBBBBBBB CCCCCCC DDDDDDDDDD FFFFFF EEEEE";
        Const.PDFD_03_NAME = "高齢者で目標眼圧に到達していない、どうする？";
        Const.PDFD_03_DESC = "AAAAAAAAAAAAA BBBBBBBB CCCCCCC DDDDDDDDDD FFFFFF EEEEE";
        Const.PDFD_04_NAME = "◯◯◯◯◯◯◯◯◯◯◯◯◯◯5";
        Const.PDFD_04_DESC = "AAAAAAAAAAAAA BBBBBBBB CCCCCCC DDDDDDDDDD FFFFFF EEEEE";
        Const.PDFD_05_NAME = "◯◯◯◯◯◯◯◯◯◯◯◯◯◯4";
        Const.PDFD_05_DESC = "AAAAAAAAAAAAA BBBBBBBB CCCCCCC DDDDDDDDDD FFFFFF EEEEE";
        Const.PDFD_06_NAME = "◯◯◯◯◯◯◯◯◯◯◯◯◯◯3";
        Const.PDFD_06_DESC = "AAAAAAAAAAAAA BBBBBBBB CCCCCCC DDDDDDDDDD FFFFFF EEEEE";
        Const.PDFD_07_NAME = "◯◯◯◯◯◯◯◯◯◯◯◯◯◯2";
        Const.PDFD_07_DESC = "AAAAAAAAAAAAA BBBBBBBB CCCCCCC DDDDDDDDDD FFFFFF EEEEE";
        Const.PDFD_08_NAME = "◯◯◯◯◯◯◯◯◯◯◯◯◯◯1";
        Const.PDFD_08_DESC = "AAAAAAAAAAAAA BBBBBBBB CCCCCCC DDDDDDDDDD FFFFFF EEEEE";

        Const.PDFK_01_NAME = "NTG：DHを見たら治療を強化する？ しない？";
        Const.PDFK_01_DESC = "NTG：DHを見たら治療を強化する？ しない？";
        Const.PDFK_02_NAME = "PG単剤使用でドライアイ合併例どうする？";
        Const.PDFK_02_DESC = "PG単剤使用でドライアイ合併例どうする？";
        Const.PDFK_03_NAME = "高齢者で目標眼圧に到達していない、どうする？";
        Const.PDFK_03_DESC = "高齢者で目標眼圧に到達していない、どうする？";
        Const.PDFK_04_NAME = "□□□□□□□□□□□□□□□□□□□□□□□□□";
        Const.PDFK_04_DESC = "□□□□□□□□□□□□□□□□□□□□□□□□□";
        Const.PDFK_05_NAME = "□□□□□□□□□□□□□□□□□□□□□□□□□";
        Const.PDFK_05_DESC = "□□□□□□□□□□□□□□□□□□□□□□□□□";
        Const.PDFK_06_NAME = "□□□□□□□□□□□□□□□□□□□□□□□□□";
        Const.PDFK_06_DESC = "□□□□□□□□□□□□□□□□□□□□□□□□□";
        Const.PDFK_07_NAME = "□□□□□□□□□□□□□□□□□□□□□□□□□";
        Const.PDFK_07_DESC = "□□□□□□□□□□□□□□□□□□□□□□□□□";
        Const.PDFK_08_NAME = "□□□□□□□□□□□□□□□□□□□□□□□□□";
        Const.PDFK_08_DESC = "□□□□□□□□□□□□□□□□□□□□□□□□□";
        Const.PDFK_09_NAME = "□□□□□□□□□□□□□□□□□□□□□□□□□";
        Const.PDFK_09_DESC = "□□□□□□□□□□□□□□□□□□□□□□□□□";
        Const.PDFK_10_NAME = "□□□□□□□□□□□□□□□□□□□□□□□□□";
        Const.PDFK_10_DESC = "□□□□□□□□□□□□□□□□□□□□□□□□□";

        Const.OID_LAST_LOGIN = "lastLogin";

        Const.CS_01_NAME = "img/img-casestudy1-01.jpg";
        Const.CS_01_DIAGNOSIS1 = "/img/casestudy-1-title.jpg";
        Const.CS_01_DIAGNOSIS2 = "6年前に人間ドックで、視神経乳頭陥凹を指摘され受診。";
        Const.CS_01_EYESIGHT = "R=0.08(1.2)　   L=0.08(1.2)";
        Const.CS_01_CENTRALCORNEALTHICKNESS = "R=480μm　     &nbsp L=490μm ";
        Const.CS_01_INTRAOCULARPRESSURE = "ベースライン眼圧は、R=21mmHg、L=24mmHgであった。<br>治療開始後の眼圧は、R=17mmHg、L=19mmHgであった。";
        Const.CS_01_AGE = "52歳";
        Const.CS_01_GENDER = "男 性";
        Const.CS_01_COMPLICATION = "なし";

        Const.CS_01_II_01_TITLE = "眼 底";
        Const.CS_01_II_01_PATH = "/image1.png";
        Const.CS_01_II_01_DESCRIPTION = "乳頭内血管の鼻側偏位を認める。陥凹が拡大し、リムの菲薄化がみられる。陥凹底を通して篩板孔が透見でき、ラミナドットサインがみられ";
        Const.CS_01_II_01_THUMBPATH = "img/img-casestudy1-02.jpg";

        Const.CS_01_II_02_TITLE = "視 野";
        Const.CS_01_II_02_PATH = "/image2.png";
        Const.CS_01_II_02_DESCRIPTION = "<dl class=&ldquo;case-dl-01 push-right-10 push-left-10&rdquo;><dt>MD値</dt><dd>-7.34dB</dd></dl><dl class=&ldquo;case-dl-02 push-right-10 push-left-10&rdquo;><dt>MDスロープ</dt><dd>-0.45dB/年</dd></dl>";
        Const.CS_01_II_02_THUMBPATH = "img/img-casestudy1-03.jpg";

        Const.CS_02_NAME = "Case Study 2";
        Const.CS_02_DIAGNOSIS1 = "/casestudy-2-title.jpg";
        Const.CS_02_DIAGNOSIS2 = "人間ドックで緑内障の疑いを指摘され受診。<br/>初診から5年経過。";
        Const.CS_02_EYESIGHT = "R=1.0(1.2)　L＝0.9(1.2)";
        Const.CS_02_CENTRALCORNEALTHICKNESS = "R=544μm　L=517μm";
        Const.CS_02_INTRAOCULARPRESSURE = "ベースライン眼圧は、18-20mmHg（日内変動測定）であった。<br>治療開始後は13-14mmHgで推移していたが、最近15-16mmHgにあがってきた。";
        Const.CS_02_AGE = "59歳";
        Const.CS_02_GENDER = "女 性";
        Const.CS_02_COMPLICATION = "なし";

        Const.CS_02_II_01_TITLE = "眼 底";
        Const.CS_02_II_01_PATH = "/image3.png";
        Const.CS_02_II_01_DESCRIPTION = "緑内障視神経陥陥凹に加え、右眼視神経乳頭7時、9時方向に乳頭出血（DH）を認めた。";
        Const.CS_02_II_01_THUMBPATH = "img/img-casestudy2-02.jpg";

        Const.CS_02_II_02_TITLE = "OCT";
        Const.CS_02_II_02_PATH = "/image4.png";
        Const.CS_02_II_02_DESCRIPTION = "<dl class=&ldquo;case-dl-01 push-right-10 push-left-10&rdquo;><dt>MD slope</dt><dd>-0.32dB/年</dd></dl>";
        Const.CS_02_II_02_THUMBPATH = "img/img-casestudy2-03.jpg";

        Const.CS_02_II_03_TITLE = "視 野(左眼)";
        Const.CS_02_II_03_PATH = "/image5.png";
        Const.CS_02_II_03_DESCRIPTION = "<dl class=&ldquo;case-dl-01 push-right-10 push-left-10&rdquo;><dt>MD slope</dt><dd>-0.39dB/年</dd></dl>";
        Const.CS_02_II_03_THUMBPATH = "img/img-casestudy2-04.jpg";

        Const.CS_03_NAME = "症例③";
        Const.CS_03_DIAGNOSIS1 = "/casestudy-3-title.jpg";
        Const.CS_03_DIAGNOSIS2 = "健診で両眼の乳頭陥凹拡大を指摘され、受診した。";
        Const.CS_03_EYESIGHT = "R=0.05(1.2)　L=0.04(1.2)";
        Const.CS_03_CENTRALCORNEALTHICKNESS = "R=539μm　&nbsp&nbspL=538μm";
        Const.CS_03_INTRAOCULARPRESSURE = "ベースライン眼圧は、R=20-21mmHg、L=21-23mmHgであった。<br/>治療開始後、17-19mmHgで推移している。";
        Const.CS_03_AGE = "45歳";
        Const.CS_03_GENDER = "女 性";
        Const.CS_03_COMPLICATION = "なし";

        Const.CS_03_II_01_TITLE = "Title 6";
        Const.CS_03_II_01_PATH = "/image6.png";
        Const.CS_03_II_01_DESCRIPTION = "上耳側にNFLDを認め、これに一致して陥凹が拡大し、リムが萎縮。下耳側にもNFLDを認め、陥凹底にはラミナドットサインを認める。";
        Const.CS_03_II_01_THUMBPATH = "img/img-casestudy3-02.jpg";

        Const.CS_03_II_02_TITLE = "Title 7";
        Const.CS_03_II_02_PATH = "/image7.png";
        Const.CS_03_II_02_DESCRIPTION = "眼底写真で認められた部位に一致して、12時から1時にかけてと、5時から6時にかけてｐ&ldquo;ldquo<&rdquo;1％の赤色セクターが描出されている。";
        Const.CS_03_II_02_THUMBPATH = "img/img-casestudy3-03.jpg";

        Const.CS_03_II_03_TITLE = "Title 8";
        Const.CS_03_II_03_PATH = "/image8.png";
        Const.CS_03_II_03_DESCRIPTION = "<dl class=&ldquo;case-dl-01 push-right-10 push-left-10&rdquo;><dt>MD値</dt><dd>-7.03dB</dd></dl><dl class=&ldquo;case-dl-02 push-right-10 push-left-10&rdquo;><dt>MDスロープ</dt><dd>-0.60dB/年</dd></dl>";
        Const.CS_03_II_03_THUMBPATH = "img/img-casestudy3-04.jpg";

        Const.PI_01_NAME = "ラタノプロスト点眼液";
        Const.PI_02_NAME = "タフルプロスト点眼液";
        Const.PI_03_NAME = "トラボプロスト点眼液";
        Const.PI_04_NAME = "ビマトプロスト点眼液";

        Const.DF_PI_01 = "1";
        Const.DF_PI_02 = "3";
        Const.DF_PI_03 = "1";

        Const.HI_01_NAME = "Ａ眼科クリニック";
        Const.HI_02_NAME = "Ｅ医療";
        Const.HI_03_NAME = "Ｉ病院";
        Const.HI_04_NAME = "Ｊ病院";
        Const.HI_05_NAME = "Ｊ総合医療センター";
        Const.HI_06_NAME = "山田病院";
        Const.HI_07_NAME = "東京総合病院";
        Const.HI_08_NAME = "Ｍアイクリニック";
        Const.HI_09_NAME = "Ｎ大阪病院";

        Const.DI_01_NAME = "山田　彩子";
        Const.DI_02_NAME = "木下　后幸";
        Const.DI_03_NAME = "内田　冠奈";
        Const.DI_04_NAME = "三波　玄一郎";
        Const.DI_05_NAME = "中野　由美子";
        Const.DI_06_NAME = "石田　香奈";
        Const.DI_07_NAME = "大石　美穂子";
        Const.DI_08_NAME = "宮田　悠";
        Const.DI_09_NAME = "清水　理紗";
        Const.DI_10_NAME = "宮田　悠";
        Const.DI_11_NAME = "山田　公子";
        Const.DI_12_NAME = "福島　宗之";
        Const.DI_13_NAME = "福田　はるみ";
        Const.DI_14_NAME = "山下　玲奈";
        Const.DI_15_NAME = "竹原　淳吉";
        Const.DI_16_NAME = "林　智";
        Const.DI_17_NAME = "上田　薫子";
        Const.DI_18_NAME = "山瀬　道秀";
        Const.DI_19_NAME = "篠原　敦子";
        Const.DI_20_NAME = "田神　康隆";
        Const.DI_21_NAME = "内海　全克";
        Const.DI_22_NAME = "山川　睦";
        Const.DI_23_NAME = "井上　保範";
        return Const;
    })();
    gumi.Const = Const;
})(gumi || (gumi = {}));
//# sourceMappingURL=consts.js.map
