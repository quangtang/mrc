/********* CDVGumiServices.m Cordova Plugin Implementation *******/

#import <Cordova/CDV.h>
#import "GumiServices.h"

static id<IGumiActionHandler> _handler;

@implementation GumiServices

+ (void)registerHandler:(id<IGumiActionHandler>)handler {
    _handler = handler;
}

- (void)execAction:(CDVInvokedUrlCommand*)command
{
    NSString* realActionName = [command argumentAtIndex:0];
    
    if (realActionName != nil) {
        if (_handler != nil) {
            NSDictionary* realArgs = [command argumentAtIndex:1];
            [_handler execAction:command actionName:realActionName args:realArgs  serviceHost:self];
        }
    }
}

@end
