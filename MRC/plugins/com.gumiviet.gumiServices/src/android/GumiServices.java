package com.gumiviet;

import java.util.Hashtable;

import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CallbackContext;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


/**
 * This class echoes a string called from JavaScript.
 */
public class GumiServices extends CordovaPlugin {
	
	public interface IGumiActionHandler {
		boolean execAction(CallbackContext callbackContext, String actionName, JSONObject args, GumiServices serviceHost) throws JSONException;
	}
	
	class DemoHandler implements IGumiActionHandler {

		@Override
		public boolean execAction(CallbackContext callbackContext, String actionName, JSONObject args, GumiServices serviceHost)
				throws JSONException {

			String message = args.getString("message");
			if (message != null && message.length() > 0) {
	            callbackContext.success(message);
				return true;
	        } else {
	            callbackContext.error("Expected one non-empty string argument.");
	        }
			
			return false;
		}
	}
	

	static IGumiActionHandler _actionHandler;
	
	public static void registerActionHandler(IGumiActionHandler actionHandler) {
		_actionHandler = actionHandler;
	}

	private static final String ACTION_NAME = "execAction";
	
    @Override
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {
    	
    	if (action.equalsIgnoreCase(ACTION_NAME)) {
    		if (_actionHandler != null) {
        		String realActionName = args.getString(0);
        		JSONObject realArgs = args.getJSONObject(1);

        		return _actionHandler.execAction(callbackContext, realActionName, realArgs, this);
    		}
    	}
        return false;
    }

}
