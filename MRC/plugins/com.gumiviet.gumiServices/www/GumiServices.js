var exec = require('cordova/exec');

exports.execAction = function(success, error, actionName, args) {
	console.log('Call to Gumiviet service, action: ' + actionName);
    exec(success, error, "GumiServices", "execAction", [actionName, args]);
};
