/// <reference path="consts.ts" />
/// <reference path="dataStore.ts" />
/// <reference path="seedData.ts" />
var gumi;
(function (gumi) {
    var ServerAnswer = (function () {
        function ServerAnswer(groupAnswers) {
            this.groupAnswers = groupAnswers;
            this.id = "SA";
            this.type = "SA";
        }
        return ServerAnswer;
    })();
    gumi.ServerAnswer = ServerAnswer;

    var DataModel = (function () {
        function DataModel(ds) {
            this._ds = ds;
            this.seedData();
        }
        DataModel.prototype.seedData = function () {
            var objs = gumi.SeedData.getSeedData();
            for (var i = 0; i < objs.length; i++) {
                var obj = objs[i];
                this._ds.setObject(obj);
            }
        };

        //-------------------- Login controller --------------------
        DataModel.prototype.getUserByName = function (userName) {
            var funcCheckUserName = function (user) {
                return user.id === userName;
            };
            return this._ds.queryOne(gumi.Const.OT_USER, funcCheckUserName);
        };

        /**
        * Get User Id of LoginInfo
        */
        DataModel.prototype.getLoginInfoData = function () {
            return this._ds.queryOne(gumi.Const.OT_LI, null);
        };

        /**
        * Save Last User Id
        */
        DataModel.prototype.setLoginInfoData = function (lastUserData) {
            this._ds.setObject(lastUserData);
        };

        //-------------------- Entry controller --------------------
        /**
        * Get Hospital List By mrId
        */
        DataModel.prototype.getHospitalList = function (mrId) {
            var funcCheckMrid = function (hospital) {
                return (hospital).mrId == mrId;
            };
            return this._ds.query(gumi.Const.OT_HI, funcCheckMrid);
        };

        /**
        * Get Doctor List By Hospital Id
        */
        DataModel.prototype.getDoctorList = function (hospitalId) {
            var funcCheckhospitalid = function (doctor) {
                return (doctor).hospitalId == hospitalId;
            };
            return this._ds.query(gumi.Const.OT_DI, funcCheckhospitalid);
        };

        /**
         * Get Doctor List deleted history
         */
        DataModel.prototype.getDoctorDeletedList = function (mrId) {
            var doctorDeletedList = function (doctor) {
                return doctor.mrId == mrId;
            };
            return this._ds.query(gumi.Const.OT_DH, doctorDeletedList);
        };

        /**
        * Get All Doctor List By Hospital Id
        */
        DataModel.prototype.getAllDoctorList = function () {
            return this._ds.query(gumi.Const.OT_DI, null);
        };

        //-------------------- Case Controller --------------------
        /**
        * Get Case Study By Id
        */
        DataModel.prototype.getCaseStudy = function (caseId) {
            var funcCheckCaseId = function (caseStudy) {
                return (caseStudy).id == caseId;
            };
            return this._ds.queryOne(gumi.Const.OT_CS, funcCheckCaseId);
        };

        /**
        * Get Pg list
        */
        DataModel.prototype.getPgInfoList = function () {
            return this._ds.getObjectList(gumi.Const.OT_PI);
        };

        //-------------------- QuestionGroupController --------------------
        /**
        * Get Question Group List
        */
        DataModel.prototype.getQuestionGroupList = function (caseStudyId) {
            var funcGetQuestionGroups = function (question) {
                return (question).caseStudyId == caseStudyId;
            };
            return this._ds.query(gumi.Const.OT_QG, funcGetQuestionGroups);
        };

        //-------------------- ConfirmController --------------------
        /**
        * Get Hospital By Id
        */
        DataModel.prototype.getHospital = function (hospitalId) {
            var funcGetHospital = function (hospital) {
                return (hospital).id == hospitalId;
            };
            return this._ds.queryOne(gumi.Const.OT_HI, funcGetHospital);
        };

        /**
        * Get Doctor By Id
        */
        DataModel.prototype.getDoctor = function (doctorId) {
            var funcGetDoctor = function (doctor) {
                return (doctor).id == doctorId;
            };
            return this._ds.queryOne(gumi.Const.OT_DI, funcGetDoctor);
        };

        /**
        * Get Pg By Id
        */
        DataModel.prototype.getPg = function (pgId) {
            var funcGetPg = function (pg) {
                return (pg).id === pgId;
            };
            return this._ds.queryOne(gumi.Const.OT_PI, funcGetPg);
        };

        /**
        * Save Application Info
        */
        DataModel.prototype.saveAppInfo = function (appInfo) {
            // insert db
            this._ds.setObject(appInfo);
        };

        //-------------------- Data01PdfController --------------------
        /**
        * Get Data01 Pdf List
        */
        DataModel.prototype.getData01PdfList = function (caseId, questionGroupId, questionMainId, questionSubId) {
            var funcGetPdfMap = function (pdfMap) {
                return (pdfMap).csId == caseId && (pdfMap).questionGroupId == questionGroupId && (pdfMap).questionMainId == questionMainId && (pdfMap).questionSubId == questionSubId;
            };
            return this._ds.query(gumi.Const.OT_PDFM, funcGetPdfMap);
        };

        /**
        * Get Pdf By Id
        */
        DataModel.prototype.getPdf = function (id) {
            var funcGetPdf = function (pdf) {
                return (pdf).id == id;
            };
            return this._ds.queryOne(gumi.Const.OT_PDF, funcGetPdf);
        };

        //-------------------- DataPdfController --------------------
        /**
        * Get Data Pdf List
        */
        DataModel.prototype.getDataPdfList = function () {
            return this._ds.getObjectList(gumi.Const.OT_PDFD);
        };

        //-------------------- KanrenController --------------------
        /**
        * Get Kanren Pdf List
        */
        DataModel.prototype.getKanrenPdfList = function () {
            return this._ds.getObjectList(gumi.Const.OT_PDFK);
        };

        //-------------------- ListController --------------------
        /**
        * Get Application Info List
        */
        DataModel.prototype.getAppInfoList = function (mrId, userId) {
            var funcGetAppInfo = function (appInfo) {
                return (appInfo).mrId == mrId && (appInfo).userId == userId;
            };
            return this._ds.query(gumi.Const.OT_AI, funcGetAppInfo);
        };

        /**
        * Delete Application Info
        */
        DataModel.prototype.deleteAppInfo = function (appInfoId) {
            this._ds.removeObject(gumi.Const.OT_AI, appInfoId);
        };

        DataModel.prototype.setServerAnswer = function (groupAnswers) {
            var sa = new ServerAnswer(groupAnswers);

            this._ds.setObject(sa);
        };

        DataModel.prototype.getServerAnswer = function () {
            var sa = this._ds.getObject("SA", "SA");
            return sa;
        };
        return DataModel;
    })();
    gumi.DataModel = DataModel;
})(gumi || (gumi = {}));
//# sourceMappingURL=dataModel.js.map
