$(document).on("pagebeforeshow",function(){
	$('.popup').click(function() {
        isOnline(function(status){
            if(status){
                $('.content-popup').bPopup({
                    closeClass:"c-close",
                    speed: 0
                });
            }else{
                $("#network").bPopup({
                    closeClass:"b-close",
                    speed: 0
                });
            }
        });
	});
	$('.popup-01').click(function() {
		$('.content-popup-01').bPopup({
			closeClass:"c-close",
            position: [242, 240],
            onOpen:function(){
                var moveLogin = angular.element("#move").scope();
                moveLogin.CheckLastUserInfo();
            },
			speed: 0,
			follow: [false, false]
		});
	});
	$('.popup-03').click(function() {
		$('.content-popup-03').bPopup({
			closeClass:"close",
			speed: 0
		});
	});
    $('.popup-03-01').click(function() {
        $('.content-popup-03-01').bPopup({
            closeClass:"close",
            speed: 0
        });
    });
	$('.popup-04').click(function() {
		$('.content-popup-04').bPopup({
			closeClass:"close",
			speed: 0
		});
	});
	$('.popup-05').click(function() {
		$('.content-popup-05').bPopup({
			closeClass:"close",
			speed: 0
		});
	});
	$('.popup-06').click(function() {
		$('.content-popup-06').bPopup({
			closeClass:"close",
			speed: 0
		});
	});
	$('.popup-07').click(function() {
		$('.content-popup-07').bPopup({
			closeClass:"close",
			speed: 0
		});
	});
	$('.popup-08').click(function() {
		$('.content-popup-08').bPopup({
			closeClass:"close",
			speed: 0
		});
	});
	$('.popup-09').click(function() {
		$('.content-popup-09').bPopup({
			closeClass:"close",
			speed: 0
		});
	});
	$('.popup-10').click(function() {
		$('.content-popup-10').bPopup({
			closeClass:"close",
			speed: 0
		});
	});
	$('.popup-12').click(function() {
		$('.content-popup-12').bPopup({
			closeClass:"b-close",
			speed: 0
		});
	});
	$('.popup-13').click(function() {
		$('.content-popup-13').bPopup({
			closeClass:"close",
			speed: 0
		});
	});
	$('.popup-11').click(function() {
		$('.content-popup-11').bPopup({
			closeClass:"b-close",
			speed: 0
		});
	});
	$('.popup-14').click(function() {
		$('.content-popup-14').bPopup({
			closeClass:"b-close",
			speed: 0
		});
	});

    $('.btn-gototop-confirm-q1').click(function() {
        $('#confirm-question-gototop-q1').bPopup({
            closeClass:"c-close",
            speed: 0
        });
    });
    $('.btn-gototop-confirm-q2').click(function() {
        $('#confirm-question-gototop-q2').bPopup({
            closeClass:"c-close",
            speed: 0
        });
    });


});
app.initialize();
$(function () {
    if(( /(iphone|ipod)/i.test(navigator.userAgent) )) {
        $("html").css({
            "-webkit-transform":"scale(0.41)",
            "transform":"scale(0.41)",
            "margin":"-94px auto 0 -201px"
        });
    }

        $(":mobile-pagecontainer").on("pagecontainerhide", function (event, ui) {
            var pageId = ui.nextPage[0].id;

            if (pageId.indexOf('question-')) {
                //console.log('Force the collapsible initialize');
                //Refresh all collapsible set too
                $(".question-group").collapsible({ collapsed: true });
                //alert( "This page was just shown: " + ui.nextPage );
            }

            //Push the isShowSyncInfo to the angluar scope of the specified element show-sync-info
            //特定のページの時フッター右下アイコンを表示する
            var syncFooterScope = angular.element($("#show-sync-info")).scope();
            var syncFooterScopeSummary = angular.element($("#show-summary")).scope();

            if(pageId !== "content-summary") syncFooterScopeSummary.resetBackChart(false);

            if (pageId === "index-page") {
                syncFooterScope.isShowSyncInfo = pageId === "index-page";
                syncFooterScopeSummary.isShowSyncSummary = pageId === "index-page";
                var indexScope = angular.element("#index-page").scope();
                indexScope.refresh();
            } else if (pageId === "data01-page") {
                syncFooterScopeSummary.isShowSyncSummary = pageId === "data01-page";
                var data01Page = angular.element("#data01-page").scope();
                data01Page.refresh();
                syncFooterScopeSummary.resetBackChart(true);
            } else if (pageId === "entry-page") {
                // Bắt sự kiện khi nhảy vào trang entry, ép scope của trang này refresh !!!
                // Có thể áp dụng điều tương tự với các trang khác !
                // Lưu ý, trong controller của trang này phải có method refresh và nó nên
                // được gọi khi bắt đầu 1 controller
                var entryScope = angular.element("#entry-page").scope();
                entryScope.refresh();
                entryScope.freeHospitalCall();
            } else if (pageId === "content-summary") {
                syncFooterScopeSummary.isShowSyncSummary = false;
                var summaryScope = angular.element("#content-summary").scope();
                //console.log("summaryScope",summaryScope,"pageId:",pageId);
                summaryScope.refresh();

            } else if (pageId === "list-page") {
                syncFooterScope.isShowSyncInfo = false;
                var listPageScope = angular.element("#list-page").scope();
                //console.log("summaryScope",summaryScope,"pageId:",pageId);
                listPageScope.refresh();

            } else if (pageId === "kanren-page") {
                var kanrenPage = angular.element($("#kanren-page")).scope();
                kanrenPage.refresh();

            }else {
                //表示しないページ
                syncFooterScope.isShowSyncInfo = pageId === "none-page";
                syncFooterScopeSummary.isShowSyncSummary = pageId === "none-page";
            }
            if (!syncFooterScope.$$phase || syncFooterScope.$$phase == "")
                syncFooterScope.$apply();
            if (!syncFooterScopeSummary.$$phase || syncFooterScopeSummary.$$phase == "")
                syncFooterScopeSummary.$apply();

        });
        $(".question-group").on("collapsiblecreate", function (event, ui) {
            // console.log('Collapsible created');
            //var container = event.currentTarget;
            //var testEle = $("#checkbox-1.1");
            ///testEle.offset({top:$(container).offset().top});
        });
});
// CSS for Login Info popup
function move(){
    $("#move").css({"position":"fixed","top":"19.5%"});
}

