mrcsApp.controller("kanrenController", ["$scope", "appFactory",
    function($scope, appFactory){
        $scope.refresh = function(){
            $scope.backPage = appFactory.backPage;
        }
        $scope.pdfFilesList = [
            {
                "title":"乳頭出血と視野進行の関連",
                "filename":"kanren01.pdf"
            },
            {
                "title":"平均余命と治療期間",
                "filename":"kanren02.pdf"
            },
            {
                "title":"正常眼圧緑内障の発症・進行のリスクファクター",
                "filename":"kanren03.pdf"
            },
            {
                "title":"アドヒアランス不良を招く諸因子",
                "filename":"kanren04.pdf"
            },
            {
                "title":"ベースライン眼圧・目標眼圧設定の参考エビデンス",
                "filename":"kanren05.pdf"
            },
            {
                "title":"緑内障診療ガイドライン",
                "filename":"kanren06.pdf"
            }
        ];

        $scope.popupPdf = function(filename, pageNumber){
            var opt = {
                url: "/kanren_pdf/" + filename,
                pageNumber: pageNumber
            };

            //console.log(opt);


            GumiServices.execAction(function(){
                console.log("success");
            }, function(){
                console.log("error");
            }, "readPDF", opt);
        }
    }
]);