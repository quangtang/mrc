mrcsApp.controller('LoginController',
    function ($scope, appFactory) {
        //check remember
        $scope.checkRemember = function(){
            $scope.loginUser.remember = !$scope.loginUser.remember;
        };

        $scope.checkRememberInfo = function(){
            $scope.loginUser.rememberInfo = !$scope.loginUser.rememberInfo;
        };

        function getEnqueteData() {
            var ret = {};
            ret['login'] = $scope.loginUser.username;
            ret['password'] = $scope.loginUser.password;
            return ret;
        }

        //truong hop lan dau login
        function updateLogin(data,formName){
            if(!angular.isDefined(data)) data = [];
            if(!angular.isArray(data)){
                data = [data];
            }
            //save user to database
            appFactory._ds.setObject({
                type: gumi.Const.OT_USER,
                id: $scope.loginUser.username,
                password:$scope.loginUser.password,
                remember:$scope.loginUser.remember,
                rememberInfo:false
            });
            updateHospitalAndDoctor(data,formName);
            var nextPage = formName =="loginPage"?"#index-page":"#list-page";
            switchPage(nextPage,formName);
        }

        //cap nhat thong tin benh vien va bac si vao database
        function updateHospitalAndDoctor(data,formName){
            //clear tat ca thong tin benh vien va bac si cua mr tuong ung truoc khi cap nhat cai moi
            if(formName == "loginPage"){
                console.log("clear hospital and doctor before update");
                _.each(appFactory.getHospitalList($scope.loginUser.username),function(hospital){

                    var hospitalId = hospital.id;
                    if(!hospital.isFree)
                        appFactory._ds.removeObject(hospital.type,hospitalId);
                    _.each(appFactory.getDoctorList(hospitalId),function(doctor){
                        if(!doctor.isfreeDoctor)
                            appFactory._ds.removeObject(doctor.type,doctor.id);
                    });
                });
            }

            _.each(data,function(hs){
                //save hospital to database
                appFactory._ds.setObject({
                    type: gumi.Const.OT_HI,
                    id: hs.id,
                    mrId: $scope.loginUser.username,
                    name: hs.name,
                    isFree:false
                });
                _.each(hs.doctor_list,function(dt){

                    if(dt.id){
                        //save doctor
                        appFactory._ds.setObject({
                            type: gumi.Const.OT_DI,
                            id: hs.id+"_"+dt.id,
                            dr_id:dt.id,
                            hospitalId: hs.id,
                            name: dt.name,
                            isfreeDoctor:false,
                            isFreeHospital:false
                        });
                    }else{
                        _.each(dt,function(dtt){
                            //save doctor
                            appFactory._ds.setObject({
                                type: gumi.Const.OT_DI,
                                id: hs.id+"_"+dtt.id,
                                hospitalId: hs.id,
                                dr_id:dtt.id,
                                name: dtt.name,
                                isfreeDoctor:false,
                                isFreeHospital:false
                            });
                        });
                    }
                });

            });
        }

        function switchPage(nextPage,formPage){
            //save username
            var lastuserdata = {
                type :  "LI" ,
                id: 1 ,
                username : $scope.loginUser.username,
                password:$scope.loginUser.password,
                remember:$scope.loginUser.remember,
                rememberInfo:$scope.loginUser.rememberInfo
            };

            appFactory.saveLastUser(lastuserdata);

            //save mrid
            appFactory._mrID = $scope.loginUser.username;
            appFactory._userID = $scope.loginUser.username;
            appFactory._userPASS = $scope.loginUser.password;
            if(formPage == "loginPage"){
                appFactory._mainMrId = appFactory._mrID;
                appFactory._mainPassword = appFactory._userPASS;
            }

            $scope.changePage(nextPage);

        }

        //check last username
        $scope.CheckLastUser = function(){
            var lastUser = appFactory.checkAndGetLastUser();
            //kie tra user ton tai hay chua

            if(lastUser){
                //neu co remember thi show 3 field len
                $scope.loginUser = {
                    username: lastUser.username,
                    password:lastUser.password,
                    remember:lastUser.remember,
                    rememberInfo:lastUser.rememberInfo
                };
                //neu k remember thi clear 3 field username, password, remember = false
                if(!lastUser.remember){
                    $scope.loginUser = {
                        username: "",
                        password:"",
                        remember:false,
                        rememberInfo:lastUser.rememberInfo
                    };
                }
                $scope.updateScope($scope);
            }else{
                //neu chua thi clear tat ca field nhap remember doi sang false
                $scope.loginUser = {
                    username: "",
                    password:"",
                    remember:false,
                    rememberInfo:false
                };
            }
        }

        $scope.CheckLastUserInfo = function(){
            var lastUser = appFactory.checkAndGetLastUser();
            if(lastUser){
                $scope.loginUser = {
                    username: lastUser.username,
                    password:lastUser.password,
                    remember:lastUser.remember,
                    rememberInfo:lastUser.rememberInfo
                };
                if(!lastUser.rememberInfo){
                    $scope.loginUser = {
                        username: "",
                        password:"",
                        remember:lastUser.remember,
                        rememberInfo:false
                    };
                }
                $scope.updateScope($scope);
            }

        }

        function clearDataMr(mr,password){
            var user = appFactory._ds.getObject(gumi.Const.OT_USER,mr);
            if(user && user.password == password){
                console.log("clear mr:"+mr);
                _.each(appFactory._ds.getObjectList(gumi.Const.OT_AI),function(appInfo){
                    if(appInfo.mrId == mr){
                        var hospitalId = appInfo.hospitalId;
                        appFactory._ds.removeObject(gumi.Const.OT_AI,appInfo.id);
                        appFactory._ds.removeObject(gumi.Const.OT_HI,hospitalId);
                        _.each(appFactory.getDoctorList(hospitalId),function(doctor){
                            appFactory._ds.removeObject(gumi.Const.OT_DI,doctor.id);
                        });
                        appFactory._ds.removeObject(gumi.Const.OT_USER,mr);
                    }
                });
            }

        }
        //login
        $scope.login = function(nextPage,formName) {
            isOnline(function(status){
                console.log("login online:" + status);
                if(status){
                    $scope.syncServer("update",getEnqueteData(),function(data){
                        updateLogin(data.facility_list.facility,formName);
                    },function(err){
                        clearDataMr($scope.loginUser.username,$scope.loginUser.password);
                        $("#login-alert-box").bPopup({
                            closeClass:"b-close",
                            speed: 0
                        });
                    });

                }else{
                    //login for the second times
                    appFactory.login(
                        $scope.loginUser.username,
                        $scope.loginUser.password,
                        function () {
                            switchPage(nextPage,formName);
                        },
                        function () {
                            $("#network").bPopup({
                                closeClass:"b-close",
                                speed: 0
                            });
                        }
                    );
                }
            });
        }
    }
);
