/// <reference path="consts.ts" />

//The main angular controllers

var mrcsApp = angular.module('mrcsApp', ['ngCordova']);

//Inject the application factory
mrcsApp.factory('appFactory', function() {
	return gumi.Application.instance();
});

mrcsApp.controller('RootController',
 	function ($scope, appFactory, $sce) {

        $scope.updateScope = function(scope){
            if(!scope.$$phase || scope.$$phase == "" || scope.$$phase == null)scope.$apply();
        };

        $scope.changePage = function(pageName){
            $( ":mobile-pagecontainer" ).pagecontainer( "change", pageName, null);
        };

        $scope.gotoEntry = function(nextPage) {
 			$("#selHopistal").selectmenu(); // Initializes
			$("#selHopistal").val("").selectmenu("refresh", true);
			$("#selDoctor").selectmenu(); // Initializes
			$("#selDoctor").val("").selectmenu("refresh", true);

            $scope.changePage(nextPage);
 		}

        $scope.getCurrentDate = function() {
            return moment().format("YYYY-MM-DD HH:mm:ss");
        }

        $scope.syncServer = function(action,data,onSuccess,onError){
            $.ajax({
                type: "POST",
                url: gumi.Const.SERVER_URL + action,
                data: data,
                success: function(result,status,xhr){
                    var x2 = new X2JS();
                    var obj = x2.xml_str2json(result);
                    if (obj.data.result.data == "1") {
                        if(onSuccess)
                            onSuccess(obj.data.result,status,xhr);
                    }else{
                        if(onError)
                            onError(obj.data.result.data,status,xhr);
                    }
                },
                error:function(xhr,status,error){
                    if(onError)
                        onError(xhr,status,error);
                }
            });
        }

 	}
 );

//Inject the main controller. 
//To keep the architect simple and effective and avoid conflicting with jQuery Mobile, we implement only 1 controller 
mrcsApp.controller('FooterController',
 	function ($scope, appFactory, $sce) {

        //ham nay de tat hoac bat nut back o trang chart
        $scope.resetBackChart = function(status){
            appFactory._showBackChart = status;
        }
 	    $scope.isShowSyncInfo = false;
 	}
 );



//
mrcsApp.directive("jqMobileCollapsibleSet", function() {
    return {
        restrict: 'C',
        link: function(scope, element, attrs) {
            element.collapsibleset({
                iconpos : "right"
            });
            element.on( "collapsibleexpand", function( event, ui ) {
                $(".kanrengraybox").css("-webkit-transform","translate(0px, 5px)");
            } );
        }
    };
});

 
 
 