mrcsApp.controller('CaseController',
	function ($scope, appFactory, $sce) {
        //console.log("CaseController");
		// Show/hide top buttons
		$scope.isEdity = appFactory._isEdity;
		appFactory.cacheQuestionScope('list-case', $scope);
		$scope.refresh = function() {
		    $scope.isEdity = appFactory._isEdity;
		}
	
		/* case top */
		//var caseidselected = null;

		$scope.chooseCase = function(nextPage, caseId){
		
			//Reset the answer group list
	    	appFactory.resetAnswerGroupList();
			
			//store caseid selected
		    appFactory._caseStudyId = caseId;
            appFactory.__caseStudyId = appFactory._caseStudyId;

		    // If from list.html flow
		    if (appFactory._isEdity) {
		    	if (appFactory._caseStudyFlowList == null || appFactory._caseStudyFlowList.length == 0) {
		    		return;
		    	}

                if (appFactory._caseStudyFlowList[appFactory._caseStudyId -1].isChecked) {
                    appFactory._answerGroupList = appFactory._caseStudyFlowList[appFactory._caseStudyId -1].answers;
                }

			}
			
			// Refresh Case Study
			appFactory.refreshQuestionScopes('case-caseStudy');

            //Force question-01 scope to refresh
			appFactory.refreshQuestionScopes('1');

            $scope.changePage(nextPage);
		}
		
	}
);
