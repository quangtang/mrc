var SERVER_URL = "http://111.87.74.121/app/api/";

mrcsApp.controller('SummaryController',
    function ($scope, appFactory) {
        $scope.groupNumber = {
            firstGroup:1,
            secondGroup:2,
            index:1,
            barChart:true

        };

        $scope.changedCase = function(number,index){
            var index = parseInt(index);
            $scope.groupNumber.index = index;
            if(index%2 == 0){
                $scope.groupNumber.barChart = false;
            }else{
                $scope.groupNumber.barChart = true;
            }
            appFactory._caseStudyId = number.toString();
            var group = number === "1" ? number:number === "2" ? parseInt(number) + 1:parseInt(number) + 2;
            $scope.groupNumber.firstGroup = parseInt(group);
            $scope.groupNumber.secondGroup = parseInt(group) + 1;
            $scope.refresh();
        }

        function calculateAnswersData() {
            $scope.GG = {
                group:{}
            };

            //Get the server answer first
            var sa = appFactory._dm.getServerAnswer();

            if (sa && sa.groupAnswers) {
                //tra ve ket qua kong can thong qua du lieu local
                $scope.GG.group = sa.groupAnswers.group;
            }else{
                //Dau tien, dua tren cau truc cua cac cau hoi, ta tao ra du lieu ban dau cho tat ca cac cau hoi
                var allQuestionGroups = appFactory._ds.getObjectList(gumi.Const.OT_QG);

                //console.log("allQuestionGroups : ", allQuestionGroups);
                _.each(allQuestionGroups, function (qg) {
                    $scope.GG.group[qg.id] = {
                        questionGroupId: qg.id,
                        counter: 0,
                        description: qg.description,
                        percent:0,
                        main:{}

                    };
                    _.each(qg.children, function (qm) {
                        $scope.GG.group[qg.id].main[qm.id] = {
                            type:gumi.Const.OT_AI,
                            questionMainId: qm.id,
                            questionGroupId: qg.id,
                            counter: 0,
                            description: qm.description,
                            percent:0,
                            sub:{}
                        };
                        _.each(qm.children, function (qs) {
                            $scope.GG.group[qg.id].main[qm.id].sub[qs.id] = {
                                questionSubId: qs.id,
                                questionMainId: qm.id,
                                questionGroupId: qg.id,
                                counter: 0,
                                description: qs.description,
                                percent:0
                            };
                        });
                    });
                });
            }


            var appInfoList = appFactory._ds.getObjectList(gumi.Const.OT_AI);

            if (appInfoList && appInfoList.length > 0) {
                //Duyet tung app info, co bao nhieu appinfo
                for (var i = 0; i < appInfoList.length; i++) {

                    // Check this appInfo (Doctor) already synced or not ?
                    if(!appInfoList[i].isSynced){
                        var ai = appInfoList[i];

                        //Duyet tung case study flow
                        var csfs = ai.caseStudyFlows;
                        if (csfs && csfs.length > 0) {
                            for (var j = 0; j < csfs.length; j++) {
                                var csf = csfs[j];

                                // Check this caseStudy already synced or not ?
                                if(!csf.isSynced){
                                    //Duyet tung group answer
                                    var groupAnswers = csf.answers;
                                    if (groupAnswers && groupAnswers.length > 0) {
                                        for (var k = 0; k < groupAnswers.length; k++) {

                                            var groupAnswer = groupAnswers[k];

                                            //cong counter cho group tuong ung
                                            if($scope.GG.group[groupAnswer.groupId])
                                                $scope.GG.group[groupAnswer.groupId].counter++;

                                            //Duyet tung main/sub answer
                                            var subAnswers = groupAnswer.answers;
                                            if (subAnswers && subAnswers.length > 0) {
                                                for (var l = 0; l < subAnswers.length; l++) {
                                                    var subAnswer = subAnswers[l];
                                                    //cong counter cho main tuong ung

                                                    if($scope.GG.group[groupAnswer.groupId].main[subAnswer.mainId])
                                                        $scope.GG.group[groupAnswer.groupId].main[subAnswer.mainId].counter++;

                                                    //cong counter cho sub tuong
                                                    if($scope.GG.group[groupAnswer.groupId].main[subAnswer.mainId].sub[subAnswer.subId])
                                                        $scope.GG.group[groupAnswer.groupId].main[subAnswer.mainId].sub[subAnswer.subId].counter++;

                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                //sau khi tinh count cua main va sub thi ta tinh phan tram cho moi sub trong main
            }

            calculatePercentOfSub();
            drawChart();
        }

        function calculatePercentOfSub(){
            for(var k in $scope.GG.group){
                for(var kk in $scope.GG.group[k].main){
                    var mainCounter = $scope.GG.group[k].main[kk].counter;
                    if(Object.keys($scope.GG.group[k].main[kk].sub).length == 0){
                        $scope.GG.group[k].main[kk].class = "single";
                    }else{
                        $scope.GG.group[k].main[kk].class = "multi";
                    }
                    for(var kkk in $scope.GG.group[k].main[kk].sub){
                        var percent = 0;
                        if(mainCounter != 0){
                            percent = ($scope.GG.group[k].main[kk].sub[kkk].counter/mainCounter)*100;
                        }
                        $scope.GG.group[k].main[kk].sub[kkk].percent = percent;

                    }
                }
            }
        }

        function drawChart() {
            //lay total de tinh phan tram cho sub
            var totalQS1 = 0;
            _.size($scope.GG.group[$scope.groupNumber.firstGroup].main);
            for(var k in $scope.GG.group[$scope.groupNumber.firstGroup].main){
                var counter = $scope.GG.group[$scope.groupNumber.firstGroup].main[k].counter;
                totalQS1 = totalQS1 + counter;
            }
            var chart = $("#sumary .demo-container,#sumary .sumary01-right>img");
            if(totalQS1 == 0){
                chart.hide();
                for(var k in $scope.GG.group[$scope.groupNumber.firstGroup].main){
                    var counter = $scope.GG.group[$scope.groupNumber.firstGroup].main[k].counter;
                    $scope.GG.group[$scope.groupNumber.firstGroup].main[k].percent = counter;
                }
            }else{
                chart.show();
                for(var k in $scope.GG.group[$scope.groupNumber.firstGroup].main){
                    var counter = $scope.GG.group[$scope.groupNumber.firstGroup].main[k].counter;
                    $scope.GG.group[$scope.groupNumber.firstGroup].main[k].percent = (counter/totalQS1)*100;
                }
            }

            var dataBarChart = [];
            for(var k in $scope.GG.group[$scope.groupNumber.firstGroup].main){
                dataBarChart.push({
                    data: [ [$scope.GG.group[$scope.groupNumber.firstGroup].main[k].description, ($scope.GG.group[$scope.groupNumber.firstGroup].main[k].percent).toFixed(1)] ]
                });
            }

            //ve bieu do cot
            $.plot("#placeholder-c1-01", dataBarChart, {
                series:
                {
                    bars: {
                        show: true,
                        barWidth: 1.5,
                        showNumbers: true,
                        numbers : {
                            show:true,
                            yAlign: function(y) {
                                return y+4;
                            }
                        }
                    },
                    percent: {
                        show: true,
                        color:["#a0d468", "#51aafc", "#fc6e51", "#da51fc", "#ffce54"]
                    }
                },
                colors: ["#a0d468", "#51aafc", "#fc6e51", "#da51fc", "#ffce54"],
                xaxis: {
                    mode: "categories",
                    tickLength: 0,
                    font:10,
                    ticks: [
                        [0, "眼圧下降効果"],
                        [1, "アドヒアランス"],
                        [2, "安全性"],
                        [3, "薬価"],
                        [4, "眼圧下降以外の効果"]
                    ]
                },
                grid: {
                    borderWidth: 1
                }
            });

            //chart pie
            var dataPieChart = [];
            for(var k in $scope.GG.group[$scope.groupNumber.secondGroup].main) {
                dataPieChart.push({
                    data:$scope.GG.group[$scope.groupNumber.secondGroup].main[k].counter
                });
            }
            //console.log(dataPieChart);
            //ve bieu do tron
            $.plot("#placeholder-c1-02", dataPieChart, {
                series: {
                    pie: {
                        show: true
                    },
                    bars: {
                        numbers: {}
                    }
                },
                legend: {
                    show: false
                },
                colors: ["#a0d468", "#51aafc", "#fc6e51", "#da51fc", "#ffce54","#ff0000"]
            });
        }

        $scope.refresh = function () {
            $scope.showBackChart = appFactory._showBackChart || false;
            calculateAnswersData();
        }
        $scope.resetBackChart = function(nextPage){
            appFactory._showBackChart = false;
            $scope.changePage(nextPage);
        }
    }
);
