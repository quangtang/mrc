cordova.define("com.gumiviet.gumiServices.GumiServices", function(require, exports, module) { var exec = require('cordova/exec');

exports.execAction = function(success, error, actionName, args) {
	console.log('Call to Gumiviet service, action: ' + actionName);
    exec(success, error, "GumiServices", "execAction", [actionName, args]);
};

});
