/********* CDVGumiServices.m Cordova Plugin Implementation *******/

#import <Cordova/CDV.h>

@class GumiServices;

@protocol IGumiActionHandler
- (void)execAction:(CDVInvokedUrlCommand*)command actionName:(NSString*)actionName args:(NSDictionary*)args serviceHost:(GumiServices*)serviceHost;
@end

@interface GumiServices : CDVPlugin {
}
+ (void)registerHandler:(id<IGumiActionHandler>)handler;
@end
