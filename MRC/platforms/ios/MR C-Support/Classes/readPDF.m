//
//  readPDF.m
//  PDF
//
//  Created by GUMI-QUANG on 8/8/14.
//
//

#import "readPDF.h"

@implementation readPDF

- (void)execAction:(CDVInvokedUrlCommand*)command actionName:(NSString*)actionName args:(NSDictionary*)args serviceHost:(GumiServices*)serviceHost {
    
    _serviceHost = serviceHost;
    _command = command;
    
    if ([actionName isEqualToString:@"readPDF"]) {
        
        //get params from javascript sending
        NSString* fileURL = [args objectForKey:@"url"];
        pageNumber = [NSString stringWithFormat:@"%@", [args objectForKey:@"pageNumber"]].floatValue;
        if(pageNumber<1){
            pageNumber = 1;
        }
        NSLog(@"page Number: %f",pageNumber);
        //convert url to correct
        NSString* path = [[NSBundle mainBundle] pathForResource:@"www" ofType:@""];
        NSString *str = [@"file://" stringByAppendingString:path];
        fileURL = [str stringByAppendingString:fileURL];

        fileURL = [fileURL stringByAddingPercentEscapesUsingEncoding:NSASCIIStringEncoding];
         NSLog(@"encode fileURL:%@",fileURL);
        //fileURL = [fileURL stringByReplacingOccurrencesOfString:@"MR C-Support" withString:@"MR%20C-Support"];
        
        NSURL *url = [[NSURL URLWithString:fileURL] absoluteURL];
        
        //get total count of pdf pages
        CGPDFDocumentRef pdf = CGPDFDocumentCreateWithURL((CFURLRef)url);
        pageCount = (int)CGPDFDocumentGetNumberOfPages(pdf);
        
        //check if webview already created or not. Because we just create a time
        if(pdfView == nil){
            CGRect screenBound = [[UIScreen mainScreen] bounds];
            CGSize screenSize = screenBound.size;
            CGFloat screenWidth = screenSize.width;
            CGFloat screenHeight = screenSize.height;
            height = screenWidth;
            pdfView = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, screenHeight, screenWidth)];
            
            //setting user can pinchin/pinchout
            pdfView.scalesPageToFit = YES;
            
            //delegate event from UIWebviewDelegate
            pdfView.delegate = self;
            
            //delegate event from UIScrollviewDelegate
            pdfView.scrollView.delegate = self;
            pdfView.scrollView.scrollEnabled = YES;
            
            //add pdfview to viewcontroller of cordova view
            [_serviceHost.webView addSubview:pdfView];
            
            
            //Create close button
            UIButton* closeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
            NSString* imagePath = [[NSBundle mainBundle] pathForResource:@"popup-close" ofType:@"png"];
            UIImage* img = [[UIImage alloc] initWithContentsOfFile:imagePath];
            [closeBtn setImage:img forState:UIControlStateNormal];
            closeBtn.frame = CGRectMake(screenHeight - 60, screenWidth - 60, 40, 40);
            
            //event click on close button call hidePdfView
            [closeBtn addTarget:self action:@selector(hidePdfView) forControlEvents:UIControlEventTouchUpInside];
            
            //add close button to pdfview
            [pdfView addSubview:closeBtn];
        }
       
            //call load pdf file
            NSURLRequest *request = [NSURLRequest requestWithURL:url];
            [pdfView loadRequest:request];
        
    }
}

//called when pdfview load starting
-(void)webViewDidStartLoad:(UIWebView *)webView{
    pdfView.hidden = YES;
}

//called when webview loaded
- (void)webViewDidFinishLoad:(UIWebView *)webView{
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
    [_serviceHost.commandDelegate sendPluginResult:pluginResult callbackId:_command.callbackId];
    NSTimer *timer = [NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(gotoPage) userInfo:nil repeats:NO];
    NSLog(@"%@",timer);
}

//scroll pdf view to the correct page number
-(void)gotoPage{
    
    //get height of content of pages
    float contentHeight = pdfView.scrollView.contentSize.height;
    NSLog(@"contentHeight:%f",contentHeight);
    //get height pdf per a page
    float pageHeight = contentHeight/pageCount;
    
    //get height of pdf to scroll
    float y = pageHeight*(pageNumber-1);

    [[pdfView scrollView] setContentOffset:CGPointMake(0,y) animated:NO];
    pdfView.hidden = NO;
}

//hide pdf view when click on close button
-(void) hidePdfView{
    pdfView.hidden = YES;
}


//#pragma UIScrollViewDelegate
//- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
//    float verticalContentOffset = pdfView.scrollView.contentOffset.y;
//    NSLog(@"PDF page number:%f",verticalContentOffset);
//}

@end
