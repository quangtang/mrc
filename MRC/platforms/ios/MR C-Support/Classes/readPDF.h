//
//  readPDF.h
//  PDF
//
//  Created by GUMI-QUANG on 8/8/14.
//
//

#import <Foundation/Foundation.h>
#import "GumiServices.h"
@interface readPDF : NSObject<IGumiActionHandler,UIWebViewDelegate,UIScrollViewDelegate> {
    UIViewController* _controller;
    GumiServices* _serviceHost;
    CDVInvokedUrlCommand* _command;
    UIWebView *pdfView;
    CGFloat pageCount,pageNumber,height;
}

@end
