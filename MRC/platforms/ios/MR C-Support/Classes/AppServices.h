//
//  AppServices.h
//  PDF
//
//  Created by GUMI-QUANG on 8/8/14.
//
//

#import <Foundation/Foundation.h>
#include "GumiServices.h"

@interface AppServices : NSObject <IGumiActionHandler> {
    NSMutableDictionary* _handlers;
}

-(void) registerHandler:(NSString*)actionName handler:(id<IGumiActionHandler>)handler;

@end
