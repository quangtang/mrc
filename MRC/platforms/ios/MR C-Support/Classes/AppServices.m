//
//  AppServices.m
//  PDF
//
//  Created by GUMI-QUANG on 8/8/14.
//
//

#import "AppServices.h"

@implementation AppServices

- (id)init {
    if ([super init]) {
        _handlers = [[NSMutableDictionary alloc] init];
        
    }
    return self;
}

-(void) registerHandler:(NSString*)actionName handler:(id<IGumiActionHandler>)handler {
    [_handlers setObject:handler forKey:actionName];
}

- (void)execAction:(CDVInvokedUrlCommand*)command actionName:(NSString*)actionName args:(NSDictionary*)args serviceHost:(GumiServices*)serviceHost {
    id<IGumiActionHandler> handler = [_handlers objectForKey:actionName];
    if (handler != nil) {
        [handler execAction:command actionName:actionName args:args serviceHost:serviceHost];
    }
}

@end
